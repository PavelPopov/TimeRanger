Name: TimeRanger  
Author: PavelPopov <pavelpopov10@gmail.com>  
License: GNU GPL v3  
Repository: https://gitlab.com/PavelPopov/TimeRanger  

TimeRanger is time and task tracking software.  
It helps you to get rid of procrastination and do great things in your life.  
