package timeranger;

import javafx.scene.paint.Color;

/**
 * Helper methods for color conversion and generation
 *
 * @author PavelPopov
 */
public final class ColorUtil {

    /**
     * Get integer value representing color from color object
     *
     * @param color color object
     * @return integer representing color
     */
    public static final Integer getValue(Color color) {
        int r = (int) (color.getRed() * 255);
        int g = (int) (color.getGreen() * 255);
        int b = (int) (color.getBlue() * 255);
        return r * 1000000 + g * 1000 + b;
    }

    /**
     * Get color object from integer value representing color
     *
     * @param value integer representing color
     * @return color object
     */
    public static final Color getColor(Integer value) {
        int r = value / 1000000;
        int g = (value - r * 1000000) / 1000;
        int b = value - r * 1000000 - g * 1000;
        return Color.rgb(r, g, b);
    }

    /**
     * Get hex color from color object
     *
     * @param color color object
     * @return hex color
     */
    public static final String getHex(Color color) {
        return String.format("#%02X%02X%02X",
                (int) (color.getRed() * 255),
                (int) (color.getGreen() * 255),
                (int) (color.getBlue() * 255));
    }

    /**
     * Get black or white color contrasting with source color
     *
     * @param source color object
     * @return contrasting color object
     */
    public static final Color getContrastingColor(Color source) {
        Color target = Color.BLACK;
        if (source.getRed() * 255 * 0.299
                + source.getGreen() * 255 * 0.587
                + source.getBlue() * 255 * 0.114
                <= 186) {
            target = Color.WHITE;
        }
        return target;
    }
}
