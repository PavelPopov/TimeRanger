package timeranger;

import java.lang.Thread.UncaughtExceptionHandler;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Class for handling application errors
 *
 * @author PavelPopov
 */
public class ErrorHandler {

    /**
     * Handler of uncaught exceptions
     */
    private static UncaughtExceptionHandler handler = (Thread t, Throwable e) -> {
        String contentr = getExceptionString(e);
        handle("Unknown error in thread \"" + t.getName() + "\" occured!\n"
                + "Application will be finished.\n"
                + "Please send this information to author: PavelPopov <pavelpopov10@gmail.com>\n"
                + "Or report it as issue in project's repository: https://gitlab.com/PavelPopov/TimeRanger\n"
                + "Thank you! :)", contentr);
        TimeRanger.hideStage();
        System.exit(1);
    };

    /**
     * Generate exception string from given throwable
     *
     * @param e throwable
     * @return exception string
     */
    private static String getExceptionString(Throwable e) {
        String contentr = e.toString() + "\n";
        for (StackTraceElement ste : e.getStackTrace()) {
            contentr += ste.toString() + "\n";
        }
        if (e.getCause() != null) {
            contentr += "=== CAUSE ===\n";
            contentr += getExceptionString(e.getCause());
        }
        return contentr;
    }

    /**
     * Handle error
     *
     * @param title error title
     * @param e throwable
     */
    public static void handle(String title, Throwable e) {
        handle(title, getExceptionString(e));
    }

    /**
     * Handle error
     *
     * @param title error title
     * @param content error description
     */
    public static void handle(String title, String content) {
        System.err.print("\n[ERROR] " + title + "\n" + content);
        TimeRanger.runAndWait(() -> {
            try {
                double sWidth = TimeRanger.getRootStage().getScene().getWidth();
                double sHeight = TimeRanger.getRootStage().getScene().getHeight();
                double nWidth = sWidth * 0.75 > 180 ? sWidth * 0.75 : 180;
                double nHeight = sHeight * 0.75 > 180 ? sHeight * 0.75 : 180;
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initOwner(TimeRanger.getRootStage());
                stage.setMinWidth(200);
                stage.setMinHeight(200);
                stage.setMaxWidth(800);
                stage.setMaxHeight(600);
                VBox vbox = new VBox(10);
                TextArea textArea = new TextArea(title + "\n" + content);
                textArea.setEditable(false);
                textArea.setStyle(".text-area {\n"
                        + "  -fx-font-family: Consolas;\n"
                        + "  -fx-highlight-fill: #ff0000;\n"
                        + "  -fx-highlight-text-fill: #ffffff;\n"
                        + "  -fx-text-fill: #ff0000;\n"
                        + "}");
                textArea.setPrefSize(800, 600);
                vbox.getChildren().add(textArea);
                Scene scene = new Scene(vbox, nWidth, nHeight);
                stage.setTitle("Error!");
                stage.setScene(scene);
                stage.showAndWait();
            } catch (Throwable e) {
                System.err.println(e);
            }
        });
    }

    /**
     * Get uncaught exception handler
     *
     * @return uncaught exception handler
     */
    public static UncaughtExceptionHandler getHandler() {
        return handler;
    }
}
