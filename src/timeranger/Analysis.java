package timeranger;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import timeranger.logic.UntaggedSliderValue;
import timeranger.model.Activity;
import timeranger.model.Tag;

/**
 * Analysis methods
 *
 * @author PavelPopov
 */
public final class Analysis {

    /**
     * Calculate activities properties
     *
     * @param activities list of activities
     * @return map of activities properties
     */
    public final static HashMap<String, Long> calcProperties(ArrayList<Activity> activities) {
        HashMap<String, Long> map = new HashMap();
        long duration = 0;
        long minimum = Long.MAX_VALUE;
        long maximum = 0;
        for (Activity activity : activities) {
            long diff = activity.duration();
            duration += diff;
            minimum = diff < minimum ? diff : minimum;
            maximum = diff > maximum ? diff : maximum;
        }
        long average = activities.isEmpty() ? 0 : Math.floorDiv(duration, activities.size());
        map.put("Duration", duration);
        map.put("Average", average);
        map.put("Minimum", minimum);
        map.put("Maximum", maximum);
        return map;
    }

    /**
     * Calculate activities by tags distribution
     *
     * @param activities list of activities
     * @param tags list of tags
     * @param untagged untagged activities handling mode
     * @return distribution of activities by tags
     */
    public final static HashMap<Tag, Double> calcDistribution(List<Activity> activities, List<Tag> tags,
            UntaggedSliderValue untagged) {
        HashMap<Tag, Double> map = new HashMap();
        if (!untagged.equals(UntaggedSliderValue.EXCLUDE)) {
            double value = 0;
            for (Activity activity : activities) {
                if (!activity.getTags().isEmpty()) {
                    continue;
                }
                value += activity.duration();
            }
            map.put(null, value);
        }
        if (!untagged.equals(UntaggedSliderValue.ONLY)) {
            for (Tag tag : tags) {
                double value = 0;
                for (Activity activity : activities) {
                    if (!activity.getTags().contains(tag)) {
                        continue;
                    }
                    value += activity.duration();
                }
                map.put(tag, value);
            }
        }
        return map;
    }

    /**
     * Calculate sum of activities durations in given time interval
     *
     * @param activities list of activities
     * @param start start time interval bound
     * @param end end time interval bound
     * @return sum of activities durations
     */
    public final static double calcDuration(ArrayList<Activity> activities,
            LocalDateTime start, LocalDateTime end) {
        double value = 0;
        for (Activity activity : activities) {
            // Continue if out of range
            long diff1 = TimeUtil.getDifference(start, activity.getEnd());
            long diff2 = ChronoUnit.SECONDS.between(activity.getStart(), end);
            if (diff1 <= 0 || diff2 <= 0) {
                continue;
            }
            // Calculate actual value for range
            long diff = activity.duration();
            diff1 = ChronoUnit.SECONDS.between(activity.getStart(), start);
            diff2 = TimeUtil.getDifference(end, activity.getEnd());
            value += diff + (diff1 > 0 ? -diff1 : 0) + (diff2 > 0 ? -diff2 : 0);
        }
        return value;
    }
}
