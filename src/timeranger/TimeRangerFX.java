package timeranger;

import com.sun.javafx.application.LauncherImpl;
import java.util.concurrent.CountDownLatch;
import javafx.application.Application;
import javafx.stage.Stage;
import timeranger.logic.controller.AboutController;
import timeranger.logic.controller.ActivityEditController;
import timeranger.logic.controller.RootController;

/**
 * JavaFX application
 *
 * @author PavelPopov
 */
public final class TimeRangerFX extends Application {

    /**
     * Class instance
     */
    private static TimeRangerFX instance;
    /**
     * Latch for releasing thread after start
     */
    private static CountDownLatch latch;
    /**
     * Stages of JavaFX application
     */
    private Stage rootStage, aboutStage, activityEditStage;

    /**
     * Get instance of class
     *
     * @return instance
     */
    public static TimeRangerFX getInstance() {
        return instance;
    }

    /**
     * Get root stage
     *
     * @return root stage
     */
    Stage getRootStage() {
        return rootStage;
    }

    /**
     * Get about stage
     *
     * @return about stage
     */
    Stage getAboutStage() {
        return aboutStage;
    }

    /**
     * Get activity edit stage
     *
     * @return activity edit stage
     */
    Stage getActivityEditStage() {
        return activityEditStage;
    }

    /**
     * Initialize and launch JavaFX application
     *
     * @param fxArgs launch arguments
     * @param latch latch for releasing threads
     */
    static void init(String[] fxArgs, CountDownLatch latch) {
        TimeRangerFX.latch = latch;
        try {
            LauncherImpl.launchApplication(TimeRangerFX.class, TimeRangerPreloader.class, fxArgs);
        } catch (Exception ex) {
            ErrorHandler.handle("OMG!", ex);
        }
    }

    @Override
    public void start(Stage rootStage) {
        instance = this;
        TimeRangerPreloader.setLoading("Loading UI [1/3]...", 0.333);
        this.rootStage = RootController.loadStage(rootStage);
        TimeRangerPreloader.setLoading("Loading UI [2/3]...", 0.666);
        this.aboutStage = AboutController.loadStage(rootStage);
        TimeRangerPreloader.setLoading("Loading UI [3/3]...", 0.999);
        this.activityEditStage = ActivityEditController.loadStage(rootStage);
        TimeRangerPreloader.setLoading("Finishing initialization...", 1);
        latch.countDown();
    }
}
