package timeranger.view.factory;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.Observable;
import javafx.event.Event;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.util.StringConverter;
import timeranger.TimeRanger;
import timeranger.logic.controller.RootController;
import timeranger.model.Tag;

/**
 * Custom editable chip view cell
 *
 * @author PavelPopov
 */
public class EditableTableChipViewCell<S>
        extends AbstractEditableTableCell<S, List<Tag>, TagChipView<Tag>> {

    /**
     * Flag indicating chips update process used to skip listeners actions
     */
    private boolean updatingChips = false;
    private TagChipViewSkin skin;

    public EditableTableChipViewCell(TableColumn<S, List<Tag>> col) {
        super(col);
        field = new TagChipView<Tag>();
        skin = (TagChipViewSkin<Tag>) field.createDefaultSkin();
        field.setSkin(skin);
        field.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        field.setPrefHeight(skin.getRoot().getHeight());
        field.setConverter(new StringConverter<Tag>() {
            @Override
            public String toString(Tag t) {
                return t == null ? null : t.getName();
            }

            @Override
            public Tag fromString(String name) {
                for (Tag tag : TimeRanger.getTags()) {
                    if (tag.getName().equals(name)) {
                        return tag;
                    }
                }
                return new Tag(0, name, 0, "");
            }
        });
        // TODO: change location of tag creation method or make something
        // to avoid association with root frame
        field.setChipFactory((chipView, tag) -> {
            return new TagChip<Tag>(chipView, tag);
        });
        field.getChips().addListener((Observable observable) -> {
            if (!updatingChips) {
                commitHelper(false);
            }
        });
        // TODO: change how it's set and to what value it's set
        skin.getRoot().heightProperty().addListener((observable, oldValue, newValue) -> {
            double v = (double) newValue;
            updatePrefHeight(v);
        });
    }

    public void updatePrefHeight(double v) {
        if (Double.compare(getPrefHeight(), v) == 0) {
            return;
        }
        setPrefHeight(v);
        updateBounds();
    }

    @Override
    protected List<Tag> getValue() {
        return getItem() == null ? new ArrayList() : getItem();
    }

    @Override
    protected void commitHelper(boolean losingFocus) {
        commitEdit(field.getChips());
        ArrayList<Tag> ts = new ArrayList();
        for (Tag t : field.getChips()) {
            if (!TimeRanger.getTags().contains(t)) {
                TimeRanger.addTag(t);
            }
            Tag tag = null;
            for (Tag t2 : TimeRanger.getTags()) {
                if (t2.getName().equals(t.getName())) {
                    tag = t2;
                }
            }
            if (tag != null) {
                boolean found = false;
                for (Tag e : ts) {
                    if (e.getName().equals(tag.getName())) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    ts.add(tag);
                }
            }
        }
        RootController.getInstance().onTagsChanged(getTableRow(), ts);
    }

    @Override
    public void commitEdit(List<Tag> item) {
        TableView<S> table = getTableView();
        if (table != null) {
            TableColumn<S, List<Tag>> column = getTableColumn();
            CellEditEvent<S, List<Tag>> event = new CellEditEvent<>(
                    table, new TablePosition(table, getIndex(), column),
                    TableColumn.editCommitEvent(), item);
            Event.fireEvent(column, event);
        }
    }

    @Override
    public void updateItem(List<Tag> item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            field.getSuggestions().setAll(TimeRanger.getTags());
            updatingChips = true;
            field.getChips().setAll(getValue());
            updatingChips = false;
            setGraphic(field);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            updatePrefHeight(skin.getRoot().getHeight());
        }
    }
}
