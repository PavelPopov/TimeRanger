package timeranger.view.factory;

import java.time.LocalDateTime;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableColumn;
import jfxtras.scene.control.LocalDateTimeTextField;
import timeranger.TimeRanger;

/**
 * Custom editable timestamp cell
 *
 * @author PavelPopov
 */
public class EditableTableDateTimeCell<S>
        extends AbstractEditableTableCell<S, LocalDateTime, LocalDateTimeTextField> {

    /**
     * Variable used to ignore two magic focus events
     */
    private int ignoreFocusCount = 2;

    public EditableTableDateTimeCell(TableColumn<S, LocalDateTime> col) {
        super(col);
    }

    @Override
    protected LocalDateTime getValue() {
        return getItem() == null ? LocalDateTime.now() : getItem();
    }

    @Override
    protected void commitHelper(boolean losingFocus) {
        if (losingFocus) {
            cancelEdit();
        } else {
            commitEdit(field.getLocalDateTime());
        }
    }

    @Override
    public void startEdit() {
        super.startEdit();
        if (field == null) {
            field = new LocalDateTimeTextField(getValue());
            field.setDateTimeFormatter(TimeRanger.DATETIME_FORMATTER);
            field.setAllowNull(true);
            field.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            field.setOnKeyReleased(KEY_HANDLER);
            Platform.runLater(() -> {
                field.getChildrenUnmodifiable().get(0).focusedProperty()
                        .addListener((ObservableValue<? extends Boolean> observable,
                                Boolean oldValue, Boolean newValue) -> {
                            // TODO: make something better then ignoring those events
                            if (ignoreFocusCount > 0) {
                                ignoreFocusCount--;
                                return;
                            }
                            // Do not clean up if we clicked a picker icon
                            if (!newValue && field != null && !field.isPickerShowing()) {
                                commitHelper(true);
                                ignoreFocusCount = 2;
                            }
                        });
            });
        }
        setGraphic(field);
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        Platform.runLater(() -> {
            field.selectAll();
            field.requestFocus();
        });
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setText(getValue().format(TimeRanger.DATETIME_FORMATTER));
        setContentDisplay(ContentDisplay.TEXT_ONLY);
        Platform.runLater(() -> {
            //Once the edit has been cancelled we no longer need the text field
            //so we mark it for cleanup here. Note though that you have to handle
            //this situation in the focus listener which gets fired at the end
            //of the editing.
            field = null;
        });
    }

    @Override
    public void updateItem(LocalDateTime item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (field != null) {
                    field.setText(getValue().format(TimeRanger.DATETIME_FORMATTER));
                }
                setGraphic(field);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            } else {
                setText(getValue().format(TimeRanger.DATETIME_FORMATTER));
                setContentDisplay(ContentDisplay.TEXT_ONLY);
            }
        }
    }
}
