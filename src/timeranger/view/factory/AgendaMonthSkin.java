package timeranger.view.factory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import jfxtras.internal.scene.control.skin.agenda.base24hour.AgendaSkinTimeScale24HourAbstract;
import jfxtras.scene.control.agenda.Agenda;

/**
 * Customized version of original blocked JFoenix AgendaMonthSkin
 *
 * @author PavelPopov
 */
public class AgendaMonthSkin extends AgendaSkinTimeScale24HourAbstract<AgendaMonthSkin> {

    public AgendaMonthSkin(Agenda agenda) {
        super(agenda);
    }

    /**
     * Assign a calendar to each day, so it knows what it must draw.
     */
    @Override
    protected List<LocalDate> determineDisplayedLocalDates() {
        List<LocalDate> localDates = new ArrayList();
        // <month length> days stating at the first day of month
        LocalDate startLocalDate = getFirstDayOfMonthLocalDate();
        for (int i = 0; i < startLocalDate.lengthOfMonth(); i++) {
            localDates.add(startLocalDate.plusDays(i));
        }
        return localDates;
    }

    /**
     * get the date of the first day of the month
     */
    public LocalDate getFirstDayOfMonthLocalDate() {
        LocalDate displayedDateTime = getSkinnable().getDisplayedLocalDateTime().toLocalDate();
        int currentDayOfMonth = displayedDateTime.getDayOfMonth();
        displayedDateTime = displayedDateTime.minusDays(currentDayOfMonth - 1);
        return displayedDateTime;
    }

}
