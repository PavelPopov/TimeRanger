package timeranger.view.factory;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Control;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;

/**
 * Custom editable text cell
 *
 * @author PavelPopov
 */
public class EditableTableNameCell<S>
        extends AbstractEditableTableCell<S, String, TextArea> {

    private static final int DEFAULT_MAX_CHARS = 512;
    private static final String DEFAULT_RESTRICT_TO = "[^\\t\\n]*";

    private final int maxChars;
    private final String restrictTo;

    private Text text = new Text();

    public EditableTableNameCell(TableColumn<S, String> col) {
        this(col, DEFAULT_MAX_CHARS, DEFAULT_RESTRICT_TO);
    }

    public EditableTableNameCell(TableColumn<S, String> col, int maxChars, String restrictTo) {
        super(col);
        this.restrictTo = restrictTo;
        this.maxChars = maxChars;
        text.wrappingWidthProperty().bind(col.widthProperty().subtract(text.getFont().getSize()));
    }

    private void updateField() {
        setGraphic(text);
        setPrefHeight(Control.USE_COMPUTED_SIZE);
    }

    @Override
    protected String getValue() {
        return getItem() == null ? "" : getItem();
    }

    @Override
    protected void commitHelper(boolean losingFocus) {
        if (field.getText().isEmpty()) {
            cancelEdit();
        } else {
            commitEdit(field.getText());
        }
    }

    @Override
    public void startEdit() {
        super.startEdit();
        if (field == null) {
            field = new TextArea(getValue()) {
                @Override
                public void replaceText(int start, int end, String text) {
                    if (matchTest(text)) {
                        super.replaceText(start, end, text);
                    }
                }

                @Override
                public void replaceSelection(String text) {
                    if (matchTest(text)) {
                        super.replaceSelection(text);
                    }
                }

                private boolean matchTest(String text) {
                    return text.isEmpty() || (text.matches(restrictTo) && getText().length() < maxChars);
                }
            };
            // Remove newlines and tabs, enter is not used for input
            field.setOnKeyReleased((KeyEvent t) -> {
                KEY_HANDLER.handle(t);
                if (field != null) {
                    Text text = (Text) field.lookup(".text");
                    field.setPrefHeight(text.boundsInParentProperty().get().getMaxY() + text.getFont().getSize());
                }
            });
            field.setMinWidth(0);
            field.setPrefWidth(1);
            field.setPrefHeight(field.boundsInParentProperty().get().getMaxY());
            field.setWrapText(true);
            field.focusedProperty().addListener((ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) -> {
                //This focus listener fires at the end of cell editing when focus is lost
                //and when enter is pressed (because that causes the text field to lose focus).
                //The problem is that if enter is pressed then cancelEdit is called before this
                //listener runs and therefore the text field has been cleaned up. If the
                //text field is null we don't commit the edit. This has the useful side effect
                //of stopping the double commit.
                if (!newValue && field != null) {
                    commitHelper(true);
                }
            });
        }
        setGraphic(field);
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        Platform.runLater(() -> {
            while (field == null) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                }
            }
            if (isEditing()) {
                ScrollBar scrollBarv = (ScrollBar) field.lookup(".scroll-bar:vertical");
                scrollBarv.setDisable(true);
                field.selectAll();
                field.requestFocus();
            }
        });
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        updateField();
        //Once the edit has been cancelled we no longer need the text field
        //so we mark it for cleanup here. Note though that you have to handle
        //this situation in the focus listener which gets fired at the end
        //of the editing.
        field = null;
    }

    @Override
    public void commitEdit(String s) {
        super.commitEdit(s);
        updateField();
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (field != null) {
                    field.setText(getValue());
                }
                setGraphic(field);
            } else {
                text.setText(getValue());
                updateField();
            }
            setWrapText(true);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        }
    }
}
