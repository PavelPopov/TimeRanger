package timeranger.view.factory;

import java.util.ArrayList;
import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyEvent;

/**
 * Provides the basis for an editable table cell. Sub-classes can provide
 * formatters for display and a commitHelper to control when editing is
 * committed.
 *
 * @author Graham Smith
 * @author PavelPopov
 */
public abstract class AbstractEditableTableCell<S, T, D extends Node>
        extends TableCell<S, T> {

    protected TableColumn<S, T> column;
    protected D field;

    protected final EventHandler<KeyEvent> KEY_HANDLER = (KeyEvent t) -> {
        if (null != t.getCode()) {
            switch (t.getCode()) {
                case ENTER:
                    commitHelper(false);
                    break;
                case ESCAPE:
                    cancelEdit();
                    break;
                case TAB:
                    commitHelper(false);
                    TableColumn nextColumn = getNeighborColumn(!t.isShiftDown());
                    if (nextColumn != null) {
                        getTableView().edit(getTableRow().getIndex(), nextColumn);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    public AbstractEditableTableCell(TableColumn<S, T> column) {
        this.column = column;
    }

    public TableColumn<S, T> getColumn() {
        return column;
    }

    /**
     * Provides the value of this cell when the cell is not being edited.
     */
    protected abstract T getValue();

    /**
     * Any action attempting to commit an edit should call this method rather
     * than commit the edit directly itself. This method will perform any
     * validation and conversion required on the value. For text values that
     * normally means this method just commits the edit but for numeric values,
     * for example, it may first parse the given input.
     * <p>
     * The only situation that needs to be treated specially is when the field
     * is losing focus. If you user hits enter to commit the cell with bad data
     * we can happily cancel the commit and force them to enter a real value. If
     * they click away from the cell though we want to give them their old value
     * back.
     *
     * @param losingFocus true if the reason for the call was because the field
     * is losing focus.
     */
    protected abstract void commitHelper(boolean losingFocus);

    /**
     * Get neighbor column from table
     *
     * @param forward true gets the column to the right, false the column to the
     * left of the current column
     * @return
     */
    protected final TableColumn<S, ?> getNeighborColumn(boolean forward) {
        List<TableColumn<S, ?>> columns = new ArrayList<>();
        for (TableColumn<S, ?> column : getTableView().getColumns()) {
            columns.addAll(getLeaves(column));
        }
        //There is no other column that supports editing.
        if (columns.size() < 2) {
            return null;
        }
        int currentIndex = columns.indexOf(getTableColumn());
        int nextIndex = currentIndex;
        if (forward) {
            nextIndex++;
            if (nextIndex > columns.size() - 1) {
                nextIndex = 0;
            }
        } else {
            nextIndex--;
            if (nextIndex < 0) {
                nextIndex = columns.size() - 1;
            }
        }
        return columns.get(nextIndex);
    }

    /**
     * Get leaves of table column
     *
     * @param root table column
     * @return list of column leaves
     */
    protected final List<TableColumn<S, ?>> getLeaves(TableColumn<S, ?> root) {
        List<TableColumn<S, ?>> columns = new ArrayList<>();
        if (root.getColumns().isEmpty()) {
            //We only want the leaves that are editable.
            if (root.isEditable()) {
                columns.add(root);
            }
            return columns;
        } else {
            for (TableColumn<S, ?> column : root.getColumns()) {
                columns.addAll(getLeaves(column));
            }
            return columns;
        }
    }
}
