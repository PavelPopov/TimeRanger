package timeranger.view.factory;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.css.CssMetaData;
import javafx.css.SimpleStyleableObjectProperty;
import javafx.css.Styleable;
import javafx.scene.Node;
import jfxtras.css.CssMetaDataForSkinProperty;
import jfxtras.css.converters.IntegerConverter;
import jfxtras.internal.scene.control.skin.agenda.base24hour.AgendaSkinTimeScale24HourAbstract;
import jfxtras.scene.control.agenda.Agenda;

/**
 * Filtered skin for agenda.
 * <p>
 * Allows visualization of filtered activities
 *
 * @author PavelPopov
 */
public final class AgendaFilteredSkin extends AgendaSkinTimeScale24HourAbstract<AgendaFilteredSkin> {

    /**
     * Displayed days from/to
     */
    private String displayedLocalDatesKey = "";
    /**
     * List of displayed days
     */
    private List<LocalDate> displayedLocalDates;
    /**
     * Start and end dates
     */
    private LocalDate startDate, endDate;
    private ObjectProperty<Integer> daysBeforeFurthestProperty = new SimpleStyleableObjectProperty<Integer>(StyleableProperties.DAYS_BEFORE_FURTHEST_CSSMETADATA, StyleableProperties.DAYS_BEFORE_FURTHEST_CSSMETADATA.getInitialValue(null)) {
//		{ // anonymous constructor
//			addListener( (invalidationEvent) -> {
//				if (daysBeforeFurthestProperty.get() > 0) {
//					daysBeforeFurthestProperty.set(0);
//				}
//			});
//		}
    };

    public AgendaFilteredSkin(Agenda control, LocalDate startDate, LocalDate endDate) {
        super(control);
        this.startDate = startDate;
        this.endDate = endDate;
        super.reconstruct();
    }

    /**
     * Assign a calendar to each day, so it knows what it must draw.
     *
     * @return list of displayed days
     */
    @Override
    protected List<LocalDate> determineDisplayedLocalDates() {
        if (startDate == null || endDate == null) {
            // if dates not set - show today
            return Arrays.asList(getSkinnable().getDisplayedLocalDateTime().toLocalDate());
        }
        LocalDate epoch = LocalDate.ofEpochDay(0);
        long start = ChronoUnit.DAYS.between(epoch, startDate);
        long end = ChronoUnit.DAYS.between(epoch, endDate);
        String key = start + " / " + end;
        if (!key.equals(displayedLocalDatesKey)) {
            displayedLocalDates = new ArrayList<>();
            for (int i = 0; i < end - start; i++) {
                displayedLocalDates.add(startDate.plusDays(i));
            }
            displayedLocalDatesKey = key;
        }
        return displayedLocalDates;
    }

    /**
     * @return days before furthest property
     */
    public final ObjectProperty<Integer> daysBeforeFurthestProperty() {
        return daysBeforeFurthestProperty;
    }

    /**
     * @param value days before furthest
     */
    public final void setDaysBeforeFurthest(int value) {
        daysBeforeFurthestProperty.set(value);
    }

    /**
     * @return days before furthest
     */
    public final int getDaysBeforeFurthest() {
        return daysBeforeFurthestProperty.get();
    }

    /**
     * Construct skin with days before furthest
     *
     * @param value
     * @return
     */
    public final AgendaFilteredSkin withDaysBeforeFurthest(int value) {
        setDaysBeforeFurthest(value);
        return this;
    }

    /**
     * @return days after furthest
     */
    public final ObjectProperty<Integer> daysAfterFurthestProperty() {
        return daysAfterFurthestProperty;
    }
    private ObjectProperty<Integer> daysAfterFurthestProperty = new SimpleStyleableObjectProperty<Integer>(StyleableProperties.DAYS_AFTER_FURTHEST_CSSMETADATA, StyleableProperties.DAYS_AFTER_FURTHEST_CSSMETADATA.getInitialValue(null)) {
//		{ // anonymous constructor
//			addListener( (invalidationEvent) -> {
//				if (daysAfterFurthestProperty.get() < 0) {
//					daysAfterFurthestProperty.set(0);
//				}
//			});
//		}
    };

    /**
     * @param value days after furthest
     */
    public final void setDaysAfterFurthest(int value) {
        daysAfterFurthestProperty.set(value);
    }

    /**
     * @return days after furthest
     */
    public final int getDaysAfterFurthest() {
        return daysAfterFurthestProperty.get();
    }

    /**
     * Construct skin with days after furthest
     *
     * @param value days after furthest
     * @return skin
     */
    public final AgendaFilteredSkin withDaysAfterFurthest(int value) {
        setDaysAfterFurthest(value);
        return this;
    }

    /**
     * Styleable properties
     */
    private static class StyleableProperties {

        private static final CssMetaData<Agenda, Integer> DAYS_BEFORE_FURTHEST_CSSMETADATA = new CssMetaDataForSkinProperty<Agenda, AgendaFilteredSkin, Integer>("-fxx-days-before-furthest", IntegerConverter.getInstance(), -9) {
            @Override
            protected ObjectProperty<Integer> getProperty(AgendaFilteredSkin s) {
                return s.daysBeforeFurthestProperty;
            }
        };

        private static final CssMetaData<Agenda, Integer> DAYS_AFTER_FURTHEST_CSSMETADATA = new CssMetaDataForSkinProperty<Agenda, AgendaFilteredSkin, Integer>("-fxx-days-after-furthest", IntegerConverter.getInstance(), 9) {
            @Override
            protected ObjectProperty<Integer> getProperty(AgendaFilteredSkin s) {
                return s.daysAfterFurthestProperty;
            }
        };

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES;

        static {
            final List<CssMetaData<? extends Styleable, ?>> classCssMetaData = new ArrayList(AgendaSkinTimeScale24HourAbstract.getClassCssMetaData());
            classCssMetaData.add(DAYS_BEFORE_FURTHEST_CSSMETADATA);
            classCssMetaData.add(DAYS_AFTER_FURTHEST_CSSMETADATA);
            STYLEABLES = Collections.unmodifiableList(classCssMetaData);
        }
    }

    /**
     * @return The CssMetaData associated with this class, which may include the
     * CssMetaData of its super classes.
     */
    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData() {
        return StyleableProperties.STYLEABLES;
    }

    /**
     * This method should delegate to {@link Node#getClassCssMetaData()} so that
     * a Node's CssMetaData can be accessed without the need for reflection.
     *
     * @return The CssMetaData associated with this node, which may include the
     * CssMetaData of its super classes.
     */
    @Override
    public List<CssMetaData<? extends Styleable, ?>> getCssMetaData() {
        return getClassCssMetaData();
    }

}
