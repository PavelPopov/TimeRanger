package timeranger.view.factory;

import com.jfoenix.controls.JFXColorPicker;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableColumn;
import timeranger.ColorUtil;

/**
 * Custom editable color cell
 *
 * @author PavelPopov
 */
public class EditableTableColorCell<S>
        extends AbstractEditableTableCell<S, Integer, JFXColorPicker> {

    public EditableTableColorCell(TableColumn<S, Integer> col) {
        super(col);
        field = new JFXColorPicker();
        field.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        field.setOnKeyReleased(KEY_HANDLER);
        field.showingProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                if (!isEditing()) {
                    getTableView().edit(getTableRow().getIndex(), getTableColumn());
                }
            } else {
                commitHelper(true);
            }
        });
    }

    @Override
    protected Integer getValue() {
        return ColorUtil.getValue(field.getValue());
    }

    @Override
    protected void commitHelper(boolean losingFocus) {
        commitEdit(getValue());
    }

    @Override
    public void updateItem(Integer item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            field.setValue(ColorUtil.getColor(item));
            setGraphic(field);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        }
    }
}
