package timeranger.view.factory;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXChip;
import com.jfoenix.controls.JFXChipView;
import com.jfoenix.svg.SVGGlyph;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import timeranger.ColorUtil;
import timeranger.model.Tag;

/**
 * Custom JFoenix chip for tags
 *
 * @author PavelPopov
 */
public class TagChip<T extends Tag> extends JFXChip<T> {

    /**
     * Root HBox
     */
    protected final HBox root;

    public TagChip(JFXChipView<T> view, T tag) {
        super(view, tag);
        // Text should have a contrast with background
        Color background = ColorUtil.getColor(tag.getColor());
        Color foreground = ColorUtil.getContrastingColor(background);
        SVGGlyph glyph = new SVGGlyph();
        JFXButton closeButton = new JFXButton(null, glyph);
        closeButton.getStyleClass().add("close-button");
        closeButton.setOnAction((event) -> {
            view.getChips().remove(tag);
        });
        Label label = new Label(getItem().getName());
        label.setWrapText(true);
        root = new HBox(label, closeButton);
        getChildren().setAll(root);
        label.setMaxWidth(100);
        root.setStyle("-fx-background-color: " + ColorUtil.getHex(background));
        label.setTextFill(Paint.valueOf(ColorUtil.getHex(foreground)));
        closeButton.setStyle("-fx-background-color: " + ColorUtil.getHex(foreground));
        glyph.setFill(Paint.valueOf(ColorUtil.getHex(background)));
    }
}
