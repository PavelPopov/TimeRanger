package timeranger.view.factory;

import javafx.scene.control.Tooltip;
import jfxtras.internal.scene.control.skin.agenda.AgendaDaySkin;
import jfxtras.internal.scene.control.skin.agenda.AgendaDaysFromDisplayedSkin;
import jfxtras.scene.control.ImageViewButton;
import jfxtras.scene.control.agenda.Agenda;
import jfxtras.scene.layout.HBox;

/**
 * This controls renders a small icon menu, where the user can select which skin
 * to use in the associated Agenda control.
 * <p>
 * It's a customized version of original JFoenix AgendaSkinSwitcher
 */
public final class AgendaSkinSwitcher extends HBox {

    /**
     * Actual agenda
     */
    private Agenda agenda;

    public AgendaSkinSwitcher() {
    }

    public AgendaSkinSwitcher(Agenda agenda) {
        setAgenda(agenda);
    }

    // When JFxtras is based on 1.8.0_40+: @Override
    @Override
    public final String getUserAgentStylesheet() {
        return AgendaSkinSwitcher.class.getResource("/jfxtras/internal/scene/control/skin/agenda/"
                + AgendaSkinSwitcher.class.getSimpleName() + ".css").toExternalForm();
    }

    /**
     * Create day skin button
     *
     * @param agenda agenda
     * @return button
     */
    private ImageViewButton createDayButton(Agenda agenda) {
        ImageViewButton button = createIcon("week", "Week view");
        button.setOnMouseClicked((actionEvent) -> {
            agenda.setSkin(new AgendaWeekSkin(agenda));
        });
        return button;
    }

    /**
     * Create week skin button
     *
     * @param agenda agenda
     * @return button
     */
    private ImageViewButton createWeekButton(Agenda agenda) {
        ImageViewButton button = createIcon("day", "Day view");
        button.setOnMouseClicked((actionEvent) -> {
            agenda.setSkin(new AgendaDaySkin(agenda));
        });
        return button;
    }

    /**
     * Create month skin button
     *
     * @param agenda agenda
     * @return button
     */
    private ImageViewButton createMonthButton(Agenda agenda) {
        ImageViewButton button = createIcon("month", "Month view");
        button.setOnMouseClicked((actionEvent) -> {
            agenda.setSkin(new AgendaMonthSkin(agenda));
        });
        return button;
    }

    /**
     * Create dynamic skin button
     *
     * @param agenda agenda
     * @return button
     */
    private ImageViewButton createDayDynamicButton(Agenda agenda) {
        ImageViewButton button = createIcon("dayDynamic", "Dynamic days view");
        button.setOnMouseClicked((actionEvent) -> {
            agenda.setSkin(new AgendaDaysFromDisplayedSkin(agenda));
        });
        return button;
    }

    /**
     * Create button icon
     *
     * @param type style type
     * @param tooltip tooltip
     * @return button
     */
    // TODO: As of 1.8.0_40 CSS files are added in the scope of a control,
    // this class does not fall under the Agenda control, so it must have its own stylesheet.
    private ImageViewButton createIcon(String type, String tooltip) {
        ImageViewButton imageView = new ImageViewButton();
        imageView.getStyleClass().add(type + "-icon");
        imageView.setPickOnBounds(true);
        Tooltip.install(imageView, new Tooltip(tooltip));
        return imageView;
    }

    /**
     * @return the agenda
     */
    public final Agenda getAgenda() {
        return agenda;
    }

    /**
     * Set agenda and add skin switcher buttons
     *
     * @param agenda the agenda to set
     */
    public final void setAgenda(Agenda agenda) {
        this.agenda = agenda;
        getStyleClass().add(AgendaSkinSwitcher.class.getSimpleName());
        getChildren().add(createWeekButton(agenda));
        getChildren().add(createDayButton(agenda));
        getChildren().add(createMonthButton(agenda));
        getChildren().add(createDayDynamicButton(agenda));
    }

}
