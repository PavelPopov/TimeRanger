package timeranger.view.factory;

import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import jfxtras.internal.scene.control.skin.agenda.base24hour.AgendaSkinTimeScale24HourAbstract;
import jfxtras.scene.control.agenda.Agenda;

/**
 * Customized version of original JFoenix AgendaWeekSkin
 *
 * @author Tom Eugelink
 * @author PavelPopov
 */
public class AgendaWeekSkin extends AgendaSkinTimeScale24HourAbstract<AgendaWeekSkin> {

    public AgendaWeekSkin(Agenda control) {
        super(control);
    }

    /**
     * Assign a calendar to each day, so it knows what it must draw.
     */
    protected List<LocalDate> determineDisplayedLocalDates() {
        List<LocalDate> localDates = new ArrayList<>();
        // 7 days stating at the first day of week
        LocalDate startLocalDate = getFirstDayOfWeekLocalDate();
        for (int i = 0; i < 7; i++) {
            localDates.add(startLocalDate.plusDays(i));
        }
        return localDates;
    }

    /**
     * get the date of the first day of the week
     */
    public LocalDate getFirstDayOfWeekLocalDate() {
        Locale locale = getSkinnable().getLocale();
        WeekFields weekFields = WeekFields.of(locale);
        int firstDayOfWeek = weekFields.getFirstDayOfWeek().getValue();
        LocalDate displayedDateTime = getSkinnable().getDisplayedLocalDateTime().toLocalDate();
        int currentDayOfWeek = displayedDateTime.getDayOfWeek().getValue();
        if (firstDayOfWeek <= currentDayOfWeek) {
            displayedDateTime = displayedDateTime.plusDays(-currentDayOfWeek + firstDayOfWeek);
        } else {
            displayedDateTime = displayedDateTime.plusDays(-currentDayOfWeek - (7 - firstDayOfWeek));
        }
        return displayedDateTime;
    }

}
