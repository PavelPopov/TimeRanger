package timeranger.view.factory;

import com.jfoenix.controls.JFXAutoCompletePopup;
import com.jfoenix.controls.JFXChipView;
import javafx.collections.ObservableList;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Skin;
import javafx.util.Callback;

/**
 * TagChipArea is the material design implementation of chip Input.
 * An easy way to manage chips in a text area component with an x to
 * omit the chip.
 *
 * @author Shadi Shaheen & Gerard Moubarak
 * @author Pavel Popov
 * @version 1.0.0
 * @since 2018-02-01
 */
public class TagChipView<T> extends JFXChipView<T> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Skin<?> createDefaultSkin() {
        return new TagChipViewSkin<T>(this);
    }

    private JFXAutoCompletePopup<T> autoCompletePopup = new TagChipViewSkin.ChipsAutoComplete<T>();

    @Override
    public JFXAutoCompletePopup<T> getAutoCompletePopup() {
        return autoCompletePopup;
    }

    @Override
    public ObservableList<T> getSuggestions() {
        return autoCompletePopup.getSuggestions();
    }

    @Override
    public void setSuggestionsCellFactory(Callback<ListView<T>, ListCell<T>> factory) {
        autoCompletePopup.setSuggestionsCellFactory(factory);
    }
}
