package timeranger.view.factory;

import com.jfoenix.controls.JFXCheckBox;
import javafx.beans.Observable;
import javafx.event.Event;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.paint.Paint;

/**
 * Custom editable check box cell
 *
 * @author PavelPopov
 */
public class EditableTableCheckBoxCell<S>
        extends AbstractEditableTableCell<S, Boolean, JFXCheckBox> {

    /**
     * Flag indicating selection update process used to skip listeners actions
     */
    private boolean updatingSelection = false;

    public EditableTableCheckBoxCell(TableColumn<S, Boolean> col) {
        super(col);
        field = new JFXCheckBox();
        field.setMinWidth(30);
        field.setMinHeight(30);
        field.selectedProperty().addListener((Observable observable) -> {
            if (!updatingSelection) {
                commitHelper(false);
            }
        });
    }

    /**
     * Set colors of check box
     *
     * @param checked color when checked
     * @param unchecked color when unchecked
     */
    public final void setColors(Paint checked, Paint unchecked) {
        field.setCheckedColor(checked);
        field.setUnCheckedColor(unchecked);
    }

    @Override
    protected Boolean getValue() {
        return getItem() == null ? false : getItem();
    }

    @Override
    protected void commitHelper(boolean losingFocus) {
        commitEdit(field.isSelected());
    }

    @Override
    public void commitEdit(Boolean item) {
        TableView<S> table = getTableView();
        if (table != null) {
            TableColumn<S, Boolean> column = getTableColumn();
            CellEditEvent<S, Boolean> event = new CellEditEvent<>(
                    table, new TablePosition(table, getIndex(), column),
                    TableColumn.editCommitEvent(), item);
            Event.fireEvent(column, event);
        }
    }

    @Override
    public void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            updatingSelection = true;
            field.setSelected(getValue());
            updatingSelection = false;
            setGraphic(field);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        }
    }
}
