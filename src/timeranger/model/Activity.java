package timeranger.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import timeranger.TimeUtil;

/**
 * Activity model
 *
 * @author PavelPopov
 */
public class Activity implements Model {

    /**
     * Identificator of activity
     */
    private IntegerProperty id;
    /**
     * Name of activity
     */
    private StringProperty name;
    /**
     * Activity's start timestamp
     */
    private ObjectProperty<LocalDateTime> start;
    /**
     * Activity's end timestamp
     */
    private ObjectProperty<LocalDateTime> end;
    /**
     * Tags of activity
     */
    private ListProperty<Tag> tags;

    public Activity() {
    }

    public Activity(Activity activity) {
        this(activity.getId(), activity.getName(), activity.getStart(), activity.getEnd(),
                activity.getTags() == null ? null : new ArrayList(activity.getTags()));
    }

    public Activity(int id, String name,
            LocalDateTime start, LocalDateTime end, List<Tag> tags) {
        setId(id);
        setName(name);
        setStart(start);
        setEnd(end);
        setTags(tags);
    }

    /**
     * Set identificator
     *
     * @param value id
     */
    public final void setId(int value) {
        idProperty().set(value);
    }

    /**
     * Set name
     *
     * @param value name
     */
    public final void setName(String value) {
        nameProperty().set(value);
    }

    /**
     * Set start timestamp
     *
     * @param value start timestamp
     */
    public final void setStart(LocalDateTime value) {
        startProperty().set(value);
    }

    /**
     * Set end timestamp
     *
     * @param value end timestamp
     */
    public final void setEnd(LocalDateTime value) {
        endProperty().set(value);
    }

    /**
     * Set tags
     *
     * @param value tags
     */
    public final void setTags(List<Tag> value) {
        if (!(value instanceof ObservableList)) {
            value = value == null ? FXCollections.observableArrayList()
                    : FXCollections.observableArrayList(value);
        }
        tagsProperty().set((ObservableList) value);
    }

    /**
     * Get identificator
     *
     * @return id
     */
    public int getId() {
        return idProperty().get();
    }

    /**
     * Get name
     *
     * @return name
     */
    public String getName() {
        return nameProperty().get();
    }

    /**
     * Get start timestamp
     *
     * @return start timestamp
     */
    public LocalDateTime getStart() {
        return startProperty().get();
    }

    /**
     * Get end timestamp
     *
     * @return end timestamp
     */
    public LocalDateTime getEnd() {
        return endProperty().get();
    }

    /**
     * Get tags
     *
     * @return tags list
     */
    public ObservableList<Tag> getTags() {
        return tagsProperty().get();
    }

    /**
     * Get identificator property
     *
     * @return id property
     */
    public IntegerProperty idProperty() {
        if (id == null) {
            id = new SimpleIntegerProperty(this, "id");
        }
        return id;
    }

    /**
     * Get name property
     *
     * @return name property
     */
    public StringProperty nameProperty() {
        if (name == null) {
            name = new SimpleStringProperty(this, "name");
        }
        return name;
    }

    /**
     * Get start timestamp property
     *
     * @return start timestamp property
     */
    public ObjectProperty<LocalDateTime> startProperty() {
        if (start == null) {
            start = new SimpleObjectProperty(this, "start");
        }
        return start;
    }

    /**
     * Get end timestamp property
     *
     * @return end timestamp property
     */
    public ObjectProperty<LocalDateTime> endProperty() {
        if (end == null) {
            end = new SimpleObjectProperty(this, "end");
        }
        return end;
    }

    /**
     * Get tags property
     *
     * @return tags property
     */
    public ListProperty<Tag> tagsProperty() {
        if (tags == null) {
            tags = new SimpleListProperty(this, "tags");
        }
        return tags;
    }

    /**
     * Get duration of activity in seconds
     *
     * @return duration of activity
     */
    public long duration() {
        return TimeUtil.getDifference(getStart(), getEnd());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Activity)) {
            return false;
        }
        Activity a = (Activity) obj;
        return a.idProperty().getValue().equals(idProperty().getValue());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.idProperty().getValue());
        return hash;
    }

    @Override
    public void deserialize(ResultSet result) throws SQLException {
        Timestamp startTS = result.getTimestamp("start");
        Timestamp endTS = result.getTimestamp("end");
        setId(result.getInt("id"));
        setName(result.getString("name"));
        setStart(startTS == null ? null : startTS.toLocalDateTime());
        setEnd(endTS == null ? null : endTS.toLocalDateTime());
    }

}
