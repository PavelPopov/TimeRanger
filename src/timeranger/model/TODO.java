package timeranger.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * TODO model
 *
 * @author PavelPopov
 */
public class TODO implements Model {

    /**
     * Name of TODO
     */
    private StringProperty name;

    public TODO() {
    }

    public TODO(TODO todo) {
        this(todo.getName());
    }

    public TODO(String name) {
        setName(name);
    }

    /**
     * Get name
     *
     * @return name
     */
    public String getName() {
        return nameProperty().getValue();
    }

    /**
     * Set name
     *
     * @param name name
     */
    public void setName(String name) {
        nameProperty().setValue(name);
    }

    /**
     * Get name property
     *
     * @return name property
     */
    public StringProperty nameProperty() {
        if (name == null) {
            name = new SimpleStringProperty(this, "name");
        }
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof TODO)) {
            return false;
        }
        TODO t = (TODO) obj;
        return t.nameProperty().getValue().equals(nameProperty().getValue());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(nameProperty().getValue());
        return hash;
    }

    @Override
    public void deserialize(ResultSet result) throws SQLException {
        setName(result.getString("name"));
    }

}
