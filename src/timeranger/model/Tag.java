package timeranger.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Tag model
 *
 * @author PavelPopov
 */
public class Tag implements Model {

    /**
     * Property of tag's identificator
     */
    private IntegerProperty id;
    /**
     * Property of tag's name
     */
    private StringProperty name;
    /**
     * Property of integer representing tag's color
     */
    private IntegerProperty color;
    /**
     * Property of tag's description
     */
    private StringProperty description;

    public Tag() {
    }

    public Tag(Tag tag) {
        this(tag.getId(), tag.getName(), tag.getColor(), tag.getDescription());
    }

    public Tag(int id, String name, int color, String description) {
        setId(id);
        setName(name);
        setColor(color);
        setDescription(description);
    }

    /**
     * Set identificator
     *
     * @param value id
     */
    public final void setId(int value) {
        idProperty().set(value);
    }

    /**
     * Set name
     *
     * @param value name
     */
    public final void setName(String value) {
        nameProperty().set(value);
    }

    /**
     * Set color
     *
     * @param value integer representing color
     */
    public final void setColor(int value) {
        colorProperty().set(value);
    }

    /**
     * Set description
     *
     * @param value description
     */
    public final void setDescription(String value) {
        descriptionProperty().set(value);
    }

    /**
     * Get identificator
     *
     * @return id
     */
    public int getId() {
        return idProperty().get();
    }

    /**
     * Get name
     *
     * @return name
     */
    public String getName() {
        return nameProperty().get();
    }

    /**
     * Get color
     *
     * @return integer representing value
     */
    public int getColor() {
        return colorProperty().get();
    }

    /**
     * Get description
     *
     * @return description
     */
    public String getDescription() {
        return descriptionProperty().get();
    }

    /**
     * Get identificator property
     *
     * @return identificator property
     */
    public IntegerProperty idProperty() {
        if (id == null) {
            id = new SimpleIntegerProperty(this, "id");
        }
        return id;
    }

    /**
     * Get name property
     *
     * @return name property
     */
    public StringProperty nameProperty() {
        if (name == null) {
            name = new SimpleStringProperty(this, "name");
        }
        return name;
    }

    /**
     * Get color property
     *
     * @return color property
     */
    public IntegerProperty colorProperty() {
        if (color == null) {
            color = new SimpleIntegerProperty(this, "color");
        }
        return color;
    }

    /**
     * Get description property
     *
     * @return description property
     */
    public StringProperty descriptionProperty() {
        if (description == null) {
            description = new SimpleStringProperty(this, "description");
        }
        return description;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Tag)) {
            return false;
        }
        Tag t = (Tag) obj;
        return t.idProperty().getValue().equals(idProperty().getValue());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.idProperty().getValue());
        return hash;
    }

    @Override
    public void deserialize(ResultSet result) throws SQLException {
        if (result.getInt("id") > 0) {
            setId(result.getInt("id"));
        }
        setName(result.getString("name"));
        setColor(result.getInt("color"));
        setDescription(result.getString("description"));
    }

    @Override
    public String toString() {
        return nameProperty().getValue();
    }

}
