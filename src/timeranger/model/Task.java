package timeranger.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Task model
 *
 * @author PavelPopov
 */
public class Task implements Model {

    /**
     * Property of task's identificator
     */
    private IntegerProperty id;
    /**
     * Property of task's name
     */
    private StringProperty name;
    /**
     * Property of task's description
     */
    private StringProperty description;
    /**
     * Property of task's deadline timestamp
     */
    private ObjectProperty<LocalDateTime> deadline;
    /**
     * Property of task's tags
     */
    private ListProperty<Tag> tags;
    /**
     * Property of task's done timestamp
     */
    private ObjectProperty<LocalDateTime> done;

    public Task() {
    }

    public Task(Task task) {
        this(task.getId(), task.getName(), task.getDescription(), task.getDeadline(),
                task.getTags() == null ? null : new ArrayList(task.getTags()), task.getDone());
    }

    public Task(int id, String name, String description,
            LocalDateTime deadline, List<Tag> tags, LocalDateTime done) {
        setId(id);
        setName(name);
        setDescription(description);
        setDeadline(deadline);
        setTags(tags);
        setDone(done);
    }

    /**
     * Set identificator
     *
     * @param value id
     */
    public final void setId(int value) {
        idProperty().set(value);
    }

    /**
     * Set name
     *
     * @param value name
     */
    public final void setName(String value) {
        nameProperty().set(value);
    }

    /**
     * Set description
     *
     * @param value description
     */
    public final void setDescription(String value) {
        descriptionProperty().set(value);
    }

    /**
     * Set tags
     *
     * @param value tags list
     */
    public final void setTags(List<Tag> value) {
        if (!(value instanceof ObservableList)) {
            value = value == null ? FXCollections.observableArrayList()
                    : FXCollections.observableArrayList(value);
        }
        tagsProperty().set((ObservableList) value);
    }

    /**
     * Set deadline timestamp
     *
     * @param value deadline timestamp
     */
    public final void setDeadline(LocalDateTime value) {
        deadlineProperty().set(value);
    }

    /**
     * Set done timestamp
     *
     * @param value done timestamp
     */
    public final void setDone(LocalDateTime value) {
        doneProperty().set(value);
    }

    /**
     * Get identificator
     *
     * @return id
     */
    public int getId() {
        return idProperty().get();
    }

    /**
     * Get name
     *
     * @return name
     */
    public String getName() {
        return nameProperty().get();
    }

    /**
     * Get description
     *
     * @return description
     */
    public String getDescription() {
        return descriptionProperty().get();
    }

    /**
     * Get tags
     *
     * @return tags list
     */
    public ObservableList<Tag> getTags() {
        return tagsProperty().get();
    }

    /**
     * Get deadline timestamp
     *
     * @return deadline timestamp
     */
    public LocalDateTime getDeadline() {
        return deadlineProperty().get();
    }

    /**
     * Get done timestamp
     *
     * @return done timestamp
     */
    public LocalDateTime getDone() {
        return doneProperty().get();
    }

    /**
     * Get identificator property
     *
     * @return identificator property
     */
    public IntegerProperty idProperty() {
        if (id == null) {
            id = new SimpleIntegerProperty(this, "id");
        }
        return id;
    }

    /**
     * Get name property
     *
     * @return name property
     */
    public StringProperty nameProperty() {
        if (name == null) {
            name = new SimpleStringProperty(this, "name");
        }
        return name;
    }

    /**
     * Get description property
     *
     * @return description property
     */
    public StringProperty descriptionProperty() {
        if (description == null) {
            description = new SimpleStringProperty(this, "description");
        }
        return description;
    }

    /**
     * Get tags property
     *
     * @return tags property
     */
    public ListProperty<Tag> tagsProperty() {
        if (tags == null) {
            tags = new SimpleListProperty(this, "tags");
        }
        return tags;
    }

    /**
     * Get deadline property
     *
     * @return deadline property
     */
    public ObjectProperty<LocalDateTime> deadlineProperty() {
        if (deadline == null) {
            deadline = new SimpleObjectProperty(this, "deadline");
        }
        return deadline;
    }

    /**
     * Get done property
     *
     * @return done property
     */
    public ObjectProperty<LocalDateTime> doneProperty() {
        if (done == null) {
            done = new SimpleObjectProperty(this, "done");
        }
        return done;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Task)) {
            return false;
        }
        Task t = (Task) obj;
        return t.idProperty().getValue().equals(idProperty().getValue());
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.idProperty().getValue());
        return hash;
    }

    @Override
    public void deserialize(ResultSet result) throws SQLException {
        Timestamp deadlineTS = result.getTimestamp("deadline");
        Timestamp doneTS = result.getTimestamp("done");
        setId(result.getInt("id"));
        setName(result.getString("name"));
        setDescription(result.getString("description"));
        setDeadline(deadlineTS == null ? null : deadlineTS.toLocalDateTime());
        setDone(doneTS == null ? null : doneTS.toLocalDateTime());
    }
}
