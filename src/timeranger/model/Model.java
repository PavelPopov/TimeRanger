package timeranger.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Base model interface
 *
 * @author PavelPopov
 */
public interface Model {

    /**
     * Deserialize result set from database into model
     *
     * @param result result set
     * @throws SQLException if an SQL error occurs while deserializing
     */
    public void deserialize(ResultSet result) throws SQLException;
}
