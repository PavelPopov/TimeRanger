package timeranger;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Helper methods for time conversion and obtaining properties
 *
 * @author PavelPopov
 */
public final class TimeUtil {

    /**
     * Get difference in seconds between start and end timestamps.
     * <p>
     * If end is null - current timestamp is used
     *
     * @param start start timestamp
     * @param end end timestamp
     * @return difference in seconds
     */
    public final static long getDifference(LocalDateTime start, LocalDateTime end) {
        LocalDateTime now = LocalDateTime.now();
        if (end == null) {
            end = now;
        }
        return ChronoUnit.SECONDS.between(start, end);
    }
}
