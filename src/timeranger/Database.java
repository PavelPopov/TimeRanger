package timeranger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import org.sqlite.util.StringUtils;
import timeranger.model.Activity;
import timeranger.model.Model;
import timeranger.model.TODO;
import timeranger.model.Tag;
import timeranger.model.Task;

/**
 * SQLite Database Access Object.
 * <p>
 * 1. Establish connection supplying SQLite database file {@code Database.connect("/sqlite.db")}<br>
 * 2. Call {@code createSchema()} to create database schema if it doesn't exist<br>
 * 3. Now you are ready to go!
 *
 * @author PavelPopov
 */
public class Database {

    /**
     * Connection session
     */
    private Connection connection = null;

    //
    // Private helpers
    //
    /**
     * Throw exception if there's no connection.
     * <p>
     * INTERNAL USE ONLY!
     *
     * @throws IllegalStateException if there's no database connection
     */
    private void checkConnection() {
        if (connection == null) {
            throw new IllegalStateException("No database connection");
        }
    }

    /**
     * Prepare statement from SQL and parameters map.
     * <p>
     * INTERNAL USE ONLY!
     *
     * @param sql SQL query string
     * @param params parameters map
     * @param returnKeys return generated keys flag
     * @return prepared statement
     * @throws SQLException if an SQL error occurred while preparing statement
     */
    private PreparedStatement prepareStmt(String sql,
            LinkedHashMap<Object, Integer> params, boolean returnKeys)
            throws SQLException {
        PreparedStatement stmt = returnKeys
                ? connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
                : connection.prepareStatement(sql);
        int i = 1;
        for (Entry<Object, Integer> param : params.entrySet()) {
            stmt.setObject(i++, param.getKey(), param.getValue());
        }
        return stmt;
    }

    /**
     * Prepare statement using SQL and parameters map, and execute it.
     * <p>
     * INTERNAL USE ONLY!
     *
     * @param sql SQL query string
     * @param params parameters map
     * @param returnKeys return generated keys flag
     * @return executed prepared statement
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if an SQL error occurred while preparing or executing statement
     */
    private PreparedStatement exec(String sql,
            LinkedHashMap<Object, Integer> params, boolean returnKeys) {
        checkConnection();
        try {
            PreparedStatement stmt = prepareStmt(sql, params, returnKeys);
            stmt.execute();
            return stmt;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to perform a query", ex);
        }
    }

    /**
     * Prepare update statement using SQL and parameters map, and execute it.
     * <p>
     * INTERNAL USE ONLY!
     *
     * @param sql SQL query string
     * @param params parameters map
     * @param returnKeys return generated keys flag
     * @return executed prepared statement
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if nothing was done or
     * an SQL error occurred while preparing or executing statement
     */
    private PreparedStatement update(String sql,
            LinkedHashMap<Object, Integer> params, boolean returnKeys) {
        checkConnection();
        try {
            PreparedStatement stmt = prepareStmt(sql, params, returnKeys);
            if (stmt.executeUpdate() < 1) {
                throw new RuntimeException("Nothing done");
            }
            return stmt;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to perform a query", ex);
        }
    }

    /**
     * Construct and execute multiple deletion query.
     * <p>
     * INTERNAL USE ONLY!
     *
     * @param table table name
     * @param key primary key name
     * @param params parameters map
     * @return executed prepared statement
     * @throws IllegalStateException if there's no database connection
     * @throws IllegalArgumentException if params are empty
     * @throws RuntimeException if deletion is incomplete or
     * if an SQL error occurred while preparing or executing statement
     */
    private PreparedStatement deleteMultiple(String table, String key, LinkedHashMap<Object, Integer> params) {
        checkConnection();
        if (params.size() < 1) {
            throw new IllegalArgumentException("No objects supplied for deletion");
        }
        String[] keyStrArr = new String[params.size()];
        Arrays.fill(keyStrArr, "?");
        String keyStr = StringUtils.join(Arrays.asList(keyStrArr), ", ");
        String sql = "DELETE FROM " + table + " WHERE " + key + " IN (" + keyStr + ")";
        try {
            PreparedStatement stmt = prepareStmt(sql, params, false);
            if (stmt.executeUpdate() < params.size()) {
                throw new RuntimeException("Some objects were not deleted");
            }
            return stmt;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to perform a query", ex);
        }
    }

    /**
     * Construct models from query result set.
     * <p>
     * INTERNAL USE ONLY!
     *
     * @param <T> model
     * @param result query result set
     * @param modelClass class of model
     * @return list containing resulting models
     * @throws RuntimeException if an error occurred while instantiating model or
     * while getting or deserializing results
     */
    private <T extends Model> ArrayList<T> obtainResults(ResultSet result, Class<T> modelClass) {
        try {
            ArrayList<T> objects = new ArrayList();
            while (result.next()) {
                T model = modelClass.newInstance();
                model.deserialize(result);
                objects.add(model);
            }
            return objects;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to get or deserialize results", ex);
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException("Failed to instantiate a model", ex);
        }
    }

    /**
     * Construct models from prepared statement result set.
     * <p>
     * INTERNAL USE ONLY!
     *
     * @param <T> model
     * @param stmt prepared statement containing results set
     * @param modelClass class of model
     * @return list containing resulting models
     * @throws RuntimeException if an error occurred while instantiating model or
     * while getting or deserializing results
     */
    private <T extends Model> ArrayList<T> obtainResults(PreparedStatement stmt, Class<T> modelClass) {
        try {
            return obtainResults(stmt.getResultSet(), modelClass);
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to get result set", ex);
        }
    }

    /**
     * Select all rows from table and construct their models.
     * <p>
     * INTERNAL USE ONLY!
     *
     * @param <T> model
     * @param table table name
     * @param modelClass class of model
     * @return list containing resulting models
     * @throws RuntimeException if an error occurred while instantiating model or
     * while getting or deserializing results
     */
    private <T extends Model> ArrayList<T> selectAll(String table, Class<T> modelClass) {
        checkConnection();
        String sql = "SELECT * FROM " + table;
        try {
            ResultSet result = connection.createStatement().executeQuery(sql);
            return obtainResults(result, modelClass);
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to perform a query", ex);
        }
    }

    //
    // Public API
    //
    /**
     * Connect to SQLite database.
     * <p>
     * Tries to connect to database, sets up {@code connection} and configures it
     *
     * @param database database file location
     * @throws ClassNotFoundException if JDBC driver is not supplied in classpath
     * @throws SQLException if an SQL error occurred while connecting to database
     */
    public void connect(String database) {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + database);
            Statement statement = connection.createStatement();
            try {
                statement.execute("PRAGMA foreign_keys = ON;");
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to enable foreign keys");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException("Failed to connect to database", ex);
        }
    }

    /**
     * Close connection.
     * <p>
     * Tries to close connection if it was established before
     *
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if an SQL error occurred while closing connection
     */
    public void close() {
        checkConnection();
        try {
            connection.close();
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to close database connection", ex);
        }
    }

    /**
     * Is connection established.
     *
     * @return TRUE if connection is established, FALSE otherwise
     */
    public boolean isConnected() {
        return connection != null;
    }

    /**
     * Create database schema.
     * <p>
     * Tries to create database schema. Creates only nonexistent tables.
     *
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if an SQL error occurred while creating tables
     */
    public void createSchema() {
        checkConnection();
        try (Statement statement = connection.createStatement()) {
            try {
                statement.execute("CREATE TABLE IF NOT EXISTS todos "
                        + "(name VARCHAR(255) NOT NULL, "
                        + "PRIMARY KEY(name) ON CONFLICT REPLACE);");
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to create activities table", ex);
            }
            try {
                statement.execute("CREATE TABLE IF NOT EXISTS activities "
                        + "(id INTEGER PRIMARY KEY NOT NULL, "
                        + "name VARCHAR(255) NOT NULL, "
                        + "start TIMESTAMP NOT NULL, "
                        + "end TIMESTAMP);");
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to create activities table", ex);
            }
            try {
                statement.execute("CREATE TABLE IF NOT EXISTS tasks "
                        + "(id INTEGER PRIMARY KEY NOT NULL, "
                        + "name VARCHAR(255) NOT NULL, "
                        + "description TEXT NOT NULL, "
                        + "deadline TIMESTAMP, "
                        + "done TIMESTAMP, "
                        + "UNIQUE (name) ON CONFLICT REPLACE);");
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to create tasks table", ex);
            }
            try {
                statement.execute("CREATE TABLE IF NOT EXISTS tags "
                        + "(id INTEGER PRIMARY KEY NOT NULL, "
                        + "name VARCHAR(64) NOT NULL, "
                        + "color INTEGER NOT NULL, "
                        + "description TEXT NOT NULL, "
                        + "UNIQUE (name) ON CONFLICT REPLACE);");
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to create tags table");
            }
            try {
                statement.execute("CREATE TABLE IF NOT EXISTS tasksTags "
                        + "(task INTEGER NOT NULL, "
                        + "tag INTEGER NOT NULL, "
                        + "PRIMARY KEY(task, tag) ON CONFLICT IGNORE, "
                        + "FOREIGN KEY(task) REFERENCES tasks(id) ON DELETE CASCADE, "
                        + "FOREIGN KEY(tag) REFERENCES tags(id) ON DELETE CASCADE);");
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to create tasksTags table", ex);
            }
            try {
                statement.execute("CREATE TABLE IF NOT EXISTS activitiesTags "
                        + "(activity INTEGER NOT NULL, "
                        + "tag INTEGER NOT NULL, "
                        + "PRIMARY KEY(activity, tag) ON CONFLICT IGNORE, "
                        + "FOREIGN KEY(activity) REFERENCES activities(id) ON DELETE CASCADE, "
                        + "FOREIGN KEY(tag) REFERENCES tags(id) ON DELETE CASCADE);");
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to create activitiesTags table", ex);
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to create or close statements", ex);
        }
    }

    /**
     * Select all TODOs from database
     *
     * @return list of TODOs
     * @throws RuntimeException if an error occurred while instantiating model or
     * while getting or deserializing results
     */
    public ArrayList<TODO> selectAllTODOs() {
        return selectAll("todos", TODO.class);
    }

    /**
     * Insert a TODO to database
     *
     * @param todo TODO model
     * @return supplied TODO
     * @throws RuntimeException if nothing was done or
     * an SQL error occurred while preparing or executing statement
     */
    public TODO insertTODO(TODO todo) {
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(todo.getName(), Types.VARCHAR);
        update("INSERT INTO todos (name) VALUES (?);", params, false);
        return todo;
    }

    /**
     * Update a TODO in database
     *
     * @param key TODO primary key value
     * @param todo TODO model
     * @return supplied TODO
     * @throws RuntimeException if nothing was done or
     * an SQL error occurred while preparing or executing statement
     */
    public TODO updateTODO(String key, TODO todo) {
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(todo.getName(), Types.VARCHAR);
        params.put(key, Types.VARCHAR);
        update("UPDATE todos SET name = ? WHERE name = ?", params, false);
        return todo;
    }

    /**
     * Delete multiple TODOs from database
     *
     * @param todos list of TODO models
     * @throws RuntimeException if deletion is incomplete or
     * if an SQL error occurred while preparing or executing statement
     */
    public void deleteTODOs(List<TODO> todos) {
        LinkedHashMap<Object, Integer> keys = new LinkedHashMap();
        for (int i = 0; i < todos.size(); i++) {
            keys.put(todos.get(i).getName(), Types.VARCHAR);
        }
        deleteMultiple("todos", "name", keys);
    }

    /**
     * Insert an activity to database
     *
     * @param activity activity model
     * @return supplied activity
     * @throws RuntimeException if nothing was done or
     * an SQL error occurred while preparing or executing statement or getting generated keys
     */
    public Activity insertActivity(Activity activity) {
        Timestamp start = Timestamp.valueOf(activity.getStart());
        Timestamp end = null;
        if (activity.getEnd() != null) {
            end = Timestamp.valueOf(activity.getEnd());
        }
        String sql = "INSERT INTO activities (name, start, end) VALUES (?, ?, ?);";
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(activity.getName(), Types.VARCHAR);
        params.put(start, Types.TIMESTAMP);
        params.put(end, end != null ? Types.TIMESTAMP : Types.NULL);
        PreparedStatement stmt = update(sql, params, true);
        try {
            activity.setId(stmt.getGeneratedKeys().getInt(1));
            return activity;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to get generated keys");
        }
    }

    /**
     * Update activity in database
     *
     * @param activity activity model
     * @return supplied activity
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if nothing was done or
     * an SQL error occurred while preparing or executing statement
     */
    public Activity updateActivity(Activity activity) {
        // TODO: SQLite don't want to save everything if something had not changed.
        // Find a way to optimize this.
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(Timestamp.valueOf(activity.getStart()), Types.TIMESTAMP);
        params.put(activity.getId(), Types.INTEGER);
        String sql = "UPDATE activities SET start = ? WHERE id = ?";
        exec(sql, params, false);
        if (activity.getEnd() != null) {
            params = new LinkedHashMap();
            params.put(Timestamp.valueOf(activity.getEnd()), Types.TIMESTAMP);
            params.put(activity.getId(), Types.INTEGER);
            sql = "UPDATE activities SET end = ? WHERE id = ?";
            exec(sql, params, false);
        }
        params = new LinkedHashMap();
        params.put(activity.getName(), Types.VARCHAR);
        params.put(activity.getId(), Types.INTEGER);
        sql = "UPDATE activities SET name = ? WHERE id = ?";
        exec(sql, params, false);
        return activity;
    }

    /**
     * Select filtered activities from database.
     * <p>
     * Selecting activities having no end and excluding end time bound from time interval
     *
     * @param start start bound
     * @param end end bound
     * @param filter name filter
     * @return list of selected activities
     * @throws RuntimeException if an SQL error occurred while preparing or executing statement or
     * if an error occurred while instantiating model or while getting or deserializing results
     */
    public ArrayList<Activity> selectActivities(LocalDateTime start,
            LocalDateTime end, String filter) {
        return selectActivities(start, end, filter, true, true);
    }

    /**
     * Select filtered activities from database
     *
     * @param start start bound
     * @param end end bound
     * @param filter name filter
     * @param selectLast include current activities flag
     * @param endExclusive exclude end timestamp from filter time interval
     * @return list of selected activities
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if an SQL error occurred while preparing or executing statement or
     * if an error occurred while instantiating model or while getting or deserializing results
     */
    public ArrayList<Activity> selectActivities(LocalDateTime start,
            LocalDateTime end, String filter,
            boolean selectLast, boolean endExclusive) {
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        String sql = "SELECT * FROM activities WHERE ";
        filter = filter.trim();
        if (!filter.isEmpty()) {
            sql += "name LIKE ? AND ";
        }
        if (!filter.isEmpty()) {
            params.put("%" + filter + "%", Types.VARCHAR);
        }
        int mode = 0;
        if (start == null) {
            mode += 1;
        }
        if (end == null) {
            mode += 2;
        }
        switch (mode) {
            case 0: // all defined
            {
                boolean today = end.toLocalDate().isAfter(LocalDate.now()) && selectLast;
                sql += "start >= ? AND (" + (today ? "end IS NULL or "
                        : "end IS NOT NULL and ") + "end "
                        + (endExclusive ? "<" : "<=") + " ?) ORDER BY start;";
                params.put(Timestamp.valueOf(start), Types.TIMESTAMP);
                params.put(Timestamp.valueOf(end), Types.TIMESTAMP);
            }
            break;
            case 1: // start undefined
            {
                boolean today = end.toLocalDate().isAfter(LocalDate.now()) && selectLast;
                sql += "(" + (today ? "end IS NULL or "
                        : "end IS NOT NULL and ") + "end "
                        + (endExclusive ? "<" : "<=") + " ?) ORDER BY end;";
                params.put(Timestamp.valueOf(end), Types.TIMESTAMP);
            }
            break;
            case 2: // end undefined
            {
                sql += "start >= ? AND " + (selectLast ? "end IS NULL "
                        : "end IS NOT NULL ") + "ORDER BY start;";
                params.put(Timestamp.valueOf(start), Types.TIMESTAMP);
            }
            break;
            default: // all undefined
            {
                sql += (selectLast ? "end IS NULL "
                        : "end IS NOT NULL ") + "ORDER BY name;";
            }
        }
        PreparedStatement stmt = exec(sql, params, false);
        return obtainResults(stmt, Activity.class);
    }

    /**
     * Select current activities from database
     *
     * @return list of selected activities
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if an SQL error occurred while preparing or executing statement or
     * if an error occurred while instantiating model or while getting or deserializing results
     */
    public ArrayList<Activity> selectCurrentActivities() {
        String sql = "SELECT * FROM activities WHERE end IS NULL OR (start < ? AND end > ?) ORDER BY id;";
        Timestamp now = Timestamp.valueOf(LocalDateTime.now());
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(now, Types.TIMESTAMP);
        params.put(now, Types.TIMESTAMP);
        PreparedStatement stmt = exec(sql, params, false);
        return obtainResults(stmt, Activity.class);
    }

    /**
     * Select first activity from database
     *
     * @return first activity
     * @throws RuntimeException if an SQL error occurred while preparing or executing statement or
     * if an error occurred while instantiating model or while getting or deserializing results
     */
    public Activity selectFirstActivity() {
        try {
            String sql = "SELECT * FROM activities WHERE end IS NOT NULL ORDER BY start LIMIT 1;";
            Statement statement = connection.createStatement();
            try {
                ResultSet result = statement.executeQuery(sql);
                ArrayList<Activity> list = obtainResults(result, Activity.class);
                return list.size() < 1 ? null : list.get(0);
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to execute query", ex);
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to create or close statements", ex);
        }
    }

    /**
     * Select last activity from database
     *
     * @return last activity
     * @throws RuntimeException if an SQL error occurred while preparing or executing statement or
     * if an error occurred while instantiating model or while getting or deserializing results
     */
    public Activity selectLastActivity() {
        try {
            String sql = "SELECT * FROM activities WHERE end IS NOT NULL ORDER BY end DESC LIMIT 1;";
            Statement statement = connection.createStatement();
            try {
                ResultSet result = statement.executeQuery(sql);
                ArrayList<Activity> list = obtainResults(result, Activity.class);
                return list.size() < 1 ? null : list.get(0);
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to execute query", ex);
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to create or close statements", ex);
        }
    }

    /**
     * Delete multiple activities from database
     *
     * @param activities list of activities models
     * @throws RuntimeException if deletion is incomplete or
     * if an SQL error occurred while preparing or executing statement
     */
    public void deleteActivities(List<Activity> activities) {
        LinkedHashMap<Object, Integer> keys = new LinkedHashMap();
        for (int i = 0; i < activities.size(); i++) {
            keys.put(activities.get(i).getId(), Types.INTEGER);
        }
        deleteMultiple("activities", "id", keys);
    }

    /**
     * Select all tags from database
     *
     * @return list of TODOs
     * @throws RuntimeException if an error occurred while instantiating model or
     * while getting or deserializing results
     */
    public ArrayList<Tag> selectAllTags() {
        return selectAll("tags", Tag.class);
    }

    /**
     * Insert a tag to database
     *
     * @param tag tag model
     * @return supplied tag
     * @throws RuntimeException if nothing was done or
     * an SQL error occurred while preparing or executing statement or getting generated keys
     */
    public Tag insertTag(Tag tag) {
        String sql = "INSERT INTO tags (name, color, description) VALUES (?, ?, ?);";
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(tag.getName(), Types.VARCHAR);
        params.put(tag.getColor(), Types.INTEGER);
        params.put(tag.getDescription(), Types.VARCHAR);
        PreparedStatement stmt = update(sql, params, true);
        try {
            tag.setId(stmt.getGeneratedKeys().getInt(1));
            return tag;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to get generated keys");
        }
    }

    /**
     * Update a tag in database
     *
     * @param tag tag model
     * @return supplied tag
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if nothing was done or
     * an SQL error occurred while preparing or executing statement
     */
    public Tag updateTag(Tag tag) {
        // TODO: SQLite don't want to save everything if something had not changed.
        // Find a way to optimize this.
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(tag.getColor(), Types.INTEGER);
        params.put(tag.getId(), Types.INTEGER);
        String sql = "UPDATE tags SET color = ? WHERE id = ?";
        exec(sql, params, false);
        params = new LinkedHashMap();
        params.put(tag.getDescription(), Types.VARCHAR);
        params.put(tag.getId(), Types.INTEGER);
        sql = "UPDATE tags SET description = ? WHERE id = ?";
        exec(sql, params, false);
        params = new LinkedHashMap();
        params.put(tag.getName(), Types.VARCHAR);
        params.put(tag.getId(), Types.INTEGER);
        sql = "UPDATE tags SET name = ? WHERE id = ?";
        exec(sql, params, false);
        return tag;
    }

    /**
     * Delete multiple tags from database
     *
     * @param tags list of tags models
     * @throws RuntimeException if deletion is incomplete or
     * if an SQL error occurred while preparing or executing statement
     */
    public void deleteTags(List<Tag> tags) {
        LinkedHashMap<Object, Integer> keys = new LinkedHashMap();
        for (int i = 0; i < tags.size(); i++) {
            keys.put(tags.get(i).getId(), Types.INTEGER);
        }
        deleteMultiple("tags", "id", keys);
    }

    /**
     * Select all tasks from database
     *
     * @return list of tasks
     * @throws RuntimeException if an error occurred while instantiating model or
     * while getting or deserializing results
     */
    public ArrayList<Task> selectAllTasks() {
        return selectAll("tasks", Task.class);
    }

    /**
     * Insert a task to database
     *
     * @param task task model
     * @return supplied task
     * @throws RuntimeException if nothing was done or
     * an SQL error occurred while preparing or executing statement
     */
    public Task insertTask(Task task) {
        Timestamp deadline = null;
        if (task.getDeadline() != null) {
            deadline = Timestamp.valueOf(task.getDeadline());
        }
        Timestamp done = null;
        if (task.getDone() != null) {
            done = Timestamp.valueOf(task.getDone());
        }
        String sql = "INSERT INTO tasks (name, description, deadline, done) VALUES (?, ?, ?, ?);";
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(task.getName(), Types.VARCHAR);
        params.put(task.getDescription(), Types.VARCHAR);
        params.put(deadline, deadline != null ? Types.TIMESTAMP : Types.NULL);
        params.put(done, done != null ? Types.TIMESTAMP : Types.NULL);
        PreparedStatement stmt = update(sql, params, true);
        try {
            task.setId(stmt.getGeneratedKeys().getInt(1));
            return task;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to get generated keys");
        }
    }

    /**
     * Update task in database
     *
     * @param task task model
     * @return supplied task
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if nothing was done or
     * an SQL error occurred while preparing or executing statement
     */
    public Task updateTask(Task task) {
        Timestamp deadline = null;
        if (task.getDeadline() != null) {
            deadline = Timestamp.valueOf(task.getDeadline());
        }
        Timestamp done = null;
        if (task.getDone() != null) {
            done = Timestamp.valueOf(task.getDone());
        }
        // TODO: SQLite don't want to save everything if something had not changed.
        // Find a way to optimize this.
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(task.getName(), Types.INTEGER);
        params.put(task.getId(), Types.INTEGER);
        String sql = "UPDATE tasks SET name = ? WHERE id = ?";
        exec(sql, params, false);
        params = new LinkedHashMap();
        params.put(task.getDescription(), Types.VARCHAR);
        params.put(task.getId(), Types.INTEGER);
        sql = "UPDATE tasks SET description = ? WHERE id = ?";
        exec(sql, params, false);
        params = new LinkedHashMap();
        params.put(deadline, deadline != null ? Types.TIMESTAMP : Types.NULL);
        params.put(task.getId(), Types.INTEGER);
        sql = "UPDATE tasks SET deadline = ? WHERE id = ?";
        exec(sql, params, false);
        params = new LinkedHashMap();
        params.put(done, done != null ? Types.TIMESTAMP : Types.NULL);
        params.put(task.getId(), Types.INTEGER);
        sql = "UPDATE tasks SET done = ? WHERE id = ?";
        exec(sql, params, false);
        return task;
    }

    /**
     * Delete multiple tasks from database
     *
     * @param tasks list of tasks models
     * @throws RuntimeException if deletion is incomplete or
     * if an SQL error occurred while preparing or executing statement
     */
    public void deleteTasks(List<Task> tasks) {
        LinkedHashMap<Object, Integer> keys = new LinkedHashMap();
        for (int i = 0; i < tasks.size(); i++) {
            keys.put(tasks.get(i).getId(), Types.INTEGER);
        }
        deleteMultiple("tasks", "id", keys);
    }

    /**
     * Select all tags of tasks from database
     *
     * @return map of tasks to tags bindings
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if an SQL error occurred while executing query
     */
    public HashMap<Task, List<Tag>> selectAllTasksTags() {
        checkConnection();
        String sql = "SELECT * FROM tasksTags";
        try {
            ResultSet result = connection.createStatement().executeQuery(sql);
            HashMap<Task, List<Tag>> tasksTags = new HashMap();
            while (result.next()) {
                int taskId = result.getInt("task");
                int tagId = result.getInt("tag");
                // TODO: remove another side effect
                Task task = null;
                for (Task t : TimeRanger.getTasks()) {
                    if (t.getId() == taskId) {
                        task = t;
                    }
                }
                if (task == null) {
                    throw new RuntimeException("Task #" + taskId + " was not found");
                }
                Tag tag = null;
                for (Tag t : TimeRanger.getTags()) {
                    if (t.getId() == tagId) {
                        tag = t;
                    }
                }
                if (tag == null) {
                    throw new RuntimeException("Tag #" + tagId + " was not found");
                }
                if (!tasksTags.containsKey(task)) {
                    tasksTags.put(task, new ArrayList());
                }
                tasksTags.get(task).add(tag);
            }
            return tasksTags;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to perform a query", ex);
        }
    }

    /**
     * Update task to tags binding in database
     *
     * @param task task model
     * @param tags list of tags to bind
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if an SQL error occurred while preparing or executing statement
     */
    public void updateTaskTags(Task task, List<Tag> tags) {
        String sql = "DELETE FROM tasksTags WHERE task = ?;";
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(task.getId(), Types.INTEGER);
        exec(sql, params, false);
        sql = "INSERT INTO tasksTags (task, tag) VALUES (?, ?);";
        for (Tag tag : tags) {
            // TODO: report a bug about setObject double integer going to null
            try {
                PreparedStatement stmt = connection.prepareStatement(sql);
                stmt.setInt(1, task.getId());
                stmt.setInt(2, tag.getId());
                if (stmt.executeUpdate() < 1) {
                    throw new RuntimeException("Nothing done");
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to perform a query", ex);
            }
        }
    }

    /**
     * Select all tags of activities from database
     *
     * @return map of activities to tags bindings
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if an SQL error occurred while executing query
     */
    public HashMap<Activity, List<Tag>> selectAllActivitiesTags() {
        checkConnection();
        String sql = "SELECT * FROM activitiesTags";
        try {
            ResultSet result = connection.createStatement().executeQuery(sql);
            HashMap<Activity, List<Tag>> activitiesTags = new HashMap();
            while (result.next()) {
                int activityId = result.getInt("activity");
                int tagId = result.getInt("tag");
                // TODO: remove another side effect
                Activity activity = null;
                for (Activity a : TimeRanger.getActivities()) {
                    if (a.getId() == activityId) {
                        activity = a;
                    }
                }
                if (activity == null) {
                    String sqlA = "SELECT * FROM activities WHERE id = " + activityId;
                    ResultSet resultA = connection.createStatement().executeQuery(sqlA);
                    ArrayList<Activity> list = obtainResults(resultA, Activity.class);
                    if (list.size() < 1) {
                        throw new RuntimeException("Activity #" + activityId + " was not found");
                    }
                    activity = list.get(0);
                }
                Tag tag = null;
                for (Tag t : TimeRanger.getTags()) {
                    if (t.getId() == tagId) {
                        tag = t;
                    }
                }
                if (tag == null) {
                    throw new RuntimeException("Tag #" + tagId + " was not found");
                }
                if (!activitiesTags.containsKey(activity)) {
                    activitiesTags.put(activity, new ArrayList());
                }
                activitiesTags.get(activity).add(tag);
            }
            return activitiesTags;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to perform a query", ex);
        }
    }

    /**
     * Update activity to tags binding in database
     *
     * @param activity activity model
     * @param tags list of tags to bind
     * @throws IllegalStateException if there's no database connection
     * @throws RuntimeException if an SQL error occurred while preparing or executing statement
     */
    public void updateActivityTags(Activity activity, List<Tag> tags) {
        String sql = "DELETE FROM activitiesTags WHERE activity = ?;";
        LinkedHashMap<Object, Integer> params = new LinkedHashMap();
        params.put(activity.getId(), Types.INTEGER);
        exec(sql, params, false);
        sql = "INSERT INTO activitiesTags(activity, tag) VALUES (?, ?);";
        for (Tag tag : tags) {
            // TODO: report a bug about setObject double integer going to null
            try {
                PreparedStatement stmt = connection.prepareStatement(sql);
                stmt.setInt(1, activity.getId());
                stmt.setInt(2, tag.getId());
                if (stmt.executeUpdate() < 1) {
                    throw new RuntimeException("Nothing done");
                }
            } catch (SQLException ex) {
                throw new RuntimeException("Failed to perform a query", ex);
            }
        }
    }
}
