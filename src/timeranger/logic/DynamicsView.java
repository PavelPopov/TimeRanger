package timeranger.logic;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import javafx.util.Callback;

/**
 * Enum containing visualization modes for dynamics analysis
 *
 * @author PavelPopov
 */
public enum DynamicsView {
    HOURS("Hours", ChronoUnit.HOURS, DateTimeFormatter.ofPattern("dd.MM.yy HH"), (v) -> {
        return LocalDateTime.of(v.getYear(), v.getMonth(), v.getDayOfMonth(), v.getHour(), 0);
    }),
    DAYS("Days", ChronoUnit.DAYS, DateTimeFormatter.ofPattern("dd.MM.yy cccc"), (v) -> {
        return LocalDateTime.of(v.getYear(), v.getMonth(), v.getDayOfMonth(), 0, 0);
    }),
    MONTHS("Months", ChronoUnit.MONTHS, DateTimeFormatter.ofPattern("MMMM yyyy"), (v) -> {
        return LocalDateTime.of(v.getYear(), v.getMonth(), 1, 0, 0);
    }),
    YEARS("Years", ChronoUnit.YEARS, DateTimeFormatter.ofPattern("yyyy"), (v) -> {
        return LocalDateTime.of(v.getYear(), 1, 1, 0, 0);
    });

    /**
     * Name of view
     */
    private final String name;
    /**
     * Unit that view represents
     */
    private final ChronoUnit unit;
    /**
     * Formatter for timestamp to label value conversion
     */
    private final DateTimeFormatter formatter;
    /**
     * Callback for truncation of timestamp to given unit
     */
    private final Callback<LocalDateTime, LocalDateTime> truncator;

    private DynamicsView(String name, ChronoUnit unit, DateTimeFormatter formatter,
            Callback<LocalDateTime, LocalDateTime> truncator) {
        this.name = name;
        this.unit = unit;
        this.formatter = formatter;
        this.truncator = truncator;
    }

    /**
     * @return the name
     */
    public final String getName() {
        return name;
    }

    /**
     * @return the unit
     */
    public final ChronoUnit getUnit() {
        return unit;
    }

    /**
     * Format timestamp using this view
     *
     * @param time timestamp
     * @return formatted timestamp
     */
    public final String format(LocalDateTime time) {
        return formatter.format(time);
    }

    /**
     * Truncate timestamp to unit of this view
     *
     * @param time timestamp
     * @return truncated timestamp
     */
    public final LocalDateTime truncate(LocalDateTime time) {
        return truncator.call(time);
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Get view from name
     *
     * @param name view name
     * @return view
     */
    public static final DynamicsView getByName(String name) {
        DynamicsView view = null;
        for (DynamicsView v : DynamicsView.values()) {
            if (v.getName().equals(name)) {
                view = v;
            }
        }
        return view;
    }

    /**
     * Get view from unit
     *
     * @param unit view unit
     * @return view
     */
    public static final DynamicsView getByUnit(ChronoUnit unit) {
        DynamicsView view = null;
        for (DynamicsView v : DynamicsView.values()) {
            if (v.getUnit().equals(unit)) {
                view = v;
            }
        }
        return view;
    }

    /**
     * Get list of views names
     *
     * @return views names list
     */
    public static final List<String> names() {
        ArrayList<String> names = new ArrayList();
        for (DynamicsView v : DynamicsView.values()) {
            names.add(v.getName());
        }
        return names;
    }
}
