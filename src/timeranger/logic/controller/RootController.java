package timeranger.logic.controller;

import com.jfoenix.controls.JFXSlider;
import com.opencsv.CSVParser;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import jfxtras.scene.control.LocalDateTimeTextField;
import jfxtras.scene.control.agenda.Agenda;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import timeranger.ColorUtil;
import timeranger.TimeRanger;
import timeranger.logic.AnalysisProperty;
import timeranger.logic.controller.tab.AnalyzeTabController;
import timeranger.logic.controller.tab.OverviewTabController;
import timeranger.logic.controller.tab.TODOTabController;
import timeranger.logic.controller.tab.TagsTabController;
import timeranger.logic.controller.tab.TasksTabController;
import timeranger.model.Activity;
import timeranger.model.TODO;
import timeranger.model.Tag;
import timeranger.model.Task;
import timeranger.view.factory.AgendaSkinSwitcher;
import timeranger.view.factory.TagChipView;

/**
 * Root stage controller
 *
 * @author PavelPopov
 */
public class RootController implements Initializable {

    /**
     * Instance of controller
     */
    private static RootController instance;
    /**
     * Flag indicating controller's state
     */
    private static boolean initialized = false;

    @FXML
    private MenuItem menuEditUndoItem;
    @FXML
    private MenuItem menuEditRedoItem;
    @FXML
    private MenuItem menuEditCopyItem;
    @FXML
    private MenuItem menuEditPasteItem;
    @FXML
    private MenuItem menuEditDeleteItem;
    @FXML
    private MenuItem menuEditSelectAllItem;
    @FXML
    private MenuItem menuEditInvertSelectionItem;

    @FXML
    private TextField activityTextField;
    @FXML
    private Button buttonStart;
    @FXML
    private Button buttonStartPlus;
    @FXML
    private Button buttonAdd;

    @FXML
    private TabPane tabPane;

    @FXML
    private Tab TODOTab;
    @FXML
    private ListView<TODO> TODOListView;

    @FXML
    private Tab overviewTab;
    @FXML
    private AnchorPane overviewAnchorPane;
    @FXML
    private DatePicker overviewStartDatePicker;
    @FXML
    private TextField overviewSearchTextField;
    @FXML
    private DatePicker overviewEndDatePicker;
    @FXML
    private Button overviewFilterButton;
    @FXML
    private AgendaSkinSwitcher overviewAgendaSkinSwitcher;
    @FXML
    private Agenda overviewAgenda;

    @FXML
    private Tab tasksTab;
    @FXML
    private TableView<Task> tasksTableView;
    @FXML
    private TableColumn<Task, String> tasksTableViewNameColumn;
    @FXML
    private TableColumn<Task, List<Tag>> tasksTableViewTagsColumn;
    @FXML
    private TableColumn<Task, String> tasksTableViewDescriptionColumn;
    @FXML
    private TableColumn<Task, LocalDateTime> tasksTableViewDeadlineColumn;
    @FXML
    private TableColumn<Task, Boolean> tasksTableViewDoneColumn;

    @FXML
    private Tab tagsTab;
    @FXML
    private TableView<Tag> tagsTableView;
    @FXML
    private TableColumn<Tag, String> tagsTableViewNameColumn;
    @FXML
    private TableColumn<Tag, Integer> tagsTableViewColorColumn;
    @FXML
    private TableColumn<Tag, String> tagsTableViewDescriptionColumn;

    @FXML
    private Tab analyzeTab;
    @FXML
    private TextField analyzeFilterFirstNameTextField;
    @FXML
    private LocalDateTimeTextField analyzeFilterFirstStartDateField;
    @FXML
    private LocalDateTimeTextField analyzeFilterFirstEndDateField;
    @FXML
    private TagChipView<Tag> analyzeFilterFirstTagsChipView;
    @FXML
    private JFXSlider analyzeFilterFirstUntaggedSlider;
    @FXML
    private TextField analyzeFilterSecondNameTextField;
    @FXML
    private LocalDateTimeTextField analyzeFilterSecondStartDateField;
    @FXML
    private LocalDateTimeTextField analyzeFilterSecondEndDateField;
    @FXML
    private TagChipView<Tag> analyzeFilterSecondTagsChipView;
    @FXML
    private JFXSlider analyzeFilterSecondUntaggedSlider;
    @FXML
    private TableView<AnalysisProperty> analyzePropertiesTableView;
    @FXML
    private TableColumn<AnalysisProperty, String> analyzePropertiesTableViewNameColumn;
    @FXML
    private TableColumn<AnalysisProperty, String> analyzePropertiesTableViewFirstValueColumn;
    @FXML
    private TableColumn<AnalysisProperty, String> analyzePropertiesTableViewSecondValueColumn;
    @FXML
    private TableColumn<AnalysisProperty, String> analyzePropertiesTableViewDifferenceColumn;
    @FXML
    private PieChart analyzeDistributionFirstPieChart;
    @FXML
    private PieChart analyzeDistributionSecondPieChart;
    @FXML
    private ChoiceBox<String> analyzeDynamicsViewChoiceBox;
    @FXML
    private LineChart<String, Number> analyzeDynamicsLineChart;

    /**
     * Controller of TODO tab
     */
    private TODOTabController TODOTabController;
    /**
     * Controller of overview tab
     */
    private OverviewTabController overviewTabController;
    /**
     * Controller of tasks tab
     */
    private TasksTabController tasksTabController;
    /**
     * Controller of tags tab
     */
    private TagsTabController tagsTabController;
    /**
     * Controller of analyze tab
     */
    private AnalyzeTabController analyzeTabController;

    /**
     * Currently selected tab
     */
    private Tab selectedTab;

    /**
     * Load controller's stage
     *
     * @param stage owner stage
     * @return controller's stage
     */
    public static Stage loadStage(Stage stage) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TimeRanger.class.getResource("view/fxml/Root.fxml"));
            VBox layout = (VBox) loader.load();
            Scene scene = new Scene(layout);
            scene.getStylesheets().add(TimeRanger.class.getResource("view/css/common.css").toExternalForm());
            stage.getIcons().add(new Image(TimeRanger.class.getResourceAsStream("view/resource/icon.png")));
            stage.setTitle("TimeRanger v" + TimeRanger.VERSION);
            stage.setOnCloseRequest((WindowEvent event) -> {
                TimeRanger.hideStage();
            });
            stage.setMinWidth(640);
            stage.setMinHeight(480);
            stage.setScene(scene);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load Root stage", ex);
        }
        return stage;
    }

    /**
     * Change focus when stage was shown
     */
    public void onStageShown() {
        activityTextField.clear();
        activityTextField.requestFocus();
    }

    /**
     * @return instance
     */
    public static final RootController getInstance() {
        return instance;
    }

    /**
     * @return is initialized
     */
    public static final boolean isInitialized() {
        return initialized;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instance = this;
        menuEditUndoItem.setOnAction((event) -> {
            TimeRanger.undo();
        });
        menuEditRedoItem.setOnAction((event) -> {
            TimeRanger.redo();
        });
        updateUndoRedoItems();
        menuEditCopyItem.setOnAction((event) -> {
            ArrayList<String> out = new ArrayList();
            CSVParser parser = new CSVParser();
            Node focusOwner = TimeRanger.getRootStage().getScene().getFocusOwner();
            switch (focusOwner.getId()) {
                case "TODOListView":
                    TODOListView.getSelectionModel().getSelectedItems().forEach((todo) -> {
                        out.add(parser.parseToLine(new String[]{
                            todo.getName()
                        }, true));
                    });
                    break;
                case "tasksTableView":
                    if (tasksTableView.getSelectionModel().getSelectedItems().size() > 0) {
                        out.add(parser.parseToLine(new String[]{"id", "name", "tags",
                            "deadline", "done", "description"}, true));
                    }
                    tasksTableView.getSelectionModel().getSelectedItems().forEach((task) -> {
                        ArrayList<String> tags = new ArrayList();
                        task.getTags().forEach((tag) -> {
                            tags.add(tag.getName());
                        });
                        out.add(parser.parseToLine(new String[]{
                            task.getId() + "",
                            task.getName(),
                            parser.parseToLine(tags.toArray(new String[tags.size()]), true),
                            task.getDeadline() + "",
                            task.getDone() + "",
                            task.getDescription()
                        }, true));
                    });
                    break;
                case "tagsTableView":
                    if (tagsTableView.getSelectionModel().getSelectedItems().size() > 0) {
                        out.add(parser.parseToLine(new String[]{"id", "name", "color", "description"}, true));
                    }
                    tagsTableView.getSelectionModel().getSelectedItems().forEach((tag) -> {
                        String colorStr = ColorUtil.getHex(ColorUtil.getColor(tag.getColor()));
                        out.add(parser.parseToLine(new String[]{
                            tag.getId() + "",
                            tag.getName(),
                            colorStr,
                            tag.getDescription()
                        }, true));
                    });
                    break;
                case "analyzePropertiesTableView":
                    if (analyzePropertiesTableView.getSelectionModel().getSelectedItems().size() > 0) {
                        out.add(parser.parseToLine(new String[]{"name", "firstValue",
                            "secondValue", "difference"}, true));
                    }
                    analyzePropertiesTableView.getSelectionModel().getSelectedItems().forEach((analysisProp) -> {
                        out.add(parser.parseToLine(new String[]{
                            analysisProp.getName(),
                            analysisProp.getFirstValue(),
                            analysisProp.getSecondValue(),
                            analysisProp.getDifference()
                        }, true));
                    });
                    break;
            }
            final Clipboard clipboard = Clipboard.getSystemClipboard();
            final ClipboardContent content = new ClipboardContent();
            content.putString(String.join("\n", out));
            clipboard.setContent(content);
        });
        menuEditPasteItem.setOnAction((event) -> {
            final Clipboard clipboard = Clipboard.getSystemClipboard();
            final String content = (String) clipboard.getContent(DataFormat.PLAIN_TEXT);
            CSVParser parser = new CSVParser();
            // Splitting and joining lines to be consistent if multiline data is present
            ArrayList<String> list = new ArrayList();
            String[] a = content.split("\n");
            for (int i = 0; i < a.length;) {
                String astr = "";
                int k = i;
                boolean shouldBeEven = true;
                while (k < a.length) {
                    boolean even = (StringUtils.countMatches(a[k], '“') + StringUtils.countMatches(a[k], '”')
                            + StringUtils.countMatches(a[k], '"')) % 2 == 0;
                    astr += a[k];
                    k++;
                    if (shouldBeEven ^ even) {
                        shouldBeEven = !shouldBeEven;
                        astr += "\n";
                    } else {
                        i = k;
                        break;
                    }
                }
                list.add(astr);
            }
            String[] arr = list.toArray(new String[list.size()]);
            Node focusOwner = TimeRanger.getRootStage().getScene().getFocusOwner();
            switch (focusOwner.getId()) {
                case "TODOListView":
                    for (int i = 0; i < arr.length; i++) {
                        try {
                            // Quick check if it's a CSV. Maybe check should be more extensive...
                            String name = arr[i].startsWith("\"") ? parser.parseLine(arr[i])[0] : arr[i];
                            TODO todo = new TODO(name);
                            TimeRanger.addTODO(todo);
                        } catch (IOException ex) {
                        }
                    }
                    break;
                case "tasksTableView":
                    if (arr.length < 2) {
                        break;
                    }
                    try {
                        String[] keys = parser.parseLineMulti(arr[0]);
                        if (keys.length == 1) {
                            keys = keys[0].split("	");
                        }
                        if (!ArrayUtils.contains(keys, "name")) {
                            break;
                        }
                        for (int i = 1; i < arr.length; i++) {
                            String[] vals = arr[i].startsWith("\"") ? parser.parseLineMulti(arr[i]) : arr[i].split("	");
                            if (vals.length != keys.length) {
                                continue;
                            }
                            Task task = new Task();
                            for (int k = 0; k < vals.length; k++) {
                                String key = keys[k];
                                String val = vals[k];
                                System.out.println(key + " = " + val);
                                switch (key) {
                                    case "id":
                                        task.setId(Integer.valueOf(val));
                                        break;
                                    case "name":
                                        task.setName(val);
                                        break;
                                    case "tags":
                                        ArrayList<Tag> tags = new ArrayList();
                                        String[] ts = parser.parseLineMulti(val);
                                        for (int j = 0; j < ts.length; j++) {
                                            Tag tag = null;
                                            for (Tag t : TimeRanger.getTags()) {
                                                if (t.getName().equals(ts[j])) {
                                                    tag = t;
                                                    break;
                                                }
                                            }
                                            if (tag != null) {
                                                tags.add(tag);
                                            }
                                        }
                                        task.setTags(tags);
                                        break;
                                    case "deadline":
                                        if (val.equals("null")) {
                                            continue;
                                        }
                                        task.setDeadline(LocalDateTime.parse(val, TimeRanger.DATETIME_FORMATTER));
                                        break;
                                    case "done":
                                        if (val.equals("null")) {
                                            continue;
                                        }
                                        task.setDone(LocalDateTime.parse(val, TimeRanger.DATETIME_FORMATTER));
                                        break;
                                    case "description":
                                        task.setDescription(val);
                                        break;
                                    default:
                                }
                            }
                            TimeRanger.addTask(task);
                        }
                    } catch (IOException ex) {
                    }
                    break;
                case "tagsTableView":
                    if (arr.length < 2) {
                        break;
                    }
                    try {
                        String[] keys = parser.parseLineMulti(arr[0]);
                        if (keys.length == 1) {
                            keys = keys[0].split("	");
                        }
                        if (!ArrayUtils.contains(keys, "name")) {
                            break;
                        }
                        for (int i = 1; i < arr.length; i++) {
                            String[] vals = parser.parseLineMulti(arr[i]);
                            if (vals.length == 1) {
                                vals = vals[0].split("	");
                            }
                            if (vals.length != keys.length) {
                                continue;
                            }
                            Tag tag = new Tag();
                            for (int k = 0; k < vals.length; k++) {
                                String key = keys[k];
                                String val = vals[k];
                                switch (key) {
                                    case "id":
                                        tag.setId(Integer.valueOf(val));
                                        break;
                                    case "name":
                                        tag.setName(val);
                                        break;
                                    case "color":
                                        tag.setColor(ColorUtil.getValue(Color.web(val)));
                                        break;
                                    case "description":
                                        tag.setDescription(val);
                                        break;
                                    default:
                                }
                            }
                            TimeRanger.addTag(tag);
                        }
                    } catch (IOException ex) {
                    }
                    break;
            }
        });
        menuEditDeleteItem.setOnAction((event) -> {
            Node focusOwner = TimeRanger.getRootStage().getScene().getFocusOwner();
            switch (focusOwner.getId()) {
                case "overviewAgenda":
                    getOverviewTabController().deleteSelected();
                    break;
                case "TODOListView":
                    getTODOTabController().deleteSelected();
                    break;
                case "tasksTableView":
                    getTasksTabController().deleteSelected();
                    break;
                case "tagsTableView":
                    getTagsTabController().deleteSelected();
                    break;
            }
        });
        menuEditSelectAllItem.setOnAction((event) -> {
            Node focusOwner = TimeRanger.getRootStage().getScene().getFocusOwner();
            switch (focusOwner.getId()) {
                case "overviewAgenda":
                    getOverviewTabController().selectAll();
                    break;
                case "TODOListView":
                    getTODOTabController().selectAll();
                    break;
                case "tasksTableView":
                    getTasksTabController().selectAll();
                    break;
                case "tagsTableView":
                    getTagsTabController().selectAll();
                    break;
            }
        });
        menuEditInvertSelectionItem.setOnAction((event) -> {
            Node focusOwner = TimeRanger.getRootStage().getScene().getFocusOwner();
            switch (focusOwner.getId()) {
                case "overviewAgenda":
                    getOverviewTabController().invertSelection();
                    break;
                case "TODOListView":
                    getTODOTabController().invertSelection();
                    break;
                case "tasksTableView":
                    getTasksTabController().invertSelection();
                    break;
                case "tagsTableView":
                    getTagsTabController().invertSelection();
                    break;
            }
        });
        TODOTabController = new TODOTabController(TODOTab, activityTextField, TODOListView);
        TODOTabController.init();
        overviewTabController = new OverviewTabController(overviewTab,
                overviewFilterButton, overviewSearchTextField, overviewStartDatePicker,
                overviewEndDatePicker, overviewAgenda, overviewAgendaSkinSwitcher);
        overviewTabController.init();
        tasksTabController = new TasksTabController(tasksTab, activityTextField, tasksTableView,
                tasksTableViewNameColumn, tasksTableViewTagsColumn, tasksTableViewDescriptionColumn,
                tasksTableViewDeadlineColumn, tasksTableViewDoneColumn);
        tasksTabController.init();
        tagsTabController = new TagsTabController(tagsTab, activityTextField, tagsTableView,
                tagsTableViewNameColumn, tagsTableViewColorColumn, tagsTableViewDescriptionColumn);
        tagsTabController.init();
        analyzeTabController = new AnalyzeTabController(analyzeTab,
                analyzeFilterFirstNameTextField, analyzeFilterFirstStartDateField,
                analyzeFilterFirstEndDateField, analyzeFilterFirstTagsChipView,
                analyzeFilterFirstUntaggedSlider,
                analyzeFilterSecondNameTextField, analyzeFilterSecondStartDateField,
                analyzeFilterSecondEndDateField, analyzeFilterSecondTagsChipView,
                analyzeFilterSecondUntaggedSlider,
                analyzePropertiesTableView, analyzePropertiesTableViewNameColumn,
                analyzePropertiesTableViewFirstValueColumn,
                analyzePropertiesTableViewSecondValueColumn,
                analyzePropertiesTableViewDifferenceColumn,
                analyzeDistributionFirstPieChart, analyzeDistributionSecondPieChart,
                analyzeDynamicsViewChoiceBox, analyzeDynamicsLineChart
        );
        analyzeTabController.init();
        activityTextField.setOnAction((event) -> {
            if (selectedTab == TODOTab) {
                getTODOTabController().onAdd();
            } else if (selectedTab == overviewTab || selectedTab == analyzeTab) {
                changeActivity(true);
            } else if (selectedTab == tasksTab) {
                getTasksTabController().onAdd();
            } else if (selectedTab == tagsTab) {
                getTagsTabController().onAdd();
            } else {
                throw new IllegalStateException("No action for selected tab");
            }
        });
        buttonAdd.setOnAction((event) -> {
            if (selectedTab == TODOTab) {
                getTODOTabController().onAdd();
            } else if (selectedTab == tasksTab) {
                getTasksTabController().onAdd();
            } else if (selectedTab == tagsTab) {
                getTagsTabController().onAdd();
            } else {
                throw new IllegalStateException("No add action for selected tab");
            }
        });
        buttonStart.setOnAction((event) -> {
            changeActivity(true);
        });
        buttonStartPlus.setOnAction((event) -> {
            changeActivity(false);
        });
        initialized = true;
    }

    /**
     * @return TODO tab controller
     */
    public final TODOTabController getTODOTabController() {
        return TODOTabController;
    }

    /**
     * @return overview tab controller
     */
    public final OverviewTabController getOverviewTabController() {
        return overviewTabController;
    }

    /**
     * @return tasks tab controller
     */
    public final TasksTabController getTasksTabController() {
        return tasksTabController;
    }

    /**
     * @return tags tab controller
     */
    public final TagsTabController getTagsTabController() {
        return tagsTabController;
    }

    /**
     * @return analyze tab controller
     */
    public final AnalyzeTabController getAnalyzeTabController() {
        return analyzeTabController;
    }

    /**
     * Quit on event
     *
     * @param event event
     */
    @FXML
    private void onQuit(ActionEvent event) {
        TimeRanger.quit();
    }

    /**
     * Show about stage on event
     *
     * @param event event
     */
    @FXML
    private void onAbout(ActionEvent event) {
        TimeRanger.getAboutStage().show();
    }

    /**
     * Change current activity
     *
     * @param endCurrentActivities flag determining inclusion of previous current activities
     */
    // TODO: Rework for OverviewTabController
    private void changeActivity(boolean endCurrentActivities) {
        // TODO: bind text field text change to selection events
        String str = null;
        final List<Tag> tags = new ArrayList();
        if (selectedTab == TODOTab) {
            if (TODOListView.getSelectionModel().getSelectedItem() != null) {
                str = TODOListView.getSelectionModel().getSelectedItem().getName();
            }
        } else if (selectedTab == overviewTab) {
            if (overviewAgenda.selectedAppointments().size() > 0) {
                Activity activity = getOverviewTabController().getActivityFromAppointment(
                        overviewAgenda.selectedAppointments().get(0));
                if (activity != null) {
                    str = activity.getName();
                    tags.addAll(activity.getTags());
                }
            }
        } else if (selectedTab == tasksTab) {
            if (tasksTableView.getSelectionModel().getSelectedItem() != null) {
                str = tasksTableView.getSelectionModel().getSelectedItem().getName();
                tags.addAll(tasksTableView.getSelectionModel().getSelectedItem().getTags());
            }
        }
        String fieldStr = activityTextField.getText().trim();
        activityTextField.clear();
        if (!fieldStr.isEmpty()) {
            str = fieldStr;
        }
        if (str == null) {
            return;
        }
        final String name = str;
        TimeRanger.runLater(() -> {
            Activity activity = new Activity();
            activity.setName(name);
            activity.setTags(tags);
            activity.setStart(LocalDateTime.now());
            TimeRanger.changeActivity(activity, endCurrentActivities);
        });
        tabPane.getSelectionModel().select(overviewTab);
    }

    /**
     * Update when tags were changed
     *
     * @param row table row
     * @param tags tags list
     */
    // TODO: rework
    public void onTagsChanged(TableRow row, List<Tag> tags) {
        if (row.getItem() == null) {
            return;
        }
        if (row.getTableView().equals(tasksTableView)) {
            TimeRanger.updateTaskTags((Task) row.getItem(), tags);
        }
    }

    /**
     * Change current tab
     *
     * @param tab tab to change to
     * @param disableAdd flag determining disable state of add button
     */
    private void changeTab(Tab tab, boolean disableAdd) {
        if (selectedTab == tab) {
            return;
        }
        selectedTab = tab;
        buttonAdd.setDisable(disableAdd);
    }

    /**
     * Change tab to TODO
     */
    @FXML
    private void onTODOTabSelected() {
        changeTab(TODOTab, false);
    }

    /**
     * Change tab to overview
     */
    @FXML
    private void onOverviewTabSelected() {
        changeTab(overviewTab, true);
    }

    /**
     * Change tab to tasks
     */
    @FXML
    private void onTasksTabSelected() {
        changeTab(tasksTab, false);
    }

    /**
     * Change tab to tags
     */
    @FXML
    private void onTagsTabSelected() {
        changeTab(tagsTab, false);
    }

    /**
     * Change tab to analysis
     */
    @FXML
    private void onAnalyzeTabSelected() {
        changeTab(analyzeTab, true);
    }

    public void updateUndoRedoItems() {
        menuEditUndoItem.setDisable(!TimeRanger.hasUndo());
        menuEditRedoItem.setDisable(!TimeRanger.hasRedo());
    }
}
