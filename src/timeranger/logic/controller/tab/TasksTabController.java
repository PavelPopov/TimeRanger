package timeranger.logic.controller.tab;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.beans.Observable;
import javafx.beans.value.ObservableValueBase;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.logic.operation.TableSelectionOperation;
import timeranger.logic.operation.TaskListModelOperation;
import timeranger.logic.operation.TaskModelOperation;
import timeranger.model.Tag;
import timeranger.model.Task;
import timeranger.view.factory.EditableTableCheckBoxCell;
import timeranger.view.factory.EditableTableChipViewCell;
import timeranger.view.factory.EditableTableDateTimeCell;
import timeranger.view.factory.EditableTableNameCell;
import timeranger.view.factory.EditableTableTextCell;

/**
 * Controller of tasks tab
 *
 * @author PavelPopov
 */
public final class TasksTabController {

    private final Tab tab;
    private final TextField textField;
    private final TableView<Task> tableView;
    private final TableColumn<Task, String> tableViewNameColumn;
    private final TableColumn<Task, List<Tag>> tableViewTagsColumn;
    private final TableColumn<Task, String> tableViewDescriptionColumn;
    private final TableColumn<Task, LocalDateTime> tableViewDeadlineColumn;
    private final TableColumn<Task, Boolean> tableViewDoneColumn;
    private final ArrayList<Integer> oldSelection = new ArrayList();

    public TasksTabController(Tab tab, TextField textField, TableView<Task> tableView,
            TableColumn<Task, String> tableViewNameColumn,
            TableColumn<Task, List<Tag>> tableViewTagsColumn,
            TableColumn<Task, String> tableViewDescriptionColumn,
            TableColumn<Task, LocalDateTime> tableViewDeadlineColumn,
            TableColumn<Task, Boolean> tableViewDoneColumn) {
        this.tab = tab;
        this.textField = textField;
        this.tableView = tableView;
        this.tableViewNameColumn = tableViewNameColumn;
        this.tableViewTagsColumn = tableViewTagsColumn;
        this.tableViewDescriptionColumn = tableViewDescriptionColumn;
        this.tableViewDeadlineColumn = tableViewDeadlineColumn;
        this.tableViewDoneColumn = tableViewDoneColumn;
    }

    /**
     * Initialize tab's components
     */
    public final void init() {
        tableView.setEditable(true);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tableView.getSelectionModel().getSelectedIndices().addListener((Observable observable) -> {
            ArrayList<Integer> oldValue = new ArrayList(oldSelection);
            ArrayList<Integer> newValue = new ArrayList(tableView.getSelectionModel().getSelectedIndices());
            // TODO: WOW! What a dirty hack!
            if (tableView.getUserData() == null || !((Boolean) tableView.getUserData())) {
                TimeRanger.addOperation(new TableSelectionOperation(tableView, oldValue, newValue, tab));
            }
            oldSelection.clear();
            oldSelection.addAll(tableView.getSelectionModel().getSelectedIndices());
        });
        tableViewNameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        tableViewNameColumn.setCellFactory((col) -> {
            return new EditableTableNameCell(col);
        });
        tableViewNameColumn.setOnEditCommit((event) -> {
            Task oldTask = new Task(event.getRowValue());
            oldTask.setName(event.getOldValue());
            Task newTask = new Task(oldTask);
            newTask.setName(event.getNewValue());
            onTableEditCommit(oldTask, newTask);
        });
        tableViewTagsColumn.setCellValueFactory(new PropertyValueFactory("tags"));
        tableViewTagsColumn.setCellFactory((col) -> {
            return new EditableTableChipViewCell(col);
        });
        tableViewTagsColumn.setOnEditCommit((event) -> {
            Task oldTask = new Task(event.getRowValue());
            oldTask.setTags(new ArrayList(event.getOldValue()));
            Task newTask = new Task(oldTask);
            newTask.setTags(new ArrayList(event.getNewValue()));
            onTableEditCommit(oldTask, newTask);
        });
        tableViewDescriptionColumn.setCellValueFactory(new PropertyValueFactory("description"));
        tableViewDescriptionColumn.setCellFactory((col) -> {
            return new EditableTableTextCell(col);
        });
        tableViewDescriptionColumn.setOnEditCommit((event) -> {
            Task oldTask = new Task(event.getRowValue());
            oldTask.setDescription(event.getOldValue());
            Task newTask = new Task(oldTask);
            newTask.setDescription(event.getNewValue());
            onTableEditCommit(oldTask, newTask);
        });
        tableViewDeadlineColumn.setCellValueFactory(new PropertyValueFactory("deadline"));
        tableViewDeadlineColumn.setCellFactory((col) -> {
            return new EditableTableDateTimeCell(col);
        });
        tableViewDeadlineColumn.setOnEditCommit((event) -> {
            Task oldTask = new Task(event.getRowValue());
            oldTask.setDeadline(event.getOldValue());
            Task newTask = new Task(oldTask);
            newTask.setDeadline(event.getNewValue());
            onTableEditCommit(oldTask, newTask);
        });
        tableViewDoneColumn.setCellValueFactory((TableColumn.CellDataFeatures<Task, Boolean> param) -> {
            return new ObservableValueBase<Boolean>() {
                @Override
                public Boolean getValue() {
                    return param.getValue().getDone() != null;
                }
            };
        });
        tableViewDoneColumn.setCellFactory((col) -> {
            return new EditableTableCheckBoxCell(col);
        });
        tableViewDoneColumn.setOnEditCommit((event) -> {
            Task oldTask = new Task(event.getRowValue());
            oldTask.setDone(event.getOldValue() ? oldTask.getDone() : null);
            Task newTask = new Task(oldTask);
            newTask.setDone(event.getNewValue() ? LocalDateTime.now() : null);
            onTableEditCommit(oldTask, newTask);
        });
    }

    /**
     * Update tab's contents
     */
    public final void update() {
        tableView.setUserData(true);
        tableView.getItems().setAll(TimeRanger.getTasks());
        tableView.getItems().sort((o1, o2) -> {
            return o1.getName().compareTo(o2.getName());
        });
        tableView.refresh();
        tableView.setUserData(false);
    }

    /**
     * Add task
     */
    public final void onAdd() {
        String name = textField.getText().trim();
        if (name.isEmpty()) {
            return;
        }
        Task task = new Task();
        task.setName(name);
        task.setDescription("");
        addTask(task);
        textField.clear();
    }

    /**
     * Delete tasks which are currently selected in table view
     */
    public final void deleteSelected() {
        ArrayList<Task> selected = new ArrayList(tableView
                .getSelectionModel().getSelectedItems());
        if (selected.size() > 0) {
            deleteTasks(selected);
        }
    }

    /**
     * Select all loaded tasks
     */
    public final void selectAll() {
        tableView.getSelectionModel().selectAll();
    }

    /**
     * Invert selection of tasks
     */
    public final void invertSelection() {
        int count = tableView.getItems().size() - tableView.getSelectionModel().getSelectedIndices().size();
        int[] indices = new int[count];
        for (int i = 0, k = 0; i < tableView.getItems().size() && k < count; i++) {
            if (tableView.getSelectionModel().getSelectedIndices().contains(i)) {
                continue;
            }
            indices[k++] = i;
        }
        tableView.getSelectionModel().clearSelection();
        tableView.getSelectionModel().selectIndices(-1, indices);
    }

    /**
     * Handle table edit commit event
     *
     * @param task task model
     */
    private void onTableEditCommit(Task oldTask, Task newTask) {
        if (newTask.getName().trim().isEmpty()) {
            deleteTasks(Arrays.asList(oldTask));
        } else {
            updateTask(oldTask, newTask);
        }
    }

    /**
     * Add task
     *
     * @param task task model
     */
    private void addTask(Task task) {
        TimeRanger.addOperation(new TaskModelOperation(null, task, CRUDMethod.CREATE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.addTask(new Task(task));
        });
    }

    /**
     * Update task
     *
     * @param task task model
     */
    private void updateTask(Task oldTask, Task newTask) {
        TimeRanger.addOperation(new TaskModelOperation(oldTask, newTask, CRUDMethod.UPDATE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.updateTask(new Task(newTask));
        });
    }

    /**
     * Delete tasks
     *
     * @param tasks list of tasks models
     */
    private void deleteTasks(List<Task> tasks) {
        TimeRanger.addOperation(new TaskListModelOperation(tasks, null, CRUDMethod.DELETE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.deleteTasks(new ArrayList(tasks));
        });
    }

}
