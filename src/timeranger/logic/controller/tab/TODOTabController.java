package timeranger.logic.controller.tab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.beans.Observable;
import javafx.scene.control.ListView;
import javafx.scene.control.ListView.EditEvent;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.util.StringConverter;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.logic.operation.ListSelectionOperation;
import timeranger.logic.operation.TODOListModelOperation;
import timeranger.logic.operation.TODOModelOperation;
import timeranger.model.TODO;

/**
 * Controller of TODO tab
 *
 * @author PavelPopov
 */
public final class TODOTabController {

    private final Tab tab;
    private final TextField textField;
    private final ListView<TODO> listView;
    private final ArrayList<Integer> oldSelection = new ArrayList();
    /**
     * Name of TODO which is currently under edit or null
     */
    private String todoUnderEditName;

    public TODOTabController(Tab tab, TextField textField, ListView<TODO> listView) {
        this.tab = tab;
        this.textField = textField;
        this.listView = listView;
    }

    /**
     * Initialize tab's components
     */
    public final void init() {
        listView.setCellFactory((p) -> {
            TextFieldListCell<TODO> cell = new TextFieldListCell<TODO>(new StringConverter<TODO>() {
                @Override
                public String toString(TODO todo) {
                    return todo.getName();
                }

                @Override
                public TODO fromString(String name) {
                    return new TODO(name);
                }
            });
            cell.setWrapText(true);
            cell.setMinWidth(0);
            cell.setPrefWidth(1);
            return cell;
        });
        listView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listView.setOnEditStart((event) -> {
            onListEditStart(event);
        });
        listView.setOnEditCommit((event) -> {
            onListEditCommit(event);
        });
        listView.getSelectionModel().getSelectedIndices().addListener((Observable observable) -> {
            ArrayList<Integer> oldValue = new ArrayList(oldSelection);
            ArrayList<Integer> newValue = new ArrayList(listView.getSelectionModel().getSelectedIndices());
            // TODO: WOW! What a dirty hack!
            if (listView.getUserData() == null || !((Boolean) listView.getUserData())) {
                TimeRanger.addOperation(new ListSelectionOperation(listView, oldValue, newValue, tab));
            }
            oldSelection.clear();
            oldSelection.addAll(listView.getSelectionModel().getSelectedIndices());
        });
    }

    /**
     * Update tab components' content
     */
    public final void update() {
        listView.setUserData(true);
        listView.getItems().setAll(TimeRanger.getTODOs());
        listView.getItems().sort((o1, o2) -> {
            return o1.getName().compareTo(o2.getName());
        });
        listView.refresh();
        listView.setUserData(false);
    }

    /**
     * Handle list edit start event
     *
     * @param event event
     */
    public final void onListEditStart(EditEvent<TODO> event) {
        todoUnderEditName = listView.getItems().get(event.getIndex()).getName();
    }

    /**
     * Handle edit commit event of list
     *
     * @param event event
     */
    public final void onListEditCommit(EditEvent<TODO> event) {
        if (!todoUnderEditName.equals(event.getNewValue().getName())) {
            if (event.getNewValue().getName().trim().isEmpty()) {
                deleteTODOs(Arrays.asList(new TODO(todoUnderEditName)));
            } else {
                updateTODO(new TODO(todoUnderEditName), event.getNewValue());
            }
        }
    }

    /**
     * Add TODO
     */
    public final void onAdd() {
        String name = textField.getText().trim();
        if (name.isEmpty()) {
            return;
        }
        TODO todo = new TODO();
        todo.setName(name);
        addTODO(todo);
        textField.clear();
    }

    /**
     * Delete TODOs which are currently selected in list view
     */
    public final void deleteSelected() {
        ArrayList<TODO> selected = new ArrayList(listView
                .getSelectionModel().getSelectedItems());
        if (selected.size() > 0) {
            deleteTODOs(selected);
        }
    }

    /**
     * Select all loaded TODOs
     */
    public final void selectAll() {
        listView.getSelectionModel().selectAll();
    }

    /**
     * Invert selection of TODOs
     */
    public final void invertSelection() {
        int count = listView.getItems().size() - listView.getSelectionModel().getSelectedIndices().size();
        int[] indices = new int[count];
        for (int i = 0, k = 0; i < listView.getItems().size() && k < count; i++) {
            if (listView.getSelectionModel().getSelectedIndices().contains(i)) {
                continue;
            }
            indices[k++] = i;
        }
        listView.getSelectionModel().clearSelection();
        listView.getSelectionModel().selectIndices(-1, indices);
    }

    /**
     * Add TODO
     *
     * @param todo TODO
     */
    private void addTODO(TODO todo) {
        TimeRanger.addOperation(new TODOModelOperation(null, todo, CRUDMethod.CREATE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.addTODO(new TODO(todo));
        });
    }

    /**
     * Update TODO
     *
     * @param key primary key
     * @param todo TODO model
     */
    private void updateTODO(TODO oldTodo, TODO newTodo) {
        TimeRanger.addOperation(new TODOModelOperation(oldTodo, newTodo, CRUDMethod.UPDATE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.updateTODO(oldTodo.getName(), new TODO(newTodo));
        });
    }

    /**
     * Delete TODOs
     *
     * @param todos list of TODOs models
     */
    private void deleteTODOs(List<TODO> todos) {
        TimeRanger.addOperation(new TODOListModelOperation(todos, null, CRUDMethod.DELETE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.deleteTODOs(new ArrayList(todos));
        });
    }

}
