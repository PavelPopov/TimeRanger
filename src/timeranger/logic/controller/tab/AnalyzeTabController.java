package timeranger.logic.controller.tab;

import com.jfoenix.controls.JFXSlider;
import com.jfoenix.skins.JFXSliderSkin;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.Bindings;
import javafx.scene.Node;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.StringConverter;
import jfxtras.scene.control.LocalDateTimeTextField;
import timeranger.Analysis;
import timeranger.ColorUtil;
import timeranger.TimeRanger;
import timeranger.logic.AnalysisProperty;
import timeranger.logic.DynamicsView;
import timeranger.logic.UntaggedSliderValue;
import timeranger.model.Activity;
import timeranger.model.Tag;
import timeranger.view.factory.TagChip;
import timeranger.view.factory.TagChipView;

/**
 * Controller for analyze tab of root stage
 *
 * @author PavelPopov
 */
public class AnalyzeTabController {

    /**
     * Default interval for dynamics analysis visualization
     */
    private final static DynamicsView DEFAULT_VIEW = DynamicsView.YEARS;
    private final Tab tab;
    private final TextField filterFirstNameTextField;
    private final LocalDateTimeTextField filterFirstStartDateField;
    private final LocalDateTimeTextField filterFirstEndDateField;
    private final TagChipView<Tag> filterFirstTagsChipView;
    private final JFXSlider filterFirstUntaggedSlider;
    private final TextField filterSecondNameTextField;
    private final LocalDateTimeTextField filterSecondStartDateField;
    private final LocalDateTimeTextField filterSecondEndDateField;
    private final TagChipView<Tag> filterSecondTagsChipView;
    private final JFXSlider filterSecondUntaggedSlider;
    private final TableView<AnalysisProperty> propertiesTableView;
    private final TableColumn<AnalysisProperty, String> propertiesTableViewNameColumn;
    private final TableColumn<AnalysisProperty, String> propertiesTableViewFirstValueColumn;
    private final TableColumn<AnalysisProperty, String> propertiesTableViewSecondValueColumn;
    private final TableColumn<AnalysisProperty, String> propertiesTableViewDifferenceColumn;
    private final PieChart distributionFirstPieChart;
    private final PieChart distributionSecondPieChart;
    private final ChoiceBox<String> dynamicsViewChoiceBox;
    private final LineChart<String, Number> dynamicsLineChart;
    /**
     * Filtered activities of first set
     */
    private ArrayList<Activity> firstActivitiesList;
    /**
     * Filtered activities of second set
     */
    private ArrayList<Activity> secondActivitiesList;

    public AnalyzeTabController(Tab tab,
            TextField filterFirstNameTextField, LocalDateTimeTextField filterFirstStartDateField,
            LocalDateTimeTextField filterFirstEndDateField, TagChipView<Tag> filterFirstTagsChipView,
            JFXSlider filterFirstUntaggedSlider,
            TextField filterSecondNameTextField, LocalDateTimeTextField filterSecondStartDateField,
            LocalDateTimeTextField filterSecondEndDateField, TagChipView<Tag> filterSecondTagsChipView,
            JFXSlider filterSecondUntaggedSlider,
            TableView<AnalysisProperty> propertiesTableView,
            TableColumn<AnalysisProperty, String> propertiesTableViewNameColumn,
            TableColumn<AnalysisProperty, String> propertiesTableViewFirstValueColumn,
            TableColumn<AnalysisProperty, String> propertiesTableViewSecondValueColumn,
            TableColumn<AnalysisProperty, String> propertiesTableViewDifferenceColumn,
            PieChart distributionFirstPieChart, PieChart distributionSecondPieChart,
            ChoiceBox<String> dynamicsViewChoiceBox, LineChart<String, Number> dynamicsLineChart) {
        this.tab = tab;
        this.filterFirstNameTextField = filterFirstNameTextField;
        this.filterFirstStartDateField = filterFirstStartDateField;
        this.filterFirstEndDateField = filterFirstEndDateField;
        this.filterFirstTagsChipView = filterFirstTagsChipView;
        this.filterFirstUntaggedSlider = filterFirstUntaggedSlider;
        this.filterSecondNameTextField = filterSecondNameTextField;
        this.filterSecondStartDateField = filterSecondStartDateField;
        this.filterSecondEndDateField = filterSecondEndDateField;
        this.filterSecondTagsChipView = filterSecondTagsChipView;
        this.filterSecondUntaggedSlider = filterSecondUntaggedSlider;
        this.propertiesTableView = propertiesTableView;
        this.propertiesTableViewNameColumn = propertiesTableViewNameColumn;
        this.propertiesTableViewFirstValueColumn = propertiesTableViewFirstValueColumn;
        this.propertiesTableViewSecondValueColumn = propertiesTableViewSecondValueColumn;
        this.propertiesTableViewDifferenceColumn = propertiesTableViewDifferenceColumn;
        this.distributionFirstPieChart = distributionFirstPieChart;
        this.distributionSecondPieChart = distributionSecondPieChart;
        this.dynamicsViewChoiceBox = dynamicsViewChoiceBox;
        this.dynamicsLineChart = dynamicsLineChart;
    }

    /**
     * Initialize tab components
     */
    public final void init() {
        initFilter();
        initProperties();
        initDistribution();
        initDynamics();
    }

    /**
     * Initialize filter pane
     */
    private void initFilter() {
        InvalidationListener listener = (observable) -> {
            update();
        };
        initFilterFields(listener, filterFirstNameTextField, filterFirstStartDateField,
                filterFirstEndDateField, filterFirstTagsChipView, filterFirstUntaggedSlider,
                "#d00", "#fdd", "#f00");
        initFilterFields(listener, filterSecondNameTextField, filterSecondStartDateField,
                filterSecondEndDateField, filterSecondTagsChipView, filterSecondUntaggedSlider,
                "#d0d", "#fdf", "#f0f");
    }

    /**
     * Initialize filter fields
     *
     * @param listener listener to call when values are changing
     * @param nameTextField name text field
     * @param startDateField start timestamp field
     * @param endDateField end timestamp field
     * @param tagsChipView tags chip view
     * @param untaggedSlider slider determining untagged handling mode
     * @param thumbColor hex color of slider's thumb
     * @param trackColor hex color of slider's track
     * @param fillColor hex color of slider's filling
     */
    private void initFilterFields(InvalidationListener listener,
            TextField nameTextField, LocalDateTimeTextField startDateField,
            LocalDateTimeTextField endDateField, TagChipView<Tag> tagsChipView,
            JFXSlider untaggedSlider, String thumbColor, String trackColor, String fillColor) {
        // Configuring filter
        nameTextField.textProperty().addListener(listener);
        startDateField.localDateTimeProperty().addListener(listener);
        endDateField.localDateTimeProperty().addListener(listener);
        tagsChipView.getChips().addListener(listener);
        tagsChipView.setConverter(new StringConverter<Tag>() {
            @Override
            public String toString(Tag t) {
                return t == null ? null : t.getName();
            }

            @Override
            public Tag fromString(String name) {
                for (Tag tag : TimeRanger.getTags()) {
                    if (tag.getName().equals(name)) {
                        return tag;
                    }
                }
                return new Tag(0, name, 0, "");
            }
        });
        // TODO: change location of tag creation method or make something
        // to avoid association with root frame
        tagsChipView.setChipFactory((chipView, tag) -> {
            return new TagChip<Tag>(chipView, tag);
        });
        // Configuring untagged slider
        untaggedSlider.setMin(0);
        untaggedSlider.setMax(UntaggedSliderValue.values().length - 1);
        untaggedSlider.setValue(0);
        untaggedSlider.setShowTickLabels(false);
        untaggedSlider.setShowTickMarks(true);
        untaggedSlider.setMajorTickUnit(1);
        untaggedSlider.setMinorTickCount(0);
        untaggedSlider.setSnapToTicks(true);
        untaggedSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            boolean changed = untaggedSlider.getUserData() == null
                    || Double.compare((double) untaggedSlider.getUserData(), newValue.doubleValue()) != 0;
            boolean discrete = Double.compare(newValue.doubleValue(), 0.0) == 0
                    || Double.compare(newValue.doubleValue(), 1.0) == 0
                    || Double.compare(newValue.doubleValue(), 2.0) == 0;
            if (changed && discrete) {
                untaggedSlider.setUserData(newValue.doubleValue());
                listener.invalidated(observable);
            }
        });
        untaggedSlider.setValueFactory((slider) -> {
            return Bindings.createStringBinding(() -> {
                return UntaggedSliderValue.getByIndex((int) slider.getValue()).getName();
            }, slider.valueProperty());
        });
        // Styling slider
        untaggedSlider.skinProperty().addListener((observable) -> {
            JFXSliderSkin skin = (JFXSliderSkin) untaggedSlider.getSkin();
            Set<Node> tickMarks = skin.getSkinnable().lookupAll(".axis-tick-mark");
            StackPane thumb = (StackPane) skin.getSkinnable().lookup(".thumb");
            StackPane animatedThumb = (StackPane) skin.getSkinnable().lookup(".animated-thumb");
            StackPane track = (StackPane) skin.getSkinnable().lookup(".track");
            StackPane coloredTrack = (StackPane) skin.getSkinnable().lookup(".colored-track");
            Text sliderValue = (Text) skin.getSkinnable().lookup(".slider-value");
            thumb.setStyle("-fx-background-color: " + thumbColor + ";");
            animatedThumb.setStyle("-fx-background-color: none;");
            track.setStyle("-fx-background-color: " + trackColor + ";");
            coloredTrack.setStyle("-fx-background-color: " + fillColor + ";");
            sliderValue.setStyle("-fx-fill: null; -fx-stroke: " + thumbColor + ";");
            for (Node tickMark : tickMarks) {
                tickMark.setStyle("-fx-fill: null; -fx-stroke: " + thumbColor + ";");
            }
        });
    }

    /**
     * Initialize properties pane
     */
    private void initProperties() {
        propertiesTableView.setEditable(false);
        propertiesTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        propertiesTableViewNameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        propertiesTableViewFirstValueColumn.setCellValueFactory(new PropertyValueFactory("firstValue"));
        propertiesTableViewSecondValueColumn.setCellValueFactory(new PropertyValueFactory("secondValue"));
        propertiesTableViewDifferenceColumn.setCellValueFactory(new PropertyValueFactory("difference"));
    }

    /**
     * Initialize distribution pane
     */
    private void initDistribution() {
        distributionFirstPieChart.setLegendVisible(false);
        distributionSecondPieChart.setLegendVisible(false);
    }

    /**
     * Initialize dynamics pane
     */
    private void initDynamics() {
        dynamicsViewChoiceBox.getItems().setAll(DynamicsView.names());
        dynamicsViewChoiceBox.setValue(DEFAULT_VIEW.getName());
        dynamicsViewChoiceBox.valueProperty().addListener((observable) -> {
            // TODO: optimize to update only dynamics view
            update();
        });
        dynamicsLineChart.setCreateSymbols(false);
        dynamicsLineChart.setLegendVisible(false);
        CategoryAxis xAxis = (CategoryAxis) dynamicsLineChart.getXAxis();
        NumberAxis yAxis = (NumberAxis) dynamicsLineChart.getYAxis();
        xAxis.setAnimated(false);
        xAxis.setTickLength(3);
        xAxis.getCategories().setAll("First value", "Second value");
        yAxis.setAutoRanging(false);
    }

    /**
     * Update tab contents
     */
    public final void update() {
        // Getting filter data
        String name1 = filterFirstNameTextField.getText(),
                name2 = filterSecondNameTextField.getText();
        LocalDateTime start1 = filterFirstStartDateField.getLocalDateTime(),
                start2 = filterSecondStartDateField.getLocalDateTime();
        LocalDateTime end1 = filterFirstEndDateField.getLocalDateTime(),
                end2 = filterSecondEndDateField.getLocalDateTime();
        List<Tag> tags1 = filterFirstTagsChipView.getChips(),
                tags2 = filterSecondTagsChipView.getChips();
        UntaggedSliderValue untagged1 = UntaggedSliderValue.getByIndex((int) filterFirstUntaggedSlider.getValue()),
                untagged2 = UntaggedSliderValue.getByIndex((int) filterSecondUntaggedSlider.getValue());
        // If something is not specified - we'll include everything
        LocalDateTime first = TimeRanger.getFirstActivityStartTime(),
                last = TimeRanger.getLastActivityEndTime();
        if (first == null) {
            first = LocalDateTime.now();
            last = first;
        }
        start1 = start1 == null ? first : start1;
        start2 = start2 == null ? first : start2;
        end1 = end1 == null ? last : end1;
        end2 = end2 == null ? last : end2;
        tags1 = tags1.isEmpty() ? TimeRanger.getTags() : tags1;
        tags2 = tags2.isEmpty() ? TimeRanger.getTags() : tags2;
        LocalDateTime firstStart = ChronoUnit.SECONDS.between(start1, start2) > 0
                ? start1 : start2;
        LocalDateTime lastEnd = ChronoUnit.SECONDS.between(end1, end2) > 0
                ? end2 : end1;
        // Obtaining filtered activities only if there are any activities
        firstActivitiesList = new ArrayList();
        secondActivitiesList = new ArrayList();
        if (first != null) {
            firstActivitiesList = TimeRanger.selectFilteredActivities(name1,
                    start1, end1, tags1, untagged1);
            secondActivitiesList = TimeRanger.selectFilteredActivities(name2,
                    start2, end2, tags2, untagged2);
        }
        // Updating panes
        updateFilter();
        updateDistribution(tags1, untagged1, tags2, untagged2);
        updateProperties(tags1, tags2);
        updateDynamics(firstStart, lastEnd);
    }

    /**
     * Update filter pane contents
     */
    private void updateFilter() {
        filterFirstTagsChipView.getSuggestions().setAll(TimeRanger.getTags());
        filterSecondTagsChipView.getSuggestions().setAll(TimeRanger.getTags());
    }

    /**
     * Update properties pane contents
     *
     * @param tags1 tags of first set
     * @param tags2 tags of second set
     */
    private void updateProperties(List<Tag> tags1, List<Tag> tags2) {
        HashMap<String, Long> props1 = Analysis.calcProperties(firstActivitiesList),
                props2 = Analysis.calcProperties(secondActivitiesList);
        ArrayList<AnalysisProperty> properties = new ArrayList();
        for (Entry<String, Long> e : props1.entrySet()) {
            long v1 = e.getValue();
            long v2 = props2.get(e.getKey());
            long v3 = v1 - v2;
            long d = Math.max(v1, v2);
            String div = "sec";
            int divisor = 1;
            if (d > 60 * 5 && d <= 3600 * 5) { // < 5 min
                div = "mins";
                divisor = 60;
            } else if (d > 3600 * 5 && d <= 86400 * 5) {
                div = "hrs";
                divisor = 3600;
            } else if (d > 86400 * 5 && d <= 2592000 * 5) {
                div = "days";
                divisor = 86400;
            } else if (d > 2592000 * 5 && d <= 31104000 * 5) {
                div = "months";
                divisor = 2592000;
            } else if (d > 31104000 * 5) {
                div = "years";
                divisor = 31104000;
            }
            v1 /= divisor;
            v2 /= divisor;
            v3 /= divisor;
            AnalysisProperty property = new AnalysisProperty();
            property.setName(e.getKey());
            property.setFirstValue(v1 + " " + div);
            property.setSecondValue(v2 + " " + div);
            property.setDifference(v3 + " " + div);
            properties.add(property);
        }
        AnalysisProperty property = new AnalysisProperty();
        property.setName("Count");
        property.setFirstValue(firstActivitiesList.size() + "");
        property.setSecondValue(secondActivitiesList.size() + "");
        property.setDifference((firstActivitiesList.size() - secondActivitiesList.size()) + "");
        properties.add(property);
        propertiesTableView.getItems().setAll(properties);
    }

    /**
     * Update distribution pane contents
     *
     * @param tags1 tags of first set
     * @param untagged1 handling mode for untagged activities of first set
     * @param tags2 tags of second set
     * @param untagged2 handling mode for untagged activities of second set
     */
    private void updateDistribution(List<Tag> tags1, UntaggedSliderValue untagged1,
            List<Tag> tags2, UntaggedSliderValue untagged2) {
        HashMap<Tag, Double> distrib1 = Analysis.calcDistribution(firstActivitiesList, tags1, untagged1),
                distrib2 = Analysis.calcDistribution(secondActivitiesList, tags2, untagged2);
        updateDistributionPieChart(distributionFirstPieChart, distrib1);
        updateDistributionPieChart(distributionSecondPieChart, distrib2);
    }

    /**
     * Update distribution pie chart values
     *
     * @param pieChart pie chart component
     * @param values map of distribution
     */
    private void updateDistributionPieChart(PieChart pieChart, HashMap<Tag, Double> values) {
        double total = 0;
        for (double value : values.values()) {
            total += value;
        }
        HashMap<Tag, PieChart.Data> map = new HashMap();
        for (Tag tag : values.keySet()) {
            double value = values.get(tag);
            double percentage = 100.0 * value / total;
            map.put(tag, new PieChart.Data((tag == null ? "< untagged >" : tag.getName()) + "\n"
                    + String.format("%.1f%%", percentage), value));
        }
        pieChart.getData().setAll(map.values());
        for (Tag tag : values.keySet()) {
            Color background = ColorUtil.getColor(tag == null ? 0 : tag.getColor());
            PieChart.Data data = map.get(tag);
            data.getNode().setStyle("-fx-pie-color: " + ColorUtil.getHex(background));
        }
    }

    /**
     * Update dynamics pane
     *
     * @param firstStart first activity's start time bound
     * @param lastEnd last activity's end time bound
     */
    private void updateDynamics(LocalDateTime firstStart, LocalDateTime lastEnd) {
        CategoryAxis xAxis = (CategoryAxis) dynamicsLineChart.getXAxis();
        NumberAxis yAxis = (NumberAxis) dynamicsLineChart.getYAxis();
        DynamicsView view = DynamicsView.getByName(dynamicsViewChoiceBox.getValue());
        firstStart = view.truncate(firstStart);
        // Calculating divisions count
        long divCount = view.getUnit().between(firstStart, lastEnd);
        if (view.getUnit().between(firstStart, lastEnd.minus(1, ChronoUnit.SECONDS)) == divCount) {
            divCount++;
        }
        // Calculating values
        double d = 0;
        XYChart.Series<String, Number> series1 = new XYChart.Series(), series2 = new XYChart.Series();
        series1.setName("First value");
        series2.setName("Second value");
        for (int i = 0; i < divCount; i++) {
            LocalDateTime start = firstStart.plus(i, view.getUnit());
            LocalDateTime end = start.plus(1, view.getUnit());
            String name = view.format(start);
            double value1 = Analysis.calcDuration(firstActivitiesList, start, end);
            double value2 = Analysis.calcDuration(secondActivitiesList, start, end);
            d = Math.max(d, Math.max(value1, value2));
            series1.getData().add(new XYChart.Data(name, value1));
            series2.getData().add(new XYChart.Data(name, value2));
        }
        final String div;
        final int divisor;
        if (d <= 60 * 5) {
            div = "sec";
            divisor = 1;
        } else if (d > 60 * 5 && d <= 3600 * 5) { // < 5 min
            div = "mins";
            divisor = 60;
        } else if (d > 3600 * 5 && d <= 86400 * 5) {
            div = "hrs";
            divisor = 3600;
        } else if (d > 86400 * 5 && d <= 2592000 * 5) {
            div = "days";
            divisor = 86400;
        } else if (d > 2592000 * 5 && d <= 31104000 * 5) {
            div = "months";
            divisor = 2592000;
        } else {
            div = "years";
            divisor = 31104000;
        }
        yAxis.setTickUnit(divisor);
        yAxis.setTickLabelFormatter(new StringConverter<Number>() {
            @Override
            public String toString(Number object) {
                double d = ((double) object) / divisor;
                return String.format(d % 1.0 != 0 ? "%.2f" : "%.0f", d);
            }

            @Override
            public Number fromString(String string) {
                return Double.valueOf(string) * divisor;
            }
        });
        yAxis.setLowerBound(0);
        yAxis.setUpperBound(d);
        yAxis.setLabel("Duration, " + div);
        dynamicsLineChart.getData().setAll(series1, series2);
        series1.getNode().setStyle("-fx-stroke: #f00;");
        series2.getNode().setStyle("-fx-stroke: #f0f;");
    }

}
