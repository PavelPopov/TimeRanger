package timeranger.logic.controller.tab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.beans.Observable;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.logic.operation.TableSelectionOperation;
import timeranger.logic.operation.TagListModelOperation;
import timeranger.logic.operation.TagModelOperation;
import timeranger.model.Tag;
import timeranger.view.factory.EditableTableColorCell;
import timeranger.view.factory.EditableTableNameCell;
import timeranger.view.factory.EditableTableTextCell;

/**
 * Controller of tags tab
 *
 * @author PavelPopov
 */
public final class TagsTabController {

    private final Tab tab;
    private final TextField textField;
    private final TableView<Tag> tableView;
    private final TableColumn<Tag, String> tableViewNameColumn;
    private final TableColumn<Tag, Integer> tableViewColorColumn;
    private final TableColumn<Tag, String> tableViewDescriptionColumn;
    private final ArrayList<Integer> oldSelection = new ArrayList();

    public TagsTabController(Tab tab, TextField textField, TableView<Tag> tableView,
            TableColumn<Tag, String> tableViewNameColumn,
            TableColumn<Tag, Integer> tableViewColorColumn,
            TableColumn<Tag, String> tableViewDescriptionColumn) {
        this.tab = tab;
        this.textField = textField;
        this.tableView = tableView;
        this.tableViewNameColumn = tableViewNameColumn;
        this.tableViewColorColumn = tableViewColorColumn;
        this.tableViewDescriptionColumn = tableViewDescriptionColumn;
    }

    /**
     * Initialize tab's components
     */
    public final void init() {
        tableView.setEditable(true);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tableView.getSelectionModel().getSelectedIndices().addListener((Observable observable) -> {
            ArrayList<Integer> oldValue = new ArrayList(oldSelection);
            ArrayList<Integer> newValue = new ArrayList(tableView.getSelectionModel().getSelectedIndices());
            // TODO: WOW! What a dirty hack!
            if (tableView.getUserData() == null || !((Boolean) tableView.getUserData())) {
                TimeRanger.addOperation(new TableSelectionOperation(tableView, oldValue, newValue, tab));
            }
            oldSelection.clear();
            oldSelection.addAll(tableView.getSelectionModel().getSelectedIndices());
        });
        tableViewNameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        tableViewNameColumn.setCellFactory((col) -> {
            return new EditableTableNameCell(col);
        });
        tableViewNameColumn.setOnEditCommit((event) -> {
            Tag oldTag = new Tag(event.getRowValue());
            oldTag.setName(event.getOldValue());
            Tag newTag = new Tag(oldTag);
            newTag.setName(event.getNewValue());
            onTableEditCommit(oldTag, newTag);
        });
        tableViewColorColumn.setCellValueFactory(new PropertyValueFactory("color"));
        tableViewColorColumn.setCellFactory((col) -> {
            return new EditableTableColorCell(col);
        });
        tableViewColorColumn.setOnEditCommit((event) -> {
            Tag oldTag = new Tag(event.getRowValue());
            oldTag.setColor(event.getOldValue());
            Tag newTag = new Tag(oldTag);
            newTag.setColor(event.getNewValue());
            onTableEditCommit(oldTag, newTag);
        });
        tableViewDescriptionColumn.setCellValueFactory(new PropertyValueFactory("description"));
        tableViewDescriptionColumn.setCellFactory((col) -> {
            return new EditableTableTextCell(col);
        });
        tableViewDescriptionColumn.setOnEditCommit((event) -> {
            Tag oldTag = new Tag(event.getRowValue());
            oldTag.setDescription(event.getOldValue());
            Tag newTag = new Tag(oldTag);
            newTag.setDescription(event.getNewValue());
            onTableEditCommit(oldTag, newTag);
        });
    }

    /**
     * Update tab's content
     */
    public final void update() {
        tableView.setUserData(true);
        tableView.getItems().setAll(TimeRanger.getTags());
        tableView.getItems().sort((o1, o2) -> {
            return o1.getName().compareTo(o2.getName());
        });
        tableView.refresh();
        tableView.setUserData(false);
    }

    /**
     * Add tag
     */
    public final void onAdd() {
        String name = textField.getText().trim().replaceAll("\t", "")
                .replace("<", "").replace(">", "");
        if (name.isEmpty()) {
            return;
        }
        Tag tag = new Tag();
        tag.setName(name);
        tag.setColor(0);
        tag.setDescription("");
        addTag(tag);
        textField.clear();
    }

    /**
     * Delete tags which are currently selected in table view
     */
    public final void deleteSelected() {
        ArrayList<Tag> selected = new ArrayList(tableView
                .getSelectionModel().getSelectedItems());
        if (selected.size() > 0) {
            deleteTags(selected);
        }
    }

    /**
     * Select all loaded tags
     */
    public final void selectAll() {
        tableView.getSelectionModel().selectAll();
    }

    /**
     * Invert selection of tags
     */
    public final void invertSelection() {
        int count = tableView.getItems().size() - tableView.getSelectionModel().getSelectedIndices().size();
        int[] indices = new int[count];
        for (int i = 0, k = 0; i < tableView.getItems().size() && k < count; i++) {
            if (tableView.getSelectionModel().getSelectedIndices().contains(i)) {
                continue;
            }
            indices[k++] = i;
        }
        tableView.getSelectionModel().clearSelection();
        tableView.getSelectionModel().selectIndices(-1, indices);
    }

    /**
     * Handle table edit commit event
     *
     * @param tag tag model
     */
    private void onTableEditCommit(Tag oldTag, Tag newTag) {
        if (newTag.getName().trim().isEmpty()) {
            deleteTags(Arrays.asList(oldTag));
        } else {
            updateTag(oldTag, newTag);
        }
    }

    /**
     * Add tag
     *
     * @param tag tag model
     */
    private void addTag(Tag tag) {
        TimeRanger.addOperation(new TagModelOperation(null, tag, CRUDMethod.CREATE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.addTag(new Tag(tag));
        });
    }

    /**
     * Update tag
     *
     * @param tag tag model
     */
    private void updateTag(Tag oldTag, Tag newTag) {
        TimeRanger.addOperation(new TagModelOperation(oldTag, newTag, CRUDMethod.UPDATE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.updateTag(new Tag(newTag));
        });
    }

    /**
     * Delete tags
     *
     * @param tags list of tags models
     */
    private void deleteTags(List<Tag> tags) {
        TimeRanger.addOperation(new TagListModelOperation(tags, null, CRUDMethod.DELETE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.deleteTags(new ArrayList(tags));
        });
    }

}
