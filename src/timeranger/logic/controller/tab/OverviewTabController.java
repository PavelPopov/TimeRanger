package timeranger.logic.controller.tab;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javafx.beans.Observable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import jfxtras.scene.control.agenda.Agenda;
import jfxtras.scene.control.agenda.Agenda.Appointment;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.logic.operation.ActivityListModelOperation;
import timeranger.logic.operation.ActivityModelOperation;
import timeranger.logic.operation.AgendaSelectionOperation;
import timeranger.model.Activity;
import timeranger.view.factory.AgendaFilteredSkin;
import timeranger.view.factory.AgendaSkinSwitcher;

/**
 * Controller of overview tab
 *
 * @author PavelPopov
 */
public final class OverviewTabController {

    private final Tab tab;
    private final Button filterButton;
    private final TextField searchTextField;
    private final DatePicker startDatePicker;
    private final DatePicker endDatePicker;
    private final Agenda agenda;
    private final AgendaSkinSwitcher agendaSkinSwitcher;
    /**
     * Map of agenda appointments to activities needed for getting activities from agenda appointments
     */
    private final HashMap<Appointment, Activity> appointmentActivityMap = new HashMap();
    private final ArrayList<Appointment> oldSelection = new ArrayList();

    public OverviewTabController(Tab tab, Button filterButton,
            TextField searchTextField, DatePicker startDatePicker, DatePicker endDatePicker,
            Agenda agenda, AgendaSkinSwitcher agendaSkinSwitcher) {
        this.tab = tab;
        this.filterButton = filterButton;
        this.searchTextField = searchTextField;
        this.startDatePicker = startDatePicker;
        this.endDatePicker = endDatePicker;
        this.agenda = agenda;
        this.agendaSkinSwitcher = agendaSkinSwitcher;
    }

    /**
     * Initialize tab components
     */
    public final void init() {
        filterButton.setOnAction((event) -> {
            filterActivities();
        });
        agendaSkinSwitcher.setAgenda(agenda);
        agenda.setAllowDragging(true);
        agenda.setAllowResize(true);
        agenda.setOnMouseClicked((event) -> {
            agenda.requestFocus();
        });
        agenda.setNewAppointmentCallback((dateTimeRange) -> {
            Activity activity = new Activity();
            activity.setName("new");
            activity.setStart(dateTimeRange.getStartLocalDateTime());
            activity.setEnd(dateTimeRange.getEndLocalDateTime());
            addActivity(activity);
            return makeAppointmentFromActivity(activity);
        });
        agenda.setLocalDateTimeRangeCallback((dateTimeRange) -> {
            // Skip if it's filtered skin, because we already got everything
            if (agenda.getSkin() instanceof AgendaFilteredSkin) {
                return null;
            }
            TimeRanger.setOverviewFilterName("");
            TimeRanger.setOverviewFilterStart(dateTimeRange.getStartLocalDateTime().toLocalDate());
            TimeRanger.setOverviewFilterEnd(dateTimeRange.getEndLocalDateTime().toLocalDate());
            TimeRanger.updateOverview();
            return null;
        });
        agenda.setAppointmentChangedCallback((appointment) -> {
            Activity oldActivity = appointmentActivityMap.get(appointment);
            Activity newActivity = new Activity(oldActivity);
            newActivity.setStart(appointment.getStartLocalDateTime());
            newActivity.setEnd(appointment.getEndLocalDateTime());
            if (newActivity.getEnd() == null) {
                newActivity.setEnd(appointment.getStartLocalDateTime().plusMinutes(30));
            }
            updateActivity(oldActivity, newActivity);
            return null;
        });
        agenda.setActionCallback((appointment) -> {
            Activity activity = appointmentActivityMap.get(appointment);
            TimeRanger.setActivityUnderEdit(activity);
            TimeRanger.getActivityEditStage().show();
            return null;
        });
        agenda.setEditAppointmentCallback((appointment) -> {
            ArrayList<Activity> activities = new ArrayList();
            activities.add(appointmentActivityMap.get(appointment));
            deleteActivities(activities);
            appointmentActivityMap.remove(appointment);
            return null;
        });
        agenda.setDisplayedLocalDateTime(LocalDateTime.now());
        agenda.selectedAppointments().addListener((Observable observable) -> {
            ArrayList<Appointment> oldValue = new ArrayList(oldSelection);
            ArrayList<Appointment> newValue = new ArrayList(agenda.selectedAppointments());
            // TODO: WOW! What a dirty hack!
            if (agenda.getUserData() == null || !((Boolean) agenda.getUserData())) {
                TimeRanger.addOperation(new AgendaSelectionOperation(agenda, oldValue, newValue, tab));
            }
            oldSelection.clear();
            oldSelection.addAll(agenda.selectedAppointments());
        });
        startDatePicker.setValue(LocalDate.now());
        endDatePicker.setValue(LocalDate.now().plusDays(1));
        StringConverter<LocalDate> stringConverter = new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate t) {
                return t.format(TimeRanger.DATE_FORMATTER);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.parse(string, TimeRanger.DATE_FORMATTER);
            }
        };
        startDatePicker.setConverter(stringConverter);
        endDatePicker.setConverter(stringConverter);
        searchTextField.setOnKeyPressed((event) -> {
            switch (event.getCode()) {
                case ENTER:
                    filterActivities();
                    break;
                default:
                    break;
            }
        });
    }

    /**
     * Generate agenda's appointment from given activity
     *
     * @param activity activity model
     * @return agenda's appointment
     */
    private final Appointment makeAppointmentFromActivity(Activity activity) {
        Appointment appointment = new Agenda.AppointmentImplLocal()
                .withStartLocalDateTime(activity.getStart())
                .withEndLocalDateTime(activity.getEnd())
                .withSummary(activity.getName())
                .withDescription(activity.getName())
                .withWholeDay(activity.getEnd() == null)
                .withAppointmentGroup(agenda.appointmentGroups().get(0));
        appointmentActivityMap.put(appointment, activity);
        return appointment;
    }

    /**
     * Add activity
     *
     * @param activity activity model
     */
    private final void addActivity(Activity activity) {
        TimeRanger.addOperation(new ActivityModelOperation(null, activity, CRUDMethod.CREATE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.addActivity(activity);
        });
    }

    /**
     * Delete activities
     *
     * @param activities list of activities to delete
     */
    private final void deleteActivities(List<Activity> activities) {
        TimeRanger.addOperation(new ActivityListModelOperation(activities, null, CRUDMethod.DELETE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.deleteActivities(activities);
        });
    }

    /**
     * Update activity
     *
     * @param activity activity model
     */
    private final void updateActivity(Activity oldActivity, Activity newActivity) {
        TimeRanger.addOperation(new ActivityModelOperation(oldActivity, newActivity, CRUDMethod.UPDATE, tab));
        TimeRanger.runLater(() -> {
            TimeRanger.updateActivity(newActivity);
        });
    }

    /**
     * Update tab's content
     */
    public final void update() {
        agenda.setUserData(true);
        agenda.appointments().clear();
        List<Activity> activities = TimeRanger.getActivities();
        List<Appointment> list = new ArrayList(activities.size());
        for (Activity activity : TimeRanger.getActivities()) {
            list.add(makeAppointmentFromActivity(activity));
        }
        agenda.appointments().addAll(list);
        agenda.setUserData(false);
    }

    /**
     * Filter activities using filter fields' values
     */
    public final void filterActivities() {
        String name = searchTextField.getText().trim();
        // If user entered spaces - delete them
        if (name.isEmpty()) {
            searchTextField.clear();
        }
        LocalDate start = startDatePicker.getValue();
        LocalDate end = endDatePicker.getValue();
        if (ChronoUnit.DAYS.between(start, end) <= 0) {
            return;
        }
        TimeRanger.setOverviewFilterName(name);
        TimeRanger.setOverviewFilterStart(start);
        TimeRanger.setOverviewFilterEnd(end);
        TimeRanger.updateOverview();
        agenda.setSkin(new AgendaFilteredSkin(agenda, start, end));
    }

    /**
     * Delete activities which are currently selected in agenda
     */
    public final void deleteSelected() {
        ArrayList<Activity> activities = new ArrayList();
        for (Appointment appointment : agenda.selectedAppointments()) {
            activities.add(appointmentActivityMap.get(appointment));
            appointmentActivityMap.remove(appointment);
        }
        if (activities.size() > 0) {
            deleteActivities(activities);
        }
    }

    /**
     * Select all loaded activities
     */
    public final void selectAll() {
        agenda.selectedAppointments().setAll(agenda.appointments());
    }

    /**
     * Invert selection of loaded activities
     */
    public final void invertSelection() {
        ArrayList<Appointment> appointments = new ArrayList(agenda.appointments());
        appointments.removeAll(agenda.selectedAppointments());
        agenda.selectedAppointments().setAll(appointments);
    }

    /**
     * Get activity from given appointment
     */
    public final Activity getActivityFromAppointment(Appointment appointment) {
        return appointmentActivityMap.get(appointment);
    }
}
