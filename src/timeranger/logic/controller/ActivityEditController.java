package timeranger.logic.controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import jfxtras.scene.control.LocalDateTimeTextField;
import timeranger.TimeRanger;
import timeranger.model.Activity;
import timeranger.model.Tag;
import timeranger.view.factory.TagChip;
import timeranger.view.factory.TagChipView;

/**
 * Activity edit stage controller
 *
 * @author PavelPopov
 */
public class ActivityEditController implements Initializable {

    /**
     * Name text field
     */
    @FXML
    private TextField activityEditNameTextField;

    /**
     * Start timestamp field
     */
    @FXML
    private LocalDateTimeTextField activityEditStartDateField;

    /**
     * End timestamp field
     */
    @FXML
    private LocalDateTimeTextField activityEditEndDateField;

    /**
     * Tags chip view field
     */
    @FXML
    private TagChipView<Tag> activityEditTagsChipViewField;

    /**
     * Submit button
     */
    @FXML
    private Button activityEditSubmitButton;

    /**
     * Controller instance
     */
    private static ActivityEditController instance;

    /**
     * Load controller's stage
     *
     * @param primaryStage owner stage
     * @return controller's stage
     */
    public static Stage loadStage(Stage primaryStage) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TimeRanger.class.getResource("view/fxml/ActivityEdit.fxml"));
            AnchorPane layout = (AnchorPane) loader.load();
            Scene scene = new Scene(layout);
            Stage stage = new Stage();
            stage.getIcons().add(new Image(TimeRanger.class.getResourceAsStream("view/resource/icon.png")));
            stage.setTitle("Activity editing");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(primaryStage);
            stage.setMinWidth(320);
            stage.setMinHeight(200);
            stage.setResizable(false);
            stage.setScene(scene);
            stage.setOnShowing((event) -> {
                Activity activity = TimeRanger.getActivityUnderEdit();
                String activityName = "";
                String activityEnd = "";
                String activityStart = "";
                if (activity != null) {
                    activityName = activity.getName();
                    activityStart = activity.getStart().format(TimeRanger.DATETIME_FORMATTER);
                    activityEnd = activity.getEnd() == null ? ""
                            : activity.getEnd().format(TimeRanger.DATETIME_FORMATTER);
                }
                instance.activityEditNameTextField.setText(activityName);
                instance.activityEditStartDateField.setText(activityStart);
                instance.activityEditStartDateField.setLocalDateTime(activity.getStart());
                instance.activityEditEndDateField.setText(activityEnd);
                if (activity.getEnd() != null) {
                    instance.activityEditEndDateField.setLocalDateTime(activity.getEnd());
                }
                instance.activityEditTagsChipViewField.setConverter(new StringConverter<Tag>() {
                    @Override
                    public String toString(Tag t) {
                        return t == null ? null : t.getName();
                    }

                    @Override
                    public Tag fromString(String name) {
                        for (Tag tag : TimeRanger.getTags()) {
                            if (tag.getName().equals(name)) {
                                return tag;
                            }
                        }
                        return new Tag(0, name, 0, "");
                    }
                });
                // TODO: change location of tag creation method or make something
                // to avoid association with root frame
                instance.activityEditTagsChipViewField.setChipFactory((chipView, tag) -> {
                    return new TagChip<Tag>(chipView, tag);
                });
                instance.activityEditTagsChipViewField.getSuggestions().setAll(TimeRanger.getTags());
                if (activity.getTags() != null) {
                    instance.activityEditTagsChipViewField.getChips().setAll(activity.getTags());
                }
            });
            return stage;
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load ActivityEdit stage", ex);
        }
    }

    /**
     * Initialize the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        StringConverter<LocalDate> stringConverter = new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate t) {
                return t.format(TimeRanger.DATE_FORMATTER);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.parse(string, TimeRanger.DATE_FORMATTER);
            }
        };
        activityEditStartDateField.setDateTimeFormatter(TimeRanger.DATETIME_FORMATTER);
        activityEditEndDateField.setDateTimeFormatter(TimeRanger.DATETIME_FORMATTER);
        instance = this;
    }

    @FXML
    private void onSubmit(ActionEvent event) {
        Activity activity = TimeRanger.getActivityUnderEdit();
        activity.setName(activityEditNameTextField.getText());
        activity.setStart(activityEditStartDateField.getLocalDateTime());
        activity.setEnd(activityEditEndDateField.getLocalDateTime());
        ArrayList<Tag> ts = new ArrayList();
        // TODO: fix bug that blocks assignment of tags to activities (DB table is empty)
        for (Tag t : activityEditTagsChipViewField.getChips()) {
            Tag tag = null;
            for (Tag tt : TimeRanger.getTags()) {
                if (tt.getName().equals(t.getName())) {
                    tag = tt;
                }
            }
            if (tag == null) {
                TimeRanger.addTag(t);
            }
            tag = null;
            for (Tag t2 : TimeRanger.getTags()) {
                if (t2.getName().equals(t.getName())) {
                    tag = t2;
                }
            }
            if (tag != null) {
                boolean found = false;
                for (Tag e : ts) {
                    if (e.getName().equals(tag.getName())) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    ts.add(tag);
                }
            }
        }
        TimeRanger.updateActivityTags(activity, ts);
        TimeRanger.updateActivity(activity);
        TimeRanger.getActivityEditStage().close();
    }

}
