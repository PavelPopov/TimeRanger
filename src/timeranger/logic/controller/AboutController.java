package timeranger.logic.controller;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import timeranger.TimeRanger;

/**
 * About stage controller
 *
 * @author PavelPopov
 */
public class AboutController implements Initializable {

    /**
     * Title of about stage
     */
    @FXML
    private Label title;

    @FXML
    private Hyperlink emailHyperlink;

    @FXML
    private Hyperlink repoHyperlink;

    /**
     * Load controller's stage
     *
     * @param primaryStage owner stage
     * @return controller's stage
     */
    public static Stage loadStage(Stage primaryStage) {
        Stage stage = null;
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TimeRanger.class.getResource("view/fxml/About.fxml"));
            HBox layout = (HBox) loader.load();
            Scene scene = new Scene(layout);
            stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(primaryStage);
            stage.setTitle("About TimeRanger");
            stage.setResizable(false);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load About stage", ex);
        }
        return stage;
    }

    /**
     * Initialize the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        title.setText("TimeRanger " + TimeRanger.VERSION);
        EventHandler<ActionEvent> hyperlinkAction = (ActionEvent event) -> {
            Hyperlink h = (Hyperlink) event.getTarget();
            String s = h.getTooltip().getText();
            Task<Void> task = new Task() {
                @Override
                protected Void call() throws Exception {
                    try {
                        if (Desktop.isDesktopSupported()
                                && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
                            Desktop.getDesktop().browse(new URI(s));
                        } else {
                            System.err.println("Desktop browsing unsupported");
                        }
                    } catch (URISyntaxException | IOException ex) {
                        Logger.getLogger(AboutController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return null;
                }
            };
            Thread thread = new Thread(task);
            thread.setDaemon(true);
            thread.start();
            event.consume();
        };
        emailHyperlink.setOnAction(hyperlinkAction);
        repoHyperlink.setOnAction(hyperlinkAction);
    }

}
