package timeranger.logic;

/**
 * Analysis property model
 *
 * @author PavelPopov
 */
public class AnalysisProperty {

    /**
     * Name of property
     */
    private String name;
    /**
     * Value for first set
     */
    private String firstValue;
    /**
     * Value for second set
     */
    private String secondValue;
    /**
     * Difference between sets
     */
    private String difference;

    public AnalysisProperty() {
    }

    public AnalysisProperty(String name, String firstValue, String secondValue, String difference) {
        this.name = name;
        this.firstValue = firstValue;
        this.secondValue = secondValue;
        this.difference = difference;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the firstValue
     */
    public String getFirstValue() {
        return firstValue;
    }

    /**
     * @param firstValue the firstValue to set
     */
    public void setFirstValue(String firstValue) {
        this.firstValue = firstValue;
    }

    /**
     * @return the secondValue
     */
    public String getSecondValue() {
        return secondValue;
    }

    /**
     * @param secondValue the secondValue to set
     */
    public void setSecondValue(String secondValue) {
        this.secondValue = secondValue;
    }

    /**
     * @return the difference
     */
    public String getDifference() {
        return difference;
    }

    /**
     * @param difference the difference to set
     */
    public void setDifference(String difference) {
        this.difference = difference;
    }
}
