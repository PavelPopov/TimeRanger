package timeranger.logic.operation;

import java.util.Arrays;
import javafx.scene.control.Tab;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.model.Tag;

/**
 * A model operation on a tag
 *
 * @author PavelPopov
 */
public class TagModelOperation extends ModelOperation<Tag> {

    public TagModelOperation(Tag oldValue, Tag newValue, CRUDMethod method, Tab tab) {
        super(oldValue, newValue, method, tab);
    }

    @Override
    protected void add(Tag value) {
        TimeRanger.addTag(value);
    }

    @Override
    protected void update(Tag oldValue, Tag newValue) {
        newValue.setId(oldValue.getId());
        TimeRanger.updateTag(newValue);
    }

    @Override
    protected void delete(Tag value) {
        TimeRanger.deleteTags(Arrays.asList(value));
    }

}
