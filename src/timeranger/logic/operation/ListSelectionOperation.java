package timeranger.logic.operation;

import java.util.List;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import org.apache.commons.lang3.ArrayUtils;
import timeranger.logic.Operation;

/**
 * Operation of changing selection on a list
 *
 * @author PavelPopov
 */
public class ListSelectionOperation extends Operation<List<Integer>> {

    protected final ListView listView;

    public ListSelectionOperation(ListView listView, List<Integer> oldValue, List<Integer> newValue, Tab tab) {
        super(oldValue, newValue, tab);
        this.listView = listView;
    }

    @Override
    public void undo() {
        int[] indices = ArrayUtils.toPrimitive(oldValue.toArray(new Integer[oldValue.size()]));
        // TODO: WOW! What a dirty hack!
        listView.setUserData(true);
        listView.getSelectionModel().clearSelection();
        listView.getSelectionModel().selectIndices(-1, indices);
        listView.setUserData(false);
    }

    @Override
    public void redo() {
        int[] indices = ArrayUtils.toPrimitive(newValue.toArray(new Integer[newValue.size()]));
        // TODO: WOW! What a dirty hack!
        listView.setUserData(true);
        listView.getSelectionModel().clearSelection();
        listView.getSelectionModel().selectIndices(-1, indices);
        listView.setUserData(false);
    }

}
