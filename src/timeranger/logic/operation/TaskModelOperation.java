package timeranger.logic.operation;

import java.util.Arrays;
import javafx.scene.control.Tab;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.model.Task;

/**
 * A model operation on a task
 *
 * @author PavelPopov
 */
public class TaskModelOperation extends ModelOperation<Task> {

    public TaskModelOperation(Task oldValue, Task newValue, CRUDMethod method, Tab tab) {
        super(oldValue, newValue, method, tab);
    }

    @Override
    protected void add(Task value) {
        TimeRanger.addTask(value);
    }

    @Override
    protected void update(Task oldValue, Task newValue) {
        newValue.setId(oldValue.getId());
        TimeRanger.updateTask(new Task(newValue));
    }

    @Override
    protected void delete(Task value) {
        TimeRanger.deleteTasks(Arrays.asList(value));
    }

}
