package timeranger.logic.operation;

import java.util.Arrays;
import java.util.List;
import javafx.scene.control.Tab;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.model.Task;

/**
 * A model operation on tasks list
 *
 * @author PavelPopov
 */
public class TaskListModelOperation extends ModelListOperation<Task> {

    public TaskListModelOperation(List<Task> oldValue, List<Task> newValue, CRUDMethod method, Tab tab) {
        super(oldValue, newValue, method, tab);
    }

    @Override
    protected void add(List<Task> value) {
        value.forEach((t) -> {
            TimeRanger.addTask(t);
        });
    }

    @Override
    protected void update(List<Task> oldValue, List<Task> newValue) {
        int oldCount = oldValue == null ? 0 : oldValue.size();
        int newCount = newValue == null ? 0 : newValue.size();
        int count = Math.max(oldCount, newCount);
        for (int i = 0; i < count; i++) {
            Task oldVal = null, newVal = null;
            try {
                oldVal = oldValue.get(i);
            } catch (Exception ex) {
            }
            try {
                newVal = newValue.get(i);
            } catch (Exception ex) {
            }
            if (newVal == null) {
                delete(Arrays.asList(oldVal));
            } else if (oldVal == null) {
                add(Arrays.asList(newVal));
            } else {
                newVal.setId(oldVal.getId());
                TimeRanger.updateTask(new Task(newVal));
            }
        }
    }

    @Override
    protected void delete(List<Task> value) {
        TimeRanger.deleteTasks(value);
    }

}
