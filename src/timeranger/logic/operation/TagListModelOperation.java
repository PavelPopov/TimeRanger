package timeranger.logic.operation;

import java.util.Arrays;
import java.util.List;
import javafx.scene.control.Tab;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.model.Tag;

/**
 * A model operation on tags list
 *
 * @author PavelPopov
 */
public class TagListModelOperation extends ModelListOperation<Tag> {

    public TagListModelOperation(List<Tag> oldValue, List<Tag> newValue, CRUDMethod method, Tab tab) {
        super(oldValue, newValue, method, tab);
    }

    @Override
    protected void add(List<Tag> value) {
        value.forEach((t) -> {
            TimeRanger.addTag(t);
        });
    }

    @Override
    protected void update(List<Tag> oldValue, List<Tag> newValue) {
        int oldCount = oldValue == null ? 0 : oldValue.size();
        int newCount = newValue == null ? 0 : newValue.size();
        int count = Math.max(oldCount, newCount);
        for (int i = 0; i < count; i++) {
            Tag oldVal = null, newVal = null;
            try {
                oldVal = oldValue.get(i);
            } catch (Exception ex) {
            }
            try {
                newVal = newValue.get(i);
            } catch (Exception ex) {
            }
            if (newVal == null) {
                delete(Arrays.asList(oldVal));
            } else if (oldVal == null) {
                add(Arrays.asList(newVal));
            } else {
                newVal.setId(oldVal.getId());
                TimeRanger.updateTag(newVal);
            }
        }
    }

    @Override
    protected void delete(List<Tag> value) {
        TimeRanger.deleteTags(value);
    }

}
