package timeranger.logic.operation;

import java.util.List;
import javafx.scene.control.Tab;
import javafx.scene.control.TableView;
import org.apache.commons.lang3.ArrayUtils;
import timeranger.logic.Operation;

/**
 * Operation of changing selection on a table
 *
 * @author PavelPopov
 */
public class TableSelectionOperation extends Operation<List<Integer>> {

    protected final TableView tableView;

    public TableSelectionOperation(TableView tableView, List<Integer> oldValue, List<Integer> newValue, Tab tab) {
        super(oldValue, newValue, tab);
        this.tableView = tableView;
    }

    @Override
    public void undo() {
        int[] indices = ArrayUtils.toPrimitive(oldValue.toArray(new Integer[oldValue.size()]));
        // TODO: WOW! What a dirty hack!
        tableView.setUserData(true);
        tableView.getSelectionModel().clearSelection();
        tableView.getSelectionModel().selectIndices(-1, indices);
        tableView.setUserData(false);
    }

    @Override
    public void redo() {
        int[] indices = ArrayUtils.toPrimitive(newValue.toArray(new Integer[newValue.size()]));
        // TODO: WOW! What a dirty hack!
        tableView.setUserData(true);
        tableView.getSelectionModel().clearSelection();
        tableView.getSelectionModel().selectIndices(-1, indices);
        tableView.setUserData(false);
    }

}
