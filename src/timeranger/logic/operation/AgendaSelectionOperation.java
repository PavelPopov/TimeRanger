package timeranger.logic.operation;

import java.util.List;
import javafx.scene.control.Tab;
import jfxtras.scene.control.agenda.Agenda;
import jfxtras.scene.control.agenda.Agenda.Appointment;
import timeranger.logic.Operation;

/**
 * Operation of changing selection on an agenda
 *
 * @author PavelPopov
 */
public class AgendaSelectionOperation extends Operation<List<Appointment>> {

    protected final Agenda agenda;

    public AgendaSelectionOperation(Agenda agenda,
            List<Appointment> oldValue, List<Appointment> newValue, Tab tab) {
        super(oldValue, newValue, tab);
        this.agenda = agenda;
    }

    @Override
    public void undo() {
        // TODO: WOW! What a dirty hack!
        agenda.setUserData(true);
        agenda.selectedAppointments().setAll(oldValue);
        agenda.setUserData(false);
    }

    @Override
    public void redo() {
        // TODO: WOW! What a dirty hack!
        agenda.setUserData(true);
        agenda.selectedAppointments().setAll(newValue);
        agenda.setUserData(false);
    }

}
