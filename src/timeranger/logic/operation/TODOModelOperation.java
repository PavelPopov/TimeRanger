package timeranger.logic.operation;

import java.util.Arrays;
import javafx.scene.control.Tab;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.model.TODO;

/**
 * A model operation on a TODO
 *
 * @author PavelPopov
 */
public class TODOModelOperation extends ModelOperation<TODO> {

    public TODOModelOperation(TODO oldValue, TODO newValue, CRUDMethod method, Tab tab) {
        super(oldValue, newValue, method, tab);
    }

    @Override
    protected void add(TODO value) {
        TimeRanger.addTODO(value);
    }

    @Override
    protected void update(TODO oldValue, TODO newValue) {
        TimeRanger.updateTODO(oldValue.getName(), newValue);
    }

    @Override
    protected void delete(TODO value) {
        TimeRanger.deleteTODOs(Arrays.asList(value));
    }

}
