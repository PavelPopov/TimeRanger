package timeranger.logic.operation;

import java.util.Arrays;
import java.util.List;
import javafx.scene.control.Tab;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.model.Activity;

/**
 * A model operation on activities list
 *
 * @author PavelPopov
 */
public class ActivityListModelOperation extends ModelListOperation<Activity> {

    public ActivityListModelOperation(List<Activity> oldValue, List<Activity> newValue,
            CRUDMethod method, Tab tab) {
        super(oldValue, newValue, method, tab);
    }

    @Override
    protected void add(List<Activity> value) {
        value.forEach((t) -> {
            TimeRanger.addActivity(t);
        });
    }

    @Override
    protected void update(List<Activity> oldValue, List<Activity> newValue) {
        int oldCount = oldValue == null ? 0 : oldValue.size();
        int newCount = newValue == null ? 0 : newValue.size();
        int count = Math.max(oldCount, newCount);
        for (int i = 0; i < count; i++) {
            Activity oldVal = null, newVal = null;
            try {
                oldVal = oldValue.get(i);
            } catch (Exception ex) {
            }
            try {
                newVal = newValue.get(i);
            } catch (Exception ex) {
            }
            if (newVal == null) {
                delete(Arrays.asList(oldVal));
            } else if (oldVal == null) {
                add(Arrays.asList(newVal));
            } else {
                newVal.setId(oldVal.getId());
                TimeRanger.updateActivity(new Activity(newVal));
            }
        }
    }

    @Override
    protected void delete(List<Activity> value) {
        TimeRanger.deleteActivities(value);
    }

}
