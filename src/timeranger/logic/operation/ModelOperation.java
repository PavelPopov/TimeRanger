package timeranger.logic.operation;

import javafx.scene.control.Tab;
import timeranger.logic.CRUDMethod;
import timeranger.logic.Operation;
import timeranger.model.Model;

/**
 * User operation on model
 *
 * @author PavelPopov
 */
public abstract class ModelOperation<T extends Model> extends Operation<T> {

    protected final CRUDMethod method;

    public ModelOperation(T oldValue, T newValue, CRUDMethod method, Tab tab) {
        super(oldValue, newValue, tab);
        this.method = method;
    }

    public final CRUDMethod getMethod() {
        return method;
    }

    @Override
    public void undo() {
        switch (method) {
            case CREATE:
                delete(newValue);
                break;
            case READ:
            case UPDATE:
                update(newValue, oldValue);
                break;
            case DELETE:
                add(oldValue);
                break;
        }
    }

    @Override
    public void redo() {
        switch (method) {
            case CREATE:
                add(newValue);
                break;
            case READ:
            case UPDATE:
                update(oldValue, newValue);
                break;
            case DELETE:
                delete(oldValue);
                break;
        }
    }

    protected abstract void add(T value);

    protected abstract void update(T oldValue, T newValue);

    protected abstract void delete(T value);

}
