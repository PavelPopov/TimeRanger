package timeranger.logic.operation;

import java.util.Arrays;
import javafx.scene.control.Tab;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.model.Activity;

/**
 * A model operation on an activity
 *
 * @author PavelPopov
 */
public class ActivityModelOperation extends ModelOperation<Activity> {

    public ActivityModelOperation(Activity oldValue, Activity newValue, CRUDMethod method, Tab tab) {
        super(oldValue, newValue, method, tab);
    }

    @Override
    protected void add(Activity value) {
        TimeRanger.addActivity(value);
    }

    @Override
    protected void update(Activity oldValue, Activity newValue) {
        newValue.setId(oldValue.getId());
        TimeRanger.updateActivity(new Activity(newValue));
    }

    @Override
    protected void delete(Activity value) {
        TimeRanger.deleteActivities(Arrays.asList(value));
    }

}
