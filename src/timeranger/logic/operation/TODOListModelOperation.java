package timeranger.logic.operation;

import java.util.Arrays;
import java.util.List;
import javafx.scene.control.Tab;
import timeranger.TimeRanger;
import timeranger.logic.CRUDMethod;
import timeranger.model.TODO;

/**
 * A model operation on TODOs list
 *
 * @author PavelPopov
 */
public class TODOListModelOperation extends ModelListOperation<TODO> {

    public TODOListModelOperation(List<TODO> oldValue, List<TODO> newValue, CRUDMethod method, Tab tab) {
        super(oldValue, newValue, method, tab);
    }

    @Override
    protected void add(List<TODO> value) {
        value.forEach((t) -> {
            TimeRanger.addTODO(t);
        });
    }

    @Override
    protected void update(List<TODO> oldValue, List<TODO> newValue) {
        int oldCount = oldValue == null ? 0 : oldValue.size();
        int newCount = newValue == null ? 0 : newValue.size();
        int count = Math.max(oldCount, newCount);
        for (int i = 0; i < count; i++) {
            TODO oldVal = null, newVal = null;
            try {
                oldVal = oldValue.get(i);
            } catch (Exception ex) {
            }
            try {
                newVal = newValue.get(i);
            } catch (Exception ex) {
            }
            if (newVal == null) {
                delete(Arrays.asList(oldVal));
            } else if (oldVal == null) {
                add(Arrays.asList(newVal));
            } else {
                TimeRanger.updateTODO(oldVal.getName(), newVal);
            }
        }
    }

    @Override
    protected void delete(List<TODO> value) {
        TimeRanger.deleteTODOs(value);
    }

}
