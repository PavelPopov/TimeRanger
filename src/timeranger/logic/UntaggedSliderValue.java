package timeranger.logic;

/**
 * Enum for untagged modes
 *
 * @author PavelPopov
 */
public enum UntaggedSliderValue {
    EXCLUDE("Exclude untagged"),
    INCLUDE("Include untagged"),
    ONLY("Untagged only");

    /**
     * Name of mode
     */
    private final String name;

    private UntaggedSliderValue(String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public final String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Get mode from name
     *
     * @param name mode name
     * @return mode
     */
    public static final UntaggedSliderValue getByName(String name) {
        UntaggedSliderValue value = null;
        for (UntaggedSliderValue v : UntaggedSliderValue.values()) {
            if (v.getName().equals(name)) {
                value = v;
            }
        }
        return value;
    }

    /**
     * Get mode by index
     *
     * @param index mode index
     * @return mode
     */
    public static final UntaggedSliderValue getByIndex(int index) {
        return index >= 0 && UntaggedSliderValue.values().length > index
                ? UntaggedSliderValue.values()[index] : null;
    }
}
