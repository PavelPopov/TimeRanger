package timeranger.logic;

import javafx.scene.control.Tab;

/**
 * A user operation base class
 *
 * @author PavelPopov
 */
public abstract class Operation<T extends Object> implements Changeable {

    protected final T oldValue, newValue;
    protected final Tab tab;

    public Operation(T oldValue, T newValue, Tab tab) {
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.tab = tab;
    }

    public final T getOldValue() {
        return oldValue;
    }

    public final T getNewValue() {
        return newValue;
    }

    public final Tab getTab() {
        return tab;
    }
}
