package timeranger.logic;

/**
 * Some method of CRUD
 *
 * @author PavelPopov
 */
public enum CRUDMethod {
    CREATE, READ, UPDATE, DELETE
}
