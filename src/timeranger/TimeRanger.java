package timeranger;

import java.awt.AWTException;
import java.awt.Font;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.WeakInvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.WeakListChangeListener;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.stage.Stage;
import javafx.util.Callback;
import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;
import timeranger.logic.Operation;
import timeranger.logic.UntaggedSliderValue;
import timeranger.logic.controller.RootController;
import timeranger.model.Activity;
import timeranger.model.TODO;
import timeranger.model.Tag;
import timeranger.model.Task;

/**
 * Main application class
 *
 * @author PavelPopov
 */
public class TimeRanger {

    /**
     * Application's version
     */
    public static final int VERSION_MAJOR = 1,
            VERSION_MINOR = 0, VERSION_FEATURE = 6;
    /**
     * String representing application's version
     */
    public static final String VERSION
            = VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_FEATURE;
    /**
     * Application's directory (%USER_HOME%/.TimeRanger)
     */
    public static final File APPDIR = new File(System.getProperty("user.home")
            + File.separator + ".TimeRanger" + File.separator);
    /**
     * Database file
     */
    public static final File DBFILE = new File(APPDIR,
            VERSION_MAJOR + "." + VERSION_MINOR + ".db");
    /**
     * Default date formatter
     */
    public static final DateTimeFormatter DATE_FORMATTER
            = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    /**
     * Default time formatter
     */
    public static final DateTimeFormatter TIME_FORMATTER
            = DateTimeFormatter.ofPattern("HH:mm:ss");
    /**
     * Default timestamp formatter
     */
    public static final DateTimeFormatter DATETIME_FORMATTER
            = DateTimeFormatter.ofPattern("dd.MM.yy HH:mm:ss");
    /**
     * Default operations log size
     */
    public static final int OPLOG_SIZE = 100;

    /**
     * Database access object
     */
    private static Database db;
    /**
     * Activeness flags
     */
    private static boolean stageActive, trayActive;
    /**
     * Application's thread
     */
    private static Thread appThread;
    /**
     * JavaFX application
     */
    private static TimeRangerFX app;
    /**
     * JavaFX application stages
     */
    private static Stage rootStage, aboutStage, activityEditStage;
    /**
     * Tray icon
     */
    private static TrayIcon trayIcon;
    /**
     * Runnable for removing app from tray
     */
    private static Runnable rmTrayRunnable;
    /**
     * Activities list
     */
    private static ObservableList<Activity> activities;
    /**
     * TODOs list
     */
    private static ObservableList<TODO> todos;
    /**
     * Tasks list
     */
    private static ObservableList<Task> tasks;
    /**
     * Tags list
     */
    private static ObservableList<Tag> tags;
    /**
     * Tasks to tags bindings
     */
    private static ObservableMap<Task, List<Tag>> tasksTags;
    /**
     * Activities to tags bindings
     */
    private static ObservableMap<Activity, List<Tag>> activitiesTags;
    /**
     * Current activities list
     */
    private static ArrayList<Activity> currentActivities = new ArrayList();
    private static String overviewFilterName = "";
    private static LocalDate overviewFilterStart = LocalDate.now();
    private static LocalDate overviewFilterEnd = LocalDate.now().plusDays(1);
    /**
     * Activity that is currently under edit
     */
    private static Activity activityUnderEdit;
    /**
     * Log of user operations
     */
    private static ArrayList<Operation> operationsLog = new ArrayList();
    private static int operationsLogIndex = 0;

    /**
     * Main application's method
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis());
        Thread.setDefaultUncaughtExceptionHandler(ErrorHandler.getHandler());
        // Starting JavaFX thread
        Platform.setImplicitExit(false);
        final CountDownLatch latch = new CountDownLatch(1);
        appThread = new Thread(() -> {
            Thread.setDefaultUncaughtExceptionHandler(ErrorHandler.getHandler());
            TimeRangerFX.init(args, latch);
        }, "FXAppThread");
        appThread.start();
        initApp();
        System.out.println(System.currentTimeMillis());
        try {
            latch.await();
        } catch (InterruptedException ex) {
            ErrorHandler.handle("Main thread tired of waiting for JavaFX thread!", ex);
            quit();
            System.exit(1);
        }
        System.out.println(System.currentTimeMillis());
        // Getting variables from JavaFX thread
        app = TimeRangerFX.getInstance();
        rootStage = app.getRootStage();
        aboutStage = app.getAboutStage();
        activityEditStage = app.getActivityEditStage();
        // Launching root stage
        final CountDownLatch latch2 = new CountDownLatch(1);
        runLater(() -> {
            rootStage.show();
            TimeRangerPreloader.close();
            stageActive = true;
            latch2.countDown();
        });
        RootController.getInstance().getTODOTabController().update();
        RootController.getInstance().getTasksTabController().update();
        RootController.getInstance().getTagsTabController().update();
        RootController.getInstance().getAnalyzeTabController().update();
        try {
            latch2.await();
        } catch (InterruptedException ex) {
            ErrorHandler.handle("Main thread tired of waiting for JavaFX thread!", ex);
            quit();
            System.exit(1);
        }
        // Sets up the tray icon (using awt code run on the swing thread).
        SwingUtilities.invokeLater(TimeRanger::addAppToTray);
        System.out.println(System.currentTimeMillis());
    }

    public static void initApp() {
        // Creating app directory if not exists
        if (!APPDIR.isDirectory()) {
            if (APPDIR.isFile()) {
                throw new IllegalStateException("Failed to create directory \""
                        + APPDIR.getAbsolutePath()
                        + "\" because there is a file with the same name");
            }
            if (!APPDIR.mkdirs()) {
                throw new RuntimeException("Failed to create app directory");
            }
        }
        // Loading database
        db = new Database();
        try {
            db.connect(DBFILE.getCanonicalPath());
        } catch (IOException ex) {
            throw new RuntimeException("Failed to get canonical path of database file", ex);
        }
        db.createSchema();
        // Initializing collections
        currentActivities = db.selectCurrentActivities();
        ArrayList<Activity> activitiesList = db.selectActivities(LocalDate.now().atStartOfDay(),
                LocalDate.now().plusDays(1).atStartOfDay(), "");
        activities = FXCollections.observableList(activitiesList);
        ArrayList<TODO> todoList = db.selectAllTODOs();
        todos = FXCollections.observableList(todoList);
        ArrayList<Task> taskList = db.selectAllTasks();
        tasks = FXCollections.observableList(taskList);
        ArrayList<Tag> tagList = db.selectAllTags();
        tags = FXCollections.observableList(tagList);
        HashMap<Activity, List<Tag>> activitiesTagsMap = db.selectAllActivitiesTags();
        activitiesTags = FXCollections.observableHashMap();
        activitiesTags.putAll(activitiesTagsMap);
        updateActivitiesTags();
        HashMap<Task, List<Tag>> tasksTagsMap = db.selectAllTasksTags();
        tasksTags = FXCollections.observableHashMap();
        tasksTags.putAll(tasksTagsMap);
        // Update tasks tags
        for (Task task : tasks) {
            List<Tag> tags = tasksTags.get(task);
            if (tags == null) {
                tags = new ArrayList();
            }
            task.setTags(tags);
        }
        // Adding updates on changes
        InvalidationListener updateActivitiesTagsListener
                = new WeakInvalidationListener((c) -> {
                    updateActivitiesTags();
                });
        activities.addListener(updateActivitiesTagsListener);
        tags.addListener(updateActivitiesTagsListener);
        activitiesTags.addListener(updateActivitiesTagsListener);
        InvalidationListener updateAnalyzeTabListener
                = new WeakInvalidationListener((c) -> {
                    RootController.getInstance().getAnalyzeTabController().update();
                });
        activities.addListener(updateAnalyzeTabListener);
        tags.addListener(updateAnalyzeTabListener);
        activitiesTags.addListener(updateAnalyzeTabListener);
        tasks.addListener(updateAnalyzeTabListener);
        tasksTags.addListener(updateAnalyzeTabListener);
        tags.addListener(new WeakListChangeListener((c) -> {
            RootController.getInstance().getTasksTabController().update();
        }));
    }

    /**
     * Add and configure application's tray
     */
    private static void addAppToTray() {
        try {
            // Ensure awt toolkit is initialized.
            Toolkit.getDefaultToolkit();
            // App requires system tray support, just exit if there is no support.
            if (!SystemTray.isSupported()) {
                ErrorHandler.handle("Failed to access tray!",
                        "Tray is required for application to work but it's not supported");
                quit();
                System.exit(1);
            }
            // Set up a system tray icon.
            SystemTray tray = SystemTray.getSystemTray();
            BufferedImage trayIconImage = ImageIO.read(TimeRanger.class.getResource("view/resource/icon.png"));
            int trayIconWidth = new TrayIcon(trayIconImage).getSize().width;
            trayIcon = new TrayIcon(trayIconImage.getScaledInstance(trayIconWidth, -1, java.awt.Image.SCALE_SMOOTH));
            // If the user double-clicks on the tray icon, show the main app stage.
            trayIcon.addActionListener(event -> Platform.runLater(TimeRanger::showStage));
            MenuItem openItem = new MenuItem("Open");
            openItem.addActionListener(event -> Platform.runLater(TimeRanger::showStage));
            // The convention for tray icons seems to be to set the default icon for opening
            // the application stage in a bold font.
            Font defaultFont = Font.decode(null);
            Font boldFont = defaultFont.deriveFont(Font.BOLD);
            openItem.setFont(boldFont);
            // To really exit the application, the user must go to the system tray icon
            // and select the exit option, this will shutdown JavaFX and remove the
            // tray icon (removing the tray icon will also shut down AWT).
            MenuItem exitItem = new MenuItem("Quit");
            rmTrayRunnable = () -> {
                tray.remove(trayIcon);
            };
            exitItem.addActionListener(event -> TimeRanger.quit());
            // Setup the popup menu for the application.
            final PopupMenu popup = new PopupMenu();
            popup.add(openItem);
            popup.add(exitItem);
            trayIcon.setPopupMenu(popup);
            // add the application tray icon to the system tray.
            tray.add(trayIcon);
        } catch (AWTException | IOException ex) {
            ErrorHandler.handle("Failed to put application into tray!", ex);
            quit();
            System.exit(1);
        }
    }

    /**
     * Quit application safely
     */
    public static void quit() {
        hideStage();
        if (db.isConnected()) {
            db.close();
        }
        if (rmTrayRunnable != null) {
            SwingUtilities.invokeLater(rmTrayRunnable);
        }
        Platform.exit();
    }

    /**
     * Run later in JavaFX thread
     *
     * @param r runnable to run
     */
    public static void runLater(Runnable r) {
        Platform.runLater(r);
    }

    /**
     * Run later and wait in JavaFX thread
     *
     * @param r runnable to run
     */
    public static void runAndWait(Runnable r) {
        if (r == null) {
            throw new NullPointerException("Runnable is null!");
        }
        // Run synchronously on JavaFX thread
        if (Platform.isFxApplicationThread()) {
            r.run();
            return;
        }
        // Queue on JavaFX thread and wait for completion
        final CountDownLatch doneLatch = new CountDownLatch(1);
        Platform.runLater(() -> {
            try {
                r.run();
            } finally {
                doneLatch.countDown();
            }
        });
        try {
            doneLatch.await();
        } catch (InterruptedException e) {
        }
    }

    /**
     * Show root stage
     */
    public static void showStage() {
        if (rootStage != null && !stageActive) {
            rootStage.show();
            rootStage.toFront();
            RootController.getInstance().onStageShown();
        }
    }

    /**
     * Hide root stage
     */
    public static void hideStage() {
        if (rootStage != null && stageActive) {
            runLater(() -> {
                rootStage.hide();
                stageActive = false;
            });
        }
    }

    /**
     * Show notification to user
     *
     * @param title notification's title
     * @param message notification's message
     * @param type type of icon
     */
    public static void showNotification(String title, String message, TrayIcon.MessageType type) {
        SwingUtilities.invokeLater(() -> trayIcon.displayMessage(title, message, type));
    }

    /**
     * Update overview tab
     */
    public static void updateOverview() {
        ArrayList<Activity> activitiesList = db.selectActivities(overviewFilterStart.atStartOfDay(),
                overviewFilterEnd.atStartOfDay(), overviewFilterName);
        activities.setAll(activitiesList);
        RootController.getInstance().getOverviewTabController().update();
    }

    /**
     * Add TODO
     *
     * @param todo TODO
     */
    public static void addTODO(TODO todo) {
        String name = todo.getName().trim();
        if (name.isEmpty()) {
            throw new IllegalArgumentException("TODO name cannot be empty");
        }
        if (!todos.contains(todo)) {
            todos.add(todo);
            RootController.getInstance().getTODOTabController().update();
            db.insertTODO(todo);
        }
    }

    /**
     * Update TODO
     *
     * @param key identificator
     * @param todo TODO
     */
    public static void updateTODO(String key, TODO todo) {
        String oldName = key.trim();
        String name = todo.getName().trim();
        if (oldName.isEmpty() || name.isEmpty()) {
            throw new IllegalArgumentException("TODO name cannot be empty");
        }
        if (oldName.equals(name)) {
            throw new IllegalArgumentException("Value is the same");
        }
        todos.removeIf((t) -> {
            return t.getName().equals(oldName);
        });
        todos.removeAll(todo);
        todos.add(todo);
        RootController.getInstance().getTODOTabController().update();
        db.updateTODO(oldName, todo);
    }

    /**
     * Delete TODOs
     *
     * @param todos list of TODOs
     */
    public static void deleteTODOs(List<TODO> todos) {
        if (todos.size() < 1) {
            return;
        }
        TimeRanger.todos.removeAll(todos);
        RootController.getInstance().getTODOTabController().update();
        db.deleteTODOs(todos);
    }

    /**
     * Add activity
     *
     * @param activity activity
     */
    public static void addActivity(Activity activity) {
        String name = activity.getName().trim();
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Activity name cannot be empty");
        }
        activities.add(activity);
        if (activity.getEnd().compareTo(LocalDateTime.now()) > 0) {
            currentActivities.add(activity);
        }
        RootController.getInstance().getOverviewTabController().update();
        db.insertActivity(activity);
    }

    /**
     * Change current activities
     *
     * @param activity current activity
     * @param endCurrentActivities flag determining inclusion of previous current activities
     */
    public static void changeActivity(Activity activity, boolean endCurrentActivities) {
        String name = activity.getName().trim();
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Activity name cannot be empty");
        }
        activity.setEnd(null);
        List<Activity> updateActivities = new ArrayList();
        if (endCurrentActivities) {
            for (Activity currentActivity : currentActivities) {
                if (currentActivity.getEnd() == null) {
                    /*
                        CurrentActivities and activities are containing different instances
                        so we need to move currentActivity to activities list.
                        To do so we need to first remove activity with same id from
                        activities and then add currentActivity instance to list.
                        TODO: Maybe there's a better way to keep collections synchronized.
                     */
                    activities.removeIf((t) -> {
                        return t.getId() == currentActivity.getId();
                    });
                    activities.add(currentActivity);
                    currentActivity.setEnd(activity.getStart());
                    updateActivities.add(currentActivity);
                }
            }
            currentActivities.clear();
        }
        currentActivities.add(activity);
        // TODO: Remove side-effect in updateActivitiesTags() method determining sequence of update
        activitiesTags.put(activity, activity.getTags());
        activities.add(activity);
        RootController.getInstance().getOverviewTabController().update();
        for (Activity updateActivity : updateActivities) {
            db.updateActivity(updateActivity);
        }
        db.insertActivity(activity);
        db.updateActivityTags(activity, activity.getTags());
    }

    /**
     * Delete activities
     *
     * @param activities activities list
     */
    public static void deleteActivities(List<Activity> activities) {
        if (activities.size() < 1) {
            return;
        }
        TimeRanger.activities.removeAll(activities);
        RootController.getInstance().getOverviewTabController().update();
        db.deleteActivities(activities);
    }

    /**
     * Update activity
     *
     * @param activity activity
     */
    public static void updateActivity(Activity activity) {
        String name = activity.getName().trim();
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Activity name cannot be empty");
        }
        activities.removeIf((t) -> {
            return t.getId() == activity.getId();
        });
        activitiesTags.remove(activity);
        activities.removeAll(activity);
        if (currentActivities.contains(activity) && activity.getEnd() != null
                && activity.getStart().compareTo(LocalDateTime.now()) < 0
                && activity.getEnd().compareTo(LocalDateTime.now()) > 0) {
            currentActivities.remove(activity);
        }
        activities.add(activity);
        activitiesTags.put(activity, activity.getTags());
        RootController.getInstance().getOverviewTabController().update();
        db.updateActivity(activity);
        db.updateActivityTags(activity, activity.getTags());
    }

    /**
     * Add task
     *
     * @param task task
     */
    public static void addTask(Task task) {
        String name = task.getName().trim();
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Task name cannot be empty");
        }
        boolean found = false;
        for (Task t : tasks) {
            if (t.getName().equals(name)) {
                found = true;
            }
        }
        if (!found) {
            List<Tag> tags = task.getTags();
            if (tags == null) {
                tags = new ArrayList();
            }
            tasksTags.put(task, tags);
            tasks.add(task);
            RootController.getInstance().getTasksTabController().update();
            db.insertTask(task);
            db.updateTaskTags(task, task.getTags());
        }
    }

    /**
     * Update task
     *
     * @param task task
     */
    public static void updateTask(Task task) {
        String name = task.getName().trim();
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Task name cannot be empty");
        }
        tasks.removeIf((t) -> {
            return t.getId() == task.getId();
        });
        tasksTags.remove(task);
        tasks.removeAll(task);
        List<Tag> tags = task.getTags();
        if (tags == null) {
            tags = new ArrayList();
        }
        tasksTags.put(task, tags);
        tasks.add(task);
        RootController.getInstance().getTasksTabController().update();
        db.updateTask(task);
        db.updateTaskTags(task, task.getTags());
    }

    /**
     * Delete tasks
     *
     * @param tasks list of tasks
     */
    public static void deleteTasks(List<Task> tasks) {
        if (tasks.size() < 1) {
            return;
        }
        TimeRanger.tasks.removeAll(tasks);
        tasksTags.keySet().removeAll(tasks);
        RootController.getInstance().getTasksTabController().update();
        db.deleteTasks(tasks);
    }

    /**
     * Add tag
     *
     * @param tag tag
     */
    public static void addTag(Tag tag) {
        String name = tag.getName().trim();
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Tag name cannot be empty");
        }
        boolean found = false;
        for (Tag t : tags) {
            if (t.getName().equals(name)) {
                found = true;
            }
        }
        if (!found) {
            tags.add(tag);
            RootController.getInstance().getTagsTabController().update();
            db.insertTag(tag);
        }
    }

    /**
     * Update tag
     *
     * @param tag tag
     */
    public static void updateTag(Tag tag) {
        String name = tag.getName().trim();
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Tag name cannot be empty");
        }
        Tag oldTag = tags.filtered((t) -> {
            return t.getId() == tag.getId();
        }).get(0);
        if (oldTag == null) {
            throw new IllegalStateException("Tag does not exist");
        }
        oldTag.setName(name);
        oldTag.setColor(tag.getColor());
        oldTag.setDescription(tag.getDescription());
        RootController.getInstance().getTagsTabController().update();
        RootController.getInstance().getTasksTabController().update();
        db.updateTag(tag);
    }

    /**
     * Delete tags
     *
     * @param tags list of tags
     */
    public static void deleteTags(List<Tag> tags) {
        if (tags.size() < 1) {
            return;
        }
        TimeRanger.tags.removeAll(tags);
        for (Task t : tasks) {
            t.getTags().removeAll(tags);
        }
        for (List<Tag> t : tasksTags.values()) {
            t.removeAll(tags);
        }
        RootController.getInstance().getTagsTabController().update();
        db.deleteTags(tags);
    }

    /**
     * Update task tags
     *
     * @param task task
     * @param tags list of tags
     */
    public static void updateTaskTags(Task task, List<Tag> tags) {
        tasksTags.put(task, tags);
        for (Task t : tasks) {
            if (t.getId() == task.getId()) {
                t.setTags(tags);
            }
        }
        RootController.getInstance().getTasksTabController().update();
        db.updateTaskTags(task, tags);
    }

    /**
     * Update activity tags
     *
     * @param activity activity
     * @param tags list of tags
     */
    public static void updateActivityTags(Activity activity, List<Tag> tags) {
        activitiesTags.put(activity, tags);
        for (Activity a : activities) {
            if (a.getId() == activity.getId()) {
                a.setTags(tags);
            }
        }
        RootController.getInstance().getOverviewTabController().update();
        RootController.getInstance().getAnalyzeTabController().update();
        db.updateActivityTags(activity, tags);
    }

    /**
     * Push activitiesTags to all activities models
     */
    public static void updateActivitiesTags() {
        for (Activity activity : activities) {
            List<Tag> tags = activitiesTags.get(activity);
            if (tags == null) {
                tags = new ArrayList();
            }
            activity.setTags(tags);
        }
    }

    /**
     * Select activities filtered by analysis tab filter
     *
     * @param name name filter
     * @param start start time bound
     * @param end end time bound
     * @param tags list of tags
     * @param untagged untagged activities handling mode
     * @return list of filtered activities
     */
    public static ArrayList<Activity> selectFilteredActivities(String name,
            LocalDateTime start, LocalDateTime end, List<Tag> tags, UntaggedSliderValue untagged) {
        ArrayList<Activity> filteredActivities = new ArrayList();
        ArrayList<Activity> activities = db.selectActivities(start, end, name, false, false);
        for (Activity activity : activities) {
            List<Tag> t = activitiesTags.get(activity);
            if (t == null) {
                t = new ArrayList();
            }
            activity.setTags(t);
            switch (untagged) {
                case ONLY: // Only untagged activities count
                    if (!t.isEmpty()) {
                        filteredActivities.add(activity);
                    }
                    continue;
                case INCLUDE: // Untagged activities included
                    if (t.isEmpty()) {
                        continue;
                    }
                case EXCLUDE: // Untagged activities excluded
                default:
            }
            boolean included = false;
            for (Tag tag : tags) {
                if (t.contains(tag)) {
                    included = true;
                }
            }
            if (!included) {
                filteredActivities.add(activity);
            }
        }
        activities.removeAll(filteredActivities);
        return activities;
    }

    /**
     * Obtain first activity start time
     *
     * @return first activity start time
     */
    public static LocalDateTime getFirstActivityStartTime() {
        Activity activity = db.selectFirstActivity();
        return activity == null ? null : activity.getStart();
    }

    /**
     * Obtain last activity end time
     *
     * @return last activity end time
     */
    public static LocalDateTime getLastActivityEndTime() {
        Activity activity = db.selectLastActivity();
        return activity == null ? null : activity.getEnd();
    }

    /**
     * TableColumn cell factory formatted by DateTimeFormatter To be used with
     * date fields
     *
     * @param <R> model
     * @param <T> type based on Temporal
     * @param format formatter of date and time
     * @return cell factory
     */
    public static <R, T extends Temporal> Callback<TableColumn<R, T>, TableCell<R, T>> getDateCell(DateTimeFormatter format) {
        return column -> {
            return new TableCell<R, T>() {
                @Override
                protected void updateItem(T item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                    } else {
                        setText(format.format(item));
                    }
                }
            };
        };
    }

    public static boolean hasUndo() {
        return operationsLogIndex > 0;
    }

    public static boolean hasRedo() {
        return operationsLogIndex < operationsLog.size();
    }

    public static void undo() {
        if (!hasUndo()) {
            return;
        }
        Operation op = operationsLog.get(operationsLogIndex - 1);
        op.undo();
        operationsLogIndex--;
        RootController.getInstance().updateUndoRedoItems();
    }

    public static void redo() {
        if (!hasRedo()) {
            return;
        }
        Operation op = operationsLog.get(operationsLogIndex);
        op.redo();
        operationsLogIndex++;
        RootController.getInstance().updateUndoRedoItems();
    }

    public static void addOperation(Operation operation) {
        if (operation == null) {
            return;
        }
        if (hasRedo()) {
            ArrayList<Operation> ops = new ArrayList();
            for (int i = 0; i < operationsLogIndex; i++) {
                ops.add(i, operationsLog.get(i));
            }
            operationsLog.clear();
            operationsLog.addAll(ops);
        }
        operationsLog.add(operationsLogIndex, operation);
        operationsLogIndex++;
        RootController.getInstance().updateUndoRedoItems();
    }

    /**
     * @return the overviewFilterName
     */
    public static String getOverviewFilterName() {
        return overviewFilterName;
    }

    /**
     * @param aOverviewFilterName the overviewFilterName to set
     */
    public static void setOverviewFilterName(String aOverviewFilterName) {
        overviewFilterName = aOverviewFilterName;
    }

    /**
     * @return the overviewFilterStart
     */
    public static LocalDate getOverviewFilterStart() {
        return overviewFilterStart;
    }

    /**
     * @param aOverviewFilterStart the overviewFilterStart to set
     */
    public static void setOverviewFilterStart(LocalDate aOverviewFilterStart) {
        overviewFilterStart = aOverviewFilterStart;
    }

    /**
     * @return the overviewFilterEnd
     */
    public static LocalDate getOverviewFilterEnd() {
        return overviewFilterEnd;
    }

    /**
     * @param aOverviewFilterEnd the overviewFilterEnd to set
     */
    public static void setOverviewFilterEnd(LocalDate aOverviewFilterEnd) {
        overviewFilterEnd = aOverviewFilterEnd;
    }

    /**
     * @return the currentActivities
     */
    public static ArrayList<Activity> getCurrentActivities() {
        return currentActivities;
    }

    /**
     * @return root stage
     */
    public static Stage getRootStage() {
        return rootStage;
    }

    /**
     * @return about stage
     */
    public static Stage getAboutStage() {
        return aboutStage;
    }

    /**
     * @return activity edit stage
     */
    public static Stage getActivityEditStage() {
        return activityEditStage;
    }

    /**
     * Set activity under edit
     *
     * @param activity activity
     */
    public static void setActivityUnderEdit(Activity activity) {
        activityUnderEdit = activity;
    }

    /**
     * @return activity under edit
     */
    public static Activity getActivityUnderEdit() {
        return activityUnderEdit;
    }

    /**
     * @return TODOs list
     */
    public static ObservableList<TODO> getTODOs() {
        return todos;
    }

    /**
     * @return activities list
     */
    public static ObservableList<Activity> getActivities() {
        return activities;
    }

    /**
     * @return tags list
     */
    public static ObservableList<Tag> getTags() {
        return tags;
    }

    /**
     * @return tasks list
     */
    public static ObservableList<Task> getTasks() {
        return tasks;
    }

}
