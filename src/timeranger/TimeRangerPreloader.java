package timeranger;

import com.jfoenix.controls.JFXProgressBar;
import javafx.application.Preloader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class TimeRangerPreloader extends Preloader {

    private static final int WIDTH = 400, HEIGHT = 320;

    private static TimeRangerPreloader instance;

    private Stage preloaderStage;
    private long startDownload = -1;
    private JFXProgressBar bar;
    private Label label;

    public TimeRangerPreloader() {
        super();
        instance = this;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.preloaderStage = primaryStage;
        Pane canvas = new Pane();
        canvas.setPrefSize(WIDTH, HEIGHT);
        Image logo = new Image(TimeRanger.class.getResourceAsStream("view/resource/logo.jpg"));
        BackgroundImage bgi = new BackgroundImage(logo, BackgroundRepeat.SPACE, BackgroundRepeat.SPACE,
                BackgroundPosition.CENTER, BackgroundSize.DEFAULT);
        canvas.setBackground(new Background(bgi));
        Label info = new Label("TimeRanger\nv"
                + TimeRanger.VERSION_MAJOR + "." + TimeRanger.VERSION_MINOR + "." + TimeRanger.VERSION_FEATURE
                + "\nby PavelPopov");
        info.setPadding(new Insets(0, 10, 0, 0));
        info.setFont(new javafx.scene.text.Font(12));
        info.setTextFill(Paint.valueOf("gray"));
        VBox vbox = new VBox(info);
        vbox.setMinSize(WIDTH, 60);
        vbox.relocate(0, 0);
        vbox.setAlignment(Pos.CENTER_RIGHT);
        label = new Label("Launching TimeRanger...");
        label.relocate(10, HEIGHT - 25);
        label.setFont(new javafx.scene.text.Font(14));
        label.setTextFill(Paint.valueOf("gray"));
        bar = new JFXProgressBar();
        bar.relocate(0, HEIGHT - 5);
        bar.setMinWidth(WIDTH);
        canvas.getChildren().addAll(vbox, label, bar);
        BorderPane root = new BorderPane(canvas);
        Scene scene = new Scene(root);
        scene.getStylesheets().add(TimeRanger.class.getResource("view/css/preloader.css").toExternalForm());
        primaryStage.setWidth(WIDTH);
        primaryStage.setHeight(HEIGHT);
        primaryStage.setResizable(false);
        primaryStage.setTitle("TimeRanger");
        primaryStage.setAlwaysOnTop(true);
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX((screenBounds.getWidth() - primaryStage.getWidth()) / 2);
        primaryStage.setY((screenBounds.getHeight() - primaryStage.getHeight()) / 2);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void handleProgressNotification(ProgressNotification info) {
        if (startDownload == -1) {
            startDownload = System.currentTimeMillis();
        }
        bar.setProgress(info.getProgress() * 0.5);
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification info) {
        switch (info.getType()) {
            case BEFORE_INIT:
                if ((System.currentTimeMillis() - startDownload) < 500) {
                    bar.setProgress(0);
                }
                label.setText("Initializing...");
                break;
            case BEFORE_LOAD:
                label.setText("Loading...");
                break;
            case BEFORE_START:
                label.setText("Starting...");
                break;
        }
    }

    public static void setLoading(String text, double value) {
        if (instance != null) {
            instance.label.setText(text);
            instance.bar.setProgress(value);
        }
    }

    public static void close() {
        if (instance != null) {
            instance.preloaderStage.close();
        }
    }
}
